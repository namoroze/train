#!/bin/bash
PIN=$1
GPIO_PATH=/sys/class/gpio/
GPIO_EXPORT=${GPIO_PATH}export
if ! [ -d ${GPIO_PATH}gpio${PIN} ];then
        echo $PIN > $GPIO_EXPORT
        echo in >${GPIO_PATH}gpio${PIN}/direction
fi
