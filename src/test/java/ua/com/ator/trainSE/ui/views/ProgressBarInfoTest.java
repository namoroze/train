package ua.com.ator.trainSE.ui.views;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 11.10.2017.
 */
public class ProgressBarInfoTest {

    @Test
    public void isWithZero() throws Exception {
        ProgressBarInfo progressBarInfo = new ProgressBarInfo(0,250);
        assertFalse(progressBarInfo.isWithZero());
    }

}