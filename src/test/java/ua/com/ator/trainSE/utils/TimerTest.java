package ua.com.ator.trainSE.utils;

import org.junit.Assert;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 20.07.2017.
 */
public class TimerTest {
    private static final int timerDelayMs = 10;
    @org.junit.Test
    public void check() throws Exception {
        Timer timer = new Timer(timerDelayMs);
        Assert.assertFalse(timer.check());
        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.check());
    }
    @org.junit.Test
    public void checkWithCondition() throws Exception {
        Timer timer = new Timer(timerDelayMs);
        Assert.assertFalse(timer.check(()->false));
        Assert.assertFalse(timer.check(()->true));

        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertFalse(timer.check(()->false));

        timer.check();
        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.check(()->true));

        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.check(()->true));

        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertFalse(timer.check(()->false));
    }
    @org.junit.Test
    public void checkWithReset() throws Exception {
        Timer timer = new Timer(timerDelayMs);
        Assert.assertFalse(timer.checkWithReset());

        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.checkWithReset());
        Assert.assertFalse(timer.checkWithReset());
    }
    @org.junit.Test
    public void checkWithResetWithCondition() throws Exception {
        Timer timer = new Timer(timerDelayMs);
        Assert.assertFalse(timer.checkWithReset(()->false));
        Assert.assertFalse(timer.checkWithReset(()->true));

        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertFalse(timer.checkWithReset(()->false));

        timer.checkWithReset();
        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.checkWithReset(()->true));
        Assert.assertFalse(timer.checkWithReset(()->true));
    }
    @org.junit.Test
    public void stop() throws Exception  {
        Timer timer = new Timer(timerDelayMs);
        Assert.assertFalse(timer.check());
        TimeUnit.MILLISECONDS.sleep(timerDelayMs);
        Assert.assertTrue(timer.check());
        Assert.assertTrue(timer.check());
        timer.stop();
        Assert.assertFalse(timer.check());
    }
}