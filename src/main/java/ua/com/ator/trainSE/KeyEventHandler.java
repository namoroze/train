package ua.com.ator.trainSE;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import org.springframework.beans.factory.annotation.Required;
import ua.com.ator.trainSE.ui.pages.PageSwitcher;

import java.util.*;
import java.util.function.Consumer;
import java.util.prefs.Preferences;

import static ua.com.ator.trainSE.KeyEventHandler.ScaleService.*;
import static ua.com.ator.trainSE.keypad.KeypadButtons.*;

/**
 * @author Dmitry Sokolov on 27.10.2017.
 * @author Baevskiy Ilya on 22.10.2017.
 */
public class KeyEventHandler implements EventHandler<KeyEvent> {
    private Runnable switchOffWithSettings;
    private Runnable restart;
    private PageSwitcher pageSwitcher;
    private List<Runnable> actionListeners = new ArrayList<>();

    /* Map with actions for every event */
    private final EnumMap<KeyCode, Consumer<PageSwitcher>> actions = new EnumMap<KeyCode, Consumer<PageSwitcher>>(KeyCode.class) {
        {
            put(KeyCode.Z, ps -> ps.handle(F1));
            put(KeyCode.X, ps -> ps.handle(F2));
            put(KeyCode.C, ps -> ps.handle(F3));
            put(KeyCode.V, ps -> ps.handle(F4));

            put(KeyCode.UP, ScaleService::decTranslateYProperty);
            put(KeyCode.DOWN, ScaleService::incTranslateYProperty);
            put(KeyCode.LEFT, ScaleService::decTranslateXProperty);
            put(KeyCode.RIGHT, ScaleService::incTranslateXProperty);

            put(KeyCode.NUMPAD4, ScaleService::decScaleXProperty);
            put(KeyCode.NUMPAD6, ScaleService::incScaleXProperty);
            put(KeyCode.NUMPAD8, ScaleService::incScaleYProperty);
            put(KeyCode.NUMPAD2, ScaleService::decScaleYProperty);

            put(KeyCode.PAGE_UP, ScaleService::decRotateProperty);
            put(KeyCode.PAGE_DOWN, ScaleService::incRotateProperty);
        }
    };
    private final KeyCodeCombination altX = new KeyCodeCombination(KeyCode.X, KeyCombination.ALT_DOWN);
    private final KeyCodeCombination altQ = new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN);


    @Override
    public void handle(KeyEvent event) {
        actionListeners.forEach(Runnable::run);

        if (altX.match(event)) {
            restart.run();
        } else if (altQ.match(event)) {
            switchOffWithSettings.run();
        } else {
            Optional.ofNullable(actions.get(event.getCode()))
                    .ifPresent(action -> action.accept(pageSwitcher));
        }
    }

    @Required
    public KeyEventHandler setSwitchOffWithSettings(Runnable switchOffWithSettings) {
        this.switchOffWithSettings = switchOffWithSettings;
        return this;
    }

    @Required
    public KeyEventHandler setRestart(Runnable restart) {
        this.restart = restart;
        return this;
    }

    @Required
    public KeyEventHandler setPageSwitcher(PageSwitcher pageSwitcher) {
        pageSwitcher.getView().translateXProperty().set(getTranslateX());
        pageSwitcher.getView().translateYProperty().set(getTranslateY());
        pageSwitcher.getView().scaleXProperty().set(getScaleX());
        pageSwitcher.getView().scaleYProperty().set(getScaleY());
        pageSwitcher.getView().rotateProperty().set(getRotate());
        this.pageSwitcher = pageSwitcher;
        return this;
    }

    public KeyEventHandler setActionListener(Runnable actionListener) {
        actionListeners.add(actionListener);
        return this;
    }

    static class ScaleService {
        private static Preferences storage = Preferences.userNodeForPackage(KeyEventHandler.class);

        static double getTranslateY() {
            return getExpendedResources("ty", 0);
        }

        static double getTranslateX() {
            return getExpendedResources("tx", 0);
        }

        static double getScaleY() {
            return getExpendedResources("sy", 1);
        }

        static double getScaleX() {
            return getExpendedResources("sx", 1);
        }

        static double getRotate() {
            return getExpendedResources("r", 0);
        }

        static void incTranslateYProperty(PageSwitcher ps) {
            double val = ps.getView().translateYProperty().get() + 1;
            ps.getView().translateYProperty().set(val);
            setExpendedResources("ty", val);
        }

        static void decTranslateYProperty(PageSwitcher ps) {
            double val = ps.getView().translateYProperty().get() - 1;
            ps.getView().translateYProperty().set(val);
            setExpendedResources("ty", val);
        }

        static void incTranslateXProperty(PageSwitcher ps) {
            double val = ps.getView().translateXProperty().get() + 1;
            ps.getView().translateXProperty().set(val);
            setExpendedResources("tx", val);
        }

        static void decTranslateXProperty(PageSwitcher ps) {
            double val = ps.getView().translateXProperty().get() - 1;
            ps.getView().translateXProperty().set(val);
            setExpendedResources("tx", val);
        }

        static void incScaleYProperty(PageSwitcher ps) {
            double val = ps.getView().scaleYProperty().get() + 0.001;
            ps.getView().scaleYProperty().setValue(val);
            setExpendedResources("sy", val);
        }

        static void decScaleYProperty(PageSwitcher ps) {
            double val = ps.getView().scaleYProperty().get() - 0.001;
            ps.getView().scaleYProperty().setValue(val);
            setExpendedResources("sy", val);
        }

        static void incScaleXProperty(PageSwitcher ps) {
            double val = ps.getView().scaleXProperty().get() + 0.001;
            ps.getView().scaleXProperty().setValue(val);
            setExpendedResources("sx", val);
        }

        static void decScaleXProperty(PageSwitcher ps) {
            double val = ps.getView().scaleXProperty().get() - 0.001;
            ps.getView().scaleXProperty().setValue(val);
            setExpendedResources("sx", val);
        }

        static void incRotateProperty(PageSwitcher ps) {
            double val = ps.getView().rotateProperty().get() + 0.1;
            ps.getView().rotateProperty().setValue(val);
            setExpendedResources("r", val);
        }

        static void decRotateProperty(PageSwitcher ps) {
            double val = ps.getView().rotateProperty().get() - 0.1;
            ps.getView().rotateProperty().setValue(val);
            setExpendedResources("r", val);
        }


        static void setExpendedResources(String storageName, double data) {
            storage.putDouble(storageName, data);
        }

        static double getExpendedResources(String storageName, double defaultValue) {
            return storage.getDouble(storageName, defaultValue);
        }
    }
}
