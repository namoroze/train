package ua.com.ator.trainSE;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.NumberBinding;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ua.com.ator.trainSE.devices.ConditionerBoard;
import ua.com.ator.trainSE.devices.IOControllerDevice;
import ua.com.ator.trainSE.devices.components.Sensor;
import ua.com.ator.trainSE.keypad.Keypad;
import ua.com.ator.trainSE.loggger.Logger;
import ua.com.ator.trainSE.ui.pages.PageSwitcher;
import ua.com.ator.trainSE.utils.DumpService;
import ua.com.ator.trainSE.utils.OsCmdService;
import ua.com.ator.trainSE.utils.ScreenSaverService;
import ua.com.ator.trainSE.utils.ShutdownService;

import java.util.Optional;

import static ua.com.ator.trainSE.keypad.KeypadButtons.*;


/**
 * BOOTSTRAP FOR APPLICATION TRAIN
 * JavaFX Application with Spring-core
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
public class App extends Application {
    private Scene scene;
    private BooleanProperty workingProperty = new SimpleBooleanProperty(false);
    private BooleanBinding connError;

    public static void main(String[] args) {
        System.setProperty("TRAIN_PATH", Optional.ofNullable(System.getenv("TRAIN_PATH")).orElse("/etc/train"));
        DumpService.checkDump();
        LauncherImpl.launchApplication(App.class, AppPreloader.class, args);
    }


    @Override
    public void init() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:xml/context.xml");

        connError = applicationContext.getBean(ConditionerBoard.class).connectionErrorProperty()
                .or(applicationContext.getBean(IOControllerDevice.class).connectionErrorProperty());

        connError.addListener((observable, oldValue, newValue) -> applicationContext
                .getBean( "establishedTemperatureProperty", SimpleDoubleProperty.class).set(24)
        );

        ((Sensor) applicationContext.getBean("establishedTemperatureSensor"))
                .averageValueProperty().bind(
                new When(connError)
                        .then(24)
                        .otherwise(applicationContext
                                .getBean(SimpleDoubleProperty.class, "establishedTemperatureProperty")));

        PageSwitcher pageSwitcher = applicationContext.getBean(PageSwitcher.class);
        scene = new Scene(new Group(pageSwitcher.getView()), 800, 480);

        scene.getStylesheets().addAll("css/style.css");

        KeyEventHandler keyEventHandler = new KeyEventHandler()
                .setActionListener(applicationContext.getBean(ScreenSaverService.class))
                .setPageSwitcher(pageSwitcher)
                .setRestart(() -> {
                    workingProperty.setValue(false);
                    Platform.exit();
                })
                .setSwitchOffWithSettings(() -> {
                    workingProperty.setValue(false);
                    OsCmdService.systemMenu();
                });

        scene.setOnKeyPressed(keyEventHandler);

        /* Binding GPIO BUTTONS */
        Keypad keypad = applicationContext.getBean(Keypad.class);
        keypad.addKeyListener(() -> pageSwitcher.handle(F1), F1);
        keypad.addKeyListener(() -> pageSwitcher.handle(F2), F2);
        keypad.addKeyListener(() -> pageSwitcher.handle(F3), F3);
        keypad.addKeyListener(() -> pageSwitcher.handle(F4), F4);

        /*Binding for shutdown GPIO*/
        ShutdownService shutdownService = new ShutdownService(7000, workingProperty);
        keypad.addKeyListener(shutdownService::shutdown, SHUTDOWN);

        applicationContext.getBean(Logger.class).logWorking(workingProperty);

        keypad.addKeyListenerForAllKey(applicationContext.getBean(ScreenSaverService.class));
    }

    private ClassPathXmlApplicationContext applicationContext;

    @Override
    public void start(Stage primaryStage) throws Exception {
        workingProperty.set(true);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setResizable(false);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        applicationContext.close();
        System.exit(0);
    }


}