package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.devices.components.State.HeatingState;
import ua.com.ator.trainSE.utils.Timer;

import java.util.Optional;
import java.util.Properties;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.minBy;
import static java.util.stream.Collectors.toList;


/**
 * Класс для высоковольтного отопления (3кВ)
 * выполненый с использованием патерна State
 *
 * @author Baevskiy Ilya
 */
public class HighVoltageHeating implements HeatingState {


    private final HighVoltageHeatingSettings settings;


    @Autowired
    @Qualifier( "outsideTemperatureSensor" )
    private Sensor outsideTemperatureSensor;
    @Autowired
    @Qualifier( "insideTemperatureSensor" )
    private Sensor insideTemperatureSensor;
    @Autowired
    @Qualifier( "boilerTemperatureSensor" )
    private Sensor boilerTemperatureSensor;
    @Autowired
    @Qualifier( "establishedTemperatureSensor" )
    private Sensor establishedTemperatureSensor;
    @Autowired
    private Boiler boiler;
    @Autowired
    private Conditioner conditioner;
    // Первая группа тенов
    private Heater firstGroup;
    // Вторая группа тенов
    private Heater secondGroup;

    // Входящее значение регистра первой группы
    private BooleanProperty group1InputState;
    // Входящее значение регистра второй группы
    private BooleanProperty group2InputState;
    // Входящее значение регистра 3кв
    private BooleanProperty v3kVInputState = new SimpleBooleanProperty(false);
    // Состояние ВВО
    private BooleanBinding heatingState;
    //Состояние ошибки для первой группы
    private BooleanProperty group1ErrorState;
    //Состояние ошибки для второй группы
    private BooleanProperty group2ErrorState;

    // Нет 3кв
    private BooleanBinding no3Kv;

    // Состояния которые бывают у ВВО
    // Состояние выключенное
    private HeatingState offStateHVH = new OffStateHVH();
    // Состояние когда работает одна группа
    private HeatingState singleStateHVH = new SingleStateHVH();
    // Состояние когда работает две группы
    private HeatingState doubleStateHVH = new DoubleStateHVH();
    // Состояние когда обе группы в ошибке
    private HeatingState errorStateHVH = new ErrorStateHVH();

    // Текущее состояние
    private HeatingState currentState;
    // Биндинг должно ли работать отопление или нет
    private BooleanBinding mustBeOn1;
    // Биндинг должна ли работать вторая группа или нет
    private BooleanBinding mustBeOn2;


    public HighVoltageHeating( HighVoltageHeatingSettings settings ) {
        this.settings = settings;

        firstGroup = new Heater();
        secondGroup = new Heater();

        group1InputState = firstGroup.inputState;
        group2InputState = secondGroup.inputState;

        group1ErrorState = firstGroup.errorState;
        group2ErrorState = secondGroup.errorState;

        heatingState = group1InputState.or(group2InputState);
        currentState = offStateHVH;
    }

    @Override
    public void process() {

        // Проверяем в Error ли группы
        if ( group1ErrorState.and(group2ErrorState).get() ) {
            // Если обе стоят в Error
            currentState = errorStateHVH;
            return;
        }
        currentState.process();
    }

    @Override
    public void reset() {
        firstGroup.reset();
        secondGroup.reset();
        currentState = offStateHVH;
    }

    public void init() {
        // Биндинг температуры колтла
        DoubleBinding temperature = establishedTemperatureSensor.lastValueProperty()
                .subtract(settings.HVH_FORMULA_SUBSTRACT_ESTABLISHED_1GROUP)
                .multiply(settings.HVH_FORMULA_MULTIPLER_1GROUP)
                .subtract(outsideTemperatureSensor.averageValueProperty().multiply(settings.HVH_FORMULA_MULTIPLER_2GROUP))
                .add(establishedTemperatureSensor.lastValueProperty()
                        .subtract(insideTemperatureSensor.averageValueProperty())
                        .multiply(settings.HVH_FORMULA_MULTIPLER_3GROUP));
        // Биндинг температуры включения
        NumberBinding temperatureSwitchOn = temperature.add(settings.HVH_FORMULA_BASE_BOILER_TEMP_FOR_ON);
        // Биндинг температуры выключения
        NumberBinding temperatureSwitchOff = temperature.add(settings.HVH_FORMULA_BASE_BOILER_TEMP_FOR_OFF);


        // Биндинг проверки должно ли работать отопление или нет
        BooleanBinding mustBeOn =
                conditioner.coolerStateProperty().not() // Охлаждение выключено
                        // Тн.в. <= 5 и Тсал. < (Tуст - 2)
                        .and(outsideTemperatureSensor.averageValueProperty().lessThanOrEqualTo(settings.HVH_OUTSIDE_TEMPERATURE_ON)
                                .and(insideTemperatureSensor.averageValueProperty().lessThan(establishedTemperatureSensor.averageValueProperty().subtract(settings.HVH_HYSTERESIS_ESTABLISHED_FOR_ON))))
                        // Ткот < Твкл
                        .and(boilerTemperatureSensor.averageValueProperty().lessThan(temperatureSwitchOn))
                        .and(boiler.highTemperatureErrorProperty().not());

        BooleanBinding mustBeOff =
                outsideTemperatureSensor.averageValueProperty().greaterThan(settings.HVH_OUTSIDE_TEMPERATURE_OFF)
                        .or(insideTemperatureSensor.averageValueProperty().greaterThan(establishedTemperatureSensor.lastValueProperty().add(settings.HVH_HYSTERESIS_ESTABLISHED_FOR_OFF)))
                        .or(boilerTemperatureSensor.averageValueProperty().greaterThan(temperatureSwitchOff))
                        .or(conditioner.coolerStateProperty())
                        .or(boiler.highTemperatureErrorProperty());


        // Биндинг включения минимум одной группы
        mustBeOn1 = Bindings
                .when(heatingState.not()) // если ничего не включено
                .then(mustBeOn) // биндим условие включения
                //Биндим условие выключения
                .otherwise(mustBeOff.not());

        //Биндинг включения второй группы
        mustBeOn2 = mustBeOn1.and(
                Bindings.when(group1InputState.and(group2InputState))
                        // Проверка надо ли отключать(Условия отключения)
                        .then(outsideTemperatureSensor.averageValueProperty().greaterThan(settings.HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_OFF).not())
                        // Проверка надо ли включать (условия включения)
                        .otherwise(group1InputState.or(group2InputState).and(outsideTemperatureSensor.averageValueProperty().lessThan(settings.HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_ON)))
        );

        no3Kv = mustBeOn1.and(v3kVInputState.not());

    }

    public BooleanBinding no3KvProperty() {
        return no3Kv;
    }
    public BooleanProperty v3kVInputStateProperty() {
        return v3kVInputState;
    }
    public BooleanProperty group1InputStateProperty() {
        return group1InputState;
    }
    public BooleanProperty group2InputStateProperty() {
        return group2InputState;
    }
    public BooleanProperty group1OutputStateProperty() {
        return firstGroup.outputStateProperty();
    }
    public BooleanProperty group2OutputStateProperty() {
        return secondGroup.outputStateProperty();
    }
    public BooleanProperty group1ErrorStateProperty() {
        return group1ErrorState;
    }
    public BooleanProperty group2ErrorStateProperty() {
        return group2ErrorState;
    }

    public BooleanBinding heatingStateProperty() {
        return heatingState;
    }

    /**
     * Класс-состояние (OFF) высоковольтного отопления
     *
     * @author Baevskiy Ilya
     */
    private class OffStateHVH implements HeatingState {
        @Override
        public void process() {
            // Проверяем надо ли включать и есть ли 3кв
            if ( mustBeOn1.get() && v3kVInputState.get() ) {
                // Берем группу для включения
                Optional<Heater> heaterGroup = Stream.of(firstGroup, secondGroup)
                        // Фильтруем которые не в ошибке
                        .filter(heater -> !heater.isErrorState())
                        // Берем ту группу которая меньше всего включалась
                        .collect(minBy(comparingInt(Heater::getCountOfSwitchOn)));
                // Пытаемся включить группу
                heaterGroup.ifPresent(Heater::on);

                // Проверяем включилась группа или нет
                heaterGroup.filter(Heater::isInputState)
                        .ifPresent(heater -> currentState = singleStateHVH);
            } else {
                // Если нет необходимости включать или нет 3кВ
                // Отключаем все группы
                firstGroup.off();
                secondGroup.off();
            }
        }
    }

    /**
     * Класс-состояние (одинарный режим, если включено
     * только один тен отопления) высоковольтного отопления
     *
     * @author Baevskiy Ilya
     */
    private class SingleStateHVH implements HeatingState {
        @Override
        public void process() {
            // Проверяем включено ли две группы
            if ( secondGroup.switchOnBinding.and(firstGroup.switchOnBinding).get() ) {
                currentState = doubleStateHVH;
                firstGroup.on();
                secondGroup.on();
                return;
            }

            // Проверяем должно ли быть включено
            if ( mustBeOn1.get() && v3kVInputState.get() ) {
                // Проверяем нужно ли включить вторую группу
                if ( mustBeOn2.get() ) {
                    // Если нужно включить вторую группу
                    Stream.of(firstGroup, secondGroup)
                            // Фильтруем ненужные группу
                            .filter(heater -> !heater.isErrorState())
                            .filter(heater -> !heater.isInputState())
                            .findFirst()
                            .ifPresent(Heater::on);
                } else {
                    Stream.of(firstGroup, secondGroup)
                            .filter(Heater::isOutputState)
                            .filter(heater -> !heater.isInputState())
                            .filter(heater -> !heater.isErrorState())
                            .findFirst()
                            .ifPresent(Heater::off);
                }
            } else {
                // Если необходимо выключить все,
                // Выключаем
                firstGroup.off();
                secondGroup.off();
                // переходим в состояние ОФФ
                currentState = offStateHVH;
            }
        }
    }


    /**
     * Класс-состояние (двойной режим, если включено
     * оба тена отопления) высоковольтного отопления
     *
     * @author Baevskiy Ilya
     */
    private class DoubleStateHVH implements HeatingState {
        @Override
        public void process() {
            if ( v3kVInputState.get() && mustBeOn1.get() ) {
                if ( !mustBeOn2.get() ) {
                    // Если необходимо выключить только вторую группу
                    // Выключаем
                    Stream.of(firstGroup, secondGroup)
                            .filter(Heater::isInputState)
                            .collect(minBy(comparingInt(Heater::getCountOfSwitchOn)))
                            .ifPresent(Heater::off);
                    // После отключения меняем текущее состояние на сингл
                    currentState = singleStateHVH;
                }
            } else {
                firstGroup.off();
                secondGroup.off();
                // Устанавливаем текущее состояние ВВО как выключено
                currentState = offStateHVH;
            }
        }
    }

    /**
     * Класс-состояние (состояние ошибки) высоковольтного отопления
     *
     * @author Baevskiy Ilya
     */
    private class ErrorStateHVH implements HeatingState {
    }


    /**
     * Нагреватель (тен)
     * реализовует патерн State
     *
     * @author Baevskiy Ilya
     */
    private class Heater implements Switchable {


        private BooleanProperty inputState = new SimpleBooleanProperty(false);
        private BooleanProperty outputState = new SimpleBooleanProperty(false);
        private BooleanProperty errorState = new SimpleBooleanProperty(false);
        private Supplier<Switchable> stateOffFactory;
        private Supplier<Switchable> stateOnFactory;
        private Supplier<Switchable> stateDelayFactory;
        private Supplier<Switchable> stateErrorFactory;

        private Switchable currentState;
        private int countOfswitchOn = 0;
        BooleanBinding switchOnBinding = inputState.and(outputState);

        Heater() {

            stateDelayFactory = () -> new DelayState(settings.HVH_DELAY_STATE_DELAY);
            stateOffFactory = () -> new OffState(settings.HVH_SWITCH_ON_DELAY, settings.HVH_RESPONSE_DELAY);
            stateOnFactory = OnState::new;
            stateErrorFactory = ErrorState::new;
            currentState = stateOffFactory.get();

        }

        @Override
        public void on() {
            currentState.on();
        }
        @Override
        public void off() {
            currentState.off();
        }

        public void reset() {
            errorState.set(false);
            outputState.set(false);
            currentState = stateOffFactory.get();
        }

        /* GETTERS */
        int getCountOfSwitchOn() {
            return countOfswitchOn;
        }
        boolean isInputState() {
            return inputState.get();
        }
        boolean isErrorState() {
            return errorState.get();
        }
        boolean isOutputState() {
            return outputState.get();
        }
        BooleanProperty outputStateProperty() {
            return outputState;
        }


        /**
         * Класс-состояние (состояние задержки перед
         * очередным включением) тена
         *
         * @author Baevskiy Ilya
         */
        private class DelayState implements Switchable {
            Timer delayTimer;

            public DelayState( int delay ) {
                this.delayTimer = new Timer(delay);
            }
            @Override
            public void on() {
                if ( delayTimer.check() ) {
                    currentState = stateOffFactory.get();
                }
            }
        }

        /**
         * Класс-состояние (выключеное состояние готов к
         * включению) тена
         *
         * @author Baevskiy Ilya
         */
        private class OffState implements Switchable {
            Timer delayBeforeSwitchOnTimer;
            private final int delayResponse;
            /**
             * Current intermediate state of offStateFactory
             * for correct switch on
             */
            private Switchable automatState;

            public OffState( int delayBeforeSwitchOn, int delayResponse ) {
                this.delayBeforeSwitchOnTimer = new Timer(delayBeforeSwitchOn);
                this.delayResponse = delayResponse;
                automatState = new TrySwitchOnState();
            }
            @Override
            public void on() {
                if ( delayBeforeSwitchOnTimer.check() ) {
                    automatState.on();
                }
            }
            public void off() {
                automatState.off();
//                outputState.setValue(false);
//                automatState = new TrySwitchOnState();
            }

            private class TrySwitchOnState implements Switchable {
                @Override
                public void on() {
                    outputState.setValue(true);
                    automatState = new WaitingForResponseSwitchState();
                }
                @Override
                public void off() {
                    outputState.setValue(false);
                    currentState = stateOffFactory.get();
                }
            }

            private class WaitingForResponseSwitchState implements Switchable {
                private Timer waitingTimer = new Timer(delayResponse);

                @Override
                public void on() {
                    if ( inputState.get() ) {
                        currentState = stateOnFactory.get();
                        countOfswitchOn++;
                        return;
                    }
                    if ( waitingTimer.check() ) {
                        errorState.setValue(true);
                        outputState.setValue(false);
                        currentState = stateErrorFactory.get();
                    }
                }

                @Override
                public void off() {
                    outputState.setValue(false);
                    currentState = stateOffFactory.get();
                }
            }
        }

        /**
         * Класс-состояние (включен) тена
         *
         * @author Baevskiy Ilya
         */
        private class OnState implements Switchable {


            @Override
            public void off() {
                outputState.setValue(false);
                currentState = stateDelayFactory.get();
            }
        }

        /**
         * Класс-состояние (ошибка) тена
         *
         * @author Baevskiy Ilya
         */
        private class ErrorState implements Switchable {
        }

    }

    private interface Switchable {
        default void on() {
        }

        default void off() {
        }
    }
}


/**
 * Класс настроек дял ВВО
 */
class HighVoltageHeatingSettings {

    // Время простоя перед следующим включением, ms
    final int HVH_DELAY_STATE_DELAY;
    // Время задержки перед включением
    final int HVH_SWITCH_ON_DELAY;
    // Время максимального ожидания ответа от IO
    // о том что нагреватель включился
    final int HVH_RESPONSE_DELAY;

    // Гистерезис для температуры уставки для включения
    final int HVH_HYSTERESIS_ESTABLISHED_FOR_ON;
    // Гистерезис для температуры уставки для отключения
    final int HVH_HYSTERESIS_ESTABLISHED_FOR_OFF;
    // Тн.р при  которой работаем в дуал режиме
    final int HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_ON;
    // Тн.р при которой переходим в сингл режим
    final int HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_OFF;
    // Тн.р для включнения
    final int HVH_OUTSIDE_TEMPERATURE_ON;
    // Тн.р для отключения
    final int HVH_OUTSIDE_TEMPERATURE_OFF;


    // ПЕРЕМЕННЫЕ ДЛЯ ФОРМУЛЫ
    final int HVH_FORMULA_BASE_BOILER_TEMP_FOR_ON;
    final int HVH_FORMULA_BASE_BOILER_TEMP_FOR_OFF;
    final double HVH_FORMULA_MULTIPLER_1GROUP;
    final double HVH_FORMULA_SUBSTRACT_ESTABLISHED_1GROUP;
    final double HVH_FORMULA_MULTIPLER_2GROUP;
    final double HVH_FORMULA_MULTIPLER_3GROUP;

    public HighVoltageHeatingSettings( Properties properties ) {
        HVH_DELAY_STATE_DELAY = Integer.parseInt(properties.getProperty("HVH_DELAY_STATE_DELAY", "5000"));
        HVH_SWITCH_ON_DELAY = Integer.parseInt(properties.getProperty("HVH_SWITCH_ON_DELAY", "5000"));
        HVH_RESPONSE_DELAY = Integer.parseInt(properties.getProperty("HVH_RESPONSE_DELAY", "5000"));

        HVH_HYSTERESIS_ESTABLISHED_FOR_ON = Integer.parseInt(properties.getProperty("HVH_HYSTERESIS_ESTABLISHED_FOR_ON", "2"));
        HVH_HYSTERESIS_ESTABLISHED_FOR_OFF = Integer.parseInt(properties.getProperty("HVH_HYSTERESIS_ESTABLISHED_FOR_OFF", "1"));
        HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_ON = Integer.parseInt(properties.getProperty("HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_ON", "-10"));
        HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_OFF = Integer.parseInt(properties.getProperty("HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_OFF", "-8"));
        HVH_OUTSIDE_TEMPERATURE_ON = Integer.parseInt(properties.getProperty("HVH_OUTSIDE_TEMPERATURE_ON", "5"));
        HVH_OUTSIDE_TEMPERATURE_OFF = Integer.parseInt(properties.getProperty("HVH_OUTSIDE_TEMPERATURE_OFF", "6"));

        HVH_FORMULA_BASE_BOILER_TEMP_FOR_ON = Integer.parseInt(properties.getProperty("HVH_FORMULA_BASE_BOILER_TEMP_FOR_ON", "35"));
        HVH_FORMULA_BASE_BOILER_TEMP_FOR_OFF = Integer.parseInt(properties.getProperty("HVH_FORMULA_BASE_BOILER_TEMP_FOR_OFF", "55"));
        HVH_FORMULA_MULTIPLER_1GROUP = Double.parseDouble(properties.getProperty("HVH_FORMULA_MULTIPLER_1GROUP", "2.5"));
        HVH_FORMULA_SUBSTRACT_ESTABLISHED_1GROUP = Integer.parseInt(properties.getProperty("HVH_FORMULA_SUBSTRACT_ESTABLISHED_1GROUP", "22"));
        HVH_FORMULA_MULTIPLER_2GROUP = Double.parseDouble(properties.getProperty("HVH_FORMULA_MULTIPLER_2GROUP", "1.5"));
        HVH_FORMULA_MULTIPLER_3GROUP = Double.parseDouble(properties.getProperty("HVH_FORMULA_MULTIPLER_3GROUP", "8"));
    }
}





