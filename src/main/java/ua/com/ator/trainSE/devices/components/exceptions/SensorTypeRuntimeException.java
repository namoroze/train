package ua.com.ator.trainSE.devices.components.exceptions;

/**
 * Исключение неверный тип Сенсора
 * @author Baevskiy Ilya
 */
public class SensorTypeRuntimeException extends RuntimeException {
    public SensorTypeRuntimeException( String message ) {
        super(message);
    }
}
