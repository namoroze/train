package ua.com.ator.trainSE.devices.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.devices.components.State.HeatingState;
import ua.com.ator.trainSE.utils.Timer;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

/**
 * Класс реализаующий низковольтное отопленеие
 * сделан с использованием патерна State
 * @author Baevskiy Ilya
 */
public class LowVoltageHeating implements HeatingState {

    /* All possible state */
    private Supplier<HeatingState> delayAfterSwitchOffStateFactory;
    private Supplier<HeatingState> offStateFactory;
    private Supplier<HeatingState> onStateFactory;
    private Supplier<HeatingState> errorStateFactory;

    /* Current state */
    private HeatingState state;

    /* Output state (register) for switch on/off low voltage heating */
    private BooleanProperty outputState = new SimpleBooleanProperty(false);

    /* Input state low voltage heating(on/off) */
    private BooleanProperty inputState = new SimpleBooleanProperty(false);

    private BooleanProperty lowVoltageHeatingError = new SimpleBooleanProperty(false);


    public LowVoltageHeating( @Autowired LowVoltageConditionOn isConditionOnChecker, @Autowired LowVoltageConditionOff isConditionOffChecker, int delayAfterSwitchOff, int switchOffdelay, int switchOndelay, int responseDelay ) {
        onStateFactory = ()->new OnState(isConditionOffChecker, this::offHeatingHandler, switchOffdelay);
        offStateFactory = ()->new OffState(isConditionOnChecker, this::onHeatingHandler, this::onErrorHandler, switchOndelay, responseDelay);
        delayAfterSwitchOffStateFactory = ()-> new DelayState(delayAfterSwitchOff);
        errorStateFactory = ErrorState::new;
        state = offStateFactory.get();
    }

    @Override
    public void process() {
        state.process();
    }

    @Override
    public void reset() {
        lowVoltageHeatingError.setValue(false);
        outputState.setValue(false);
        state = offStateFactory.get();
    }


    /* Handlers*/
    /**
     * This handler executes when "on state"
     * must be changed to "off state" by conditions
     */
    private void offHeatingHandler() {
        outputState.set(false);
        state = delayAfterSwitchOffStateFactory.get();
    }
    /**
     * This handler executes when "off state"
     * must be changed to "on state" by conditions
     */
    private void onHeatingHandler() {
        outputState.set(true);
    }
    /**
     * This handler executes when "off state"
     * must be changed to on state by conditions
     * but device not confirm "on state" after timeout
     */
    private void onErrorHandler() {
        lowVoltageHeatingError.setValue(true);
        outputState.setValue(false);
        setState(errorStateFactory.get());
    }


    /**
     * Setter for current state
     *
     * @param newState new current state
     */
    private void setState( HeatingState newState ) {
        this.state = newState;
    }
    public BooleanProperty outputStateProperty() {
        return outputState;
    }

    /* Input state accessor */
    public void setInputState( boolean inputState ) {
        this.inputState.set(inputState);
    }

    public BooleanProperty inputStateProperty() {
        return inputState;
    }
    public BooleanProperty errorProperty() {
        return lowVoltageHeatingError;
    }

    /**
     * Класс-состояние (задержка перед очередным
     * включением) низковольтного отопления
     * @author Baevskiy Ilya
     */
    private class DelayState implements HeatingState{
        Timer timer;
        public DelayState( int delay ) {
            this.timer = new Timer(delay);
        }
        @Override
        public void process() {
            if(timer.check())
                setState(offStateFactory.get());
        }
    }
    /**
     * Класс-состояние (ошибка) низковольтного отопления
     * @author Baevskiy Ilya
     */
    private class ErrorState implements HeatingState {
    }
    /**
     * Класс-состояние (выключено)
     * низковольтного отопления.
     * Реализует патерн State
     * @author Baevskiy Ilya
     */
    private class OffState implements HeatingState {

        /**
         * Current intermediate state of offStateFactory
         * for correct switch on
         */
        private HeatingState automatState;


        private final BooleanSupplier conditionSwitchOn;
        private final Runnable actionSwitchOn;
        private final Runnable actionSwitchOnError;

        private final Timer conditionTimerSwitchOn;
        private int delayResponse;

        OffState( BooleanSupplier conditionSwitchOn,
                  Runnable actionSwitchOn,
                  Runnable actionSwitchOnError,
                  int delayConditionBeforeSwitchOn,
                  int delayResponse ) {
            this.conditionSwitchOn = conditionSwitchOn;
            this.actionSwitchOn = actionSwitchOn;
            this.actionSwitchOnError = actionSwitchOnError;

            this.delayResponse = delayResponse;

            this.conditionTimerSwitchOn = new Timer(delayConditionBeforeSwitchOn);
            automatState = new TrySwitchOnState();
        }


        @Override
        public void process() {

            boolean check = conditionTimerSwitchOn.check(conditionSwitchOn);
            if ( check ) {
                automatState.process();
            }
        }
        /**
         * Класс-состояние (можно пробывать включать)
         * состояния OffState
         * Реализует патерн State
         * @author Baevskiy Ilya
         */
        private class TrySwitchOnState implements HeatingState {
            @Override
            public void process() {
                actionSwitchOn.run();
                automatState = new WaitingForResponseSwitchState();
            }
        }
        /**
         * Класс-состояние (ожидание ответа о включении)
         * состояния OffState
         * Реализует патерн State
         * @author Baevskiy Ilya
         */
        private class WaitingForResponseSwitchState implements HeatingState {
            private Timer waitingTimer = new Timer(delayResponse);
            @Override
            public void process() {
                if ( inputState.get() ) {
                    state = onStateFactory.get();
                    return;
                }
                if ( waitingTimer.check() ) {
                    actionSwitchOnError.run();
                }
            }
        }

    }
    /**
     * Класс-состояние (включено)
     * низковольтного отопления.
     * Реализует патерн State
     * @author Baevskiy Ilya
     */
    private class OnState implements HeatingState {

        /* Action for switch to off state */
        private final Runnable actionSwitchOff;
        /* Conditions for switch to off state */
        private BooleanSupplier conditionSwitchOff;
        /* Timer for switch to off state if conditions true for switch off */
        private Timer conditionTimerSwitchOff;

        OnState( BooleanSupplier conditionSwitchOff, Runnable actionSwitchOff, int switchOffDelay ) {
            this.actionSwitchOff = actionSwitchOff;
            this.conditionSwitchOff = conditionSwitchOff;
            conditionTimerSwitchOff = new Timer(switchOffDelay);
        }
        @Override
        public void process() {
            if ( conditionTimerSwitchOff.checkWithReset(conditionSwitchOff) ) {
                actionSwitchOff.run();
            }
        }
    }
}

/**
 * Класс предикат для определения необходимости
 * включать отопление
 * @author Baevskiy Ilya
 */
class LowVoltageConditionOn implements BooleanSupplier {

    private final int OUTSIDE_TEMPERATURE_ON; //15
    private final int HYSTERESIS; // = 2

    @Autowired
    private Generator generator;
    @Autowired
    private Conditioner conditioner;
    @Autowired
    @Qualifier( "outsideTemperatureSensor" )
    private Sensor outsideTemperatureSensor;
    @Autowired
    @Qualifier( "insideTemperatureSensor" )
    private Sensor insideTemperatureSensor;
    @Autowired
    @Qualifier( "establishedTemperatureSensor" )
    private Sensor establishedTemperature;
    //TODO://-------------------


    public LowVoltageConditionOn( final int outsideTemperatureOn, final int hysteresis ) {
        this.OUTSIDE_TEMPERATURE_ON = outsideTemperatureOn;
        this.HYSTERESIS = hysteresis;
    }
    @Override
    public boolean getAsBoolean() {
        return generator.getInputGeneratorState()
                && outsideTemperatureSensor.getAverageValue() < OUTSIDE_TEMPERATURE_ON
                && insideTemperatureSensor.getAverageValue() < ( establishedTemperature.lastValueProperty().getValue() - HYSTERESIS )

                && !conditioner.getCoolerState()
                && ( conditioner.getIsCombine()
                ? conditioner.getIsCombine()
                : !conditioner.getHeaterState());
    }

}
/**
 * Класс предикат для определения необходимости
 * выключать отопление
 * @author Baevskiy Ilya
 */
class LowVoltageConditionOff implements BooleanSupplier {

    private final int OUTSIDE_TEMPERATURE_OFF; //16
    private final int HYSTERESIS;

    @Autowired
    private Generator generator;
    @Autowired
    private Conditioner conditioner;
    @Autowired
    @Qualifier( "outsideTemperatureSensor" )
    private Sensor outsideTemperatureSensor;
    @Autowired
    @Qualifier( "insideTemperatureSensor" )
    private Sensor insideTemperatureSensor;

    //TODO::// Определится сенсор или проперти
    @Autowired
    @Qualifier( "establishedTemperatureSensor" )
    private Sensor establishedTemperatureSensor;
    //TODO://-------------------


    public LowVoltageConditionOff( int outsideTemperatureOff, int hysteresis ) {
        this.HYSTERESIS = hysteresis;
        this.OUTSIDE_TEMPERATURE_OFF = outsideTemperatureOff;
    }
    @Override
    public boolean getAsBoolean() {
        return !generator.getInputGeneratorState()
                || insideTemperatureSensor.getAverageValue() > ( establishedTemperatureSensor.lastValueProperty().getValue() + HYSTERESIS )
                || outsideTemperatureSensor.getAverageValue() >= OUTSIDE_TEMPERATURE_OFF
                || conditioner.getCoolerState()
                || ( !conditioner.getIsCombine() && conditioner.getHeaterState() );
    }


}

