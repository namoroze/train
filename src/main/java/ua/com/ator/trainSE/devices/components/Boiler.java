package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.utils.Timer;

/**
 * Компонент котел
 *
 * Зависимости: Сенсор температуры котла (*С)
 * События: Температура котла выше > XX*C
 *
 * @author Baevskiy Ilya
 */
public class Boiler {

    /**
     * Предельное значение температуры выше которого срабатывает
     * ошибка "Температура котла выше >XX"
     */
    private final int HIGH_TEMPERATURE_ERROR_VALUE;


    /**
     * Сенсор температуры котла
     */
    @Autowired
    @Qualifier( "boilerTemperatureSensor" )
    private Sensor boilerTemperatureSensor;

    /**
     * Биндинг на состояние ошибки "Высокая температура в котле"
     */
    private BooleanBinding highTemperatureError;

    /**
     * Таймер запускаемый при повышенной температуре,
     * для ожидания нормализации значения температуре.
     * И сбрасываемый при понижении температуры
     */
    private Timer highTemperatureErrorTimer;

    /**
     * @param boilerHighTemperatureErrorValue - максимальная температура при которой
     *                                        до которой нет ошибки "Температура в котле >95*"
     * @param boilerHighTemperatureErrorDelay - Задержка на срабатывание ошибки "Температура в котле >95*", ms
     */
    public Boiler( int boilerHighTemperatureErrorValue, int boilerHighTemperatureErrorDelay ) {
        this.HIGH_TEMPERATURE_ERROR_VALUE = boilerHighTemperatureErrorValue;
        this.highTemperatureErrorTimer = new Timer(boilerHighTemperatureErrorDelay);
    }
    /**
     * Метод предназначен для инициализации
     * биндингов ошибок, запускаемый при
     * создании компонента
     */
    public void init() {
        highTemperatureError = Bindings
                .createBooleanBinding(
                        () -> highTemperatureErrorTimer.check(
                                () -> HIGH_TEMPERATURE_ERROR_VALUE <= boilerTemperatureSensor.lastValueProperty().get()),
                        boilerTemperatureSensor.lastValueProperty())
                .and(boilerTemperatureSensor.isErrorProperty().not());
    }


    /**
     * Возвращает Binding ошибки "Температура в котле > 95*"
     * @return Binding указывающий на состояние ошибки
     */
    public BooleanBinding highTemperatureErrorProperty() {
        return highTemperatureError;
    }

    public void reset(){
        highTemperatureErrorTimer.stop();
    }
}
