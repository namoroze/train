package ua.com.ator.trainSE.devices.components.State;

/**
 * Интерфейс отопления для патерна State
 * @author Baevskiy Ilya
 */
public interface HeatingState {

    default void process(){}
    default void reset(){}
}
