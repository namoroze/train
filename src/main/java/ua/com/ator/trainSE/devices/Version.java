package ua.com.ator.trainSE.devices;

import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;

import static ua.com.ator.trainSE.configuration.IOControllerConfiguration.*;


/**
 * Класс пакета версии
 * @author Baevskiy Ilya
 */
final public class Version implements Serializable {

    /**
     * Version of device software
     */
    private final String version;
    /**
     * Date of compiling device software
     */
    private final LocalDate date;

    private Version( String version, LocalDate date ) {
        this.version = version;
        this.date = date;
    }

    /**
     * The factory method creates an instance from the ModbusResponse
     * Register numbers use global ones such as:
     *                      VERSION_REGISTER_VERSION - version register
     *                      VERSION_REGISTER_YEAR - year register
     *                      VERSION_REGISTER_MONTH - month register
     *                      VERSION_REGISTER_DAY - day register
     * @param response - must be instance of ReadInputRegistersResponse
     * @return Composite object from the version in the form of a
     *         string and date in the form of LocalDate
     */
    public static Version create( ModbusResponse response ) {

        try {
            ReadInputRegistersResponse readInputRegistersResponse = (ReadInputRegistersResponse) response;
            String version = String.valueOf(readInputRegistersResponse.getRegisterValue(VERSION_REGISTER_VERSION));
            LocalDate date;
            try {
                date = LocalDate.of(
                        readInputRegistersResponse.getRegisterValue(VERSION_REGISTER_YEAR),
                        readInputRegistersResponse.getRegisterValue(VERSION_REGISTER_MONTH),
                        readInputRegistersResponse.getRegisterValue(VERSION_REGISTER_DAY)
                );
            } catch ( DateTimeException e ) {
                date = LocalDate.MIN;
            }
            return new Version(version, date);
        } catch (IndexOutOfBoundsException e) {
            return new Version(null, null);
        }
    }

    /**
     * Getter for string of version
     * @return
     */
    public String getVersion() {
        return version;
    }
    /**
     * Getter for date of version
     * @return
     */
    public LocalDate getDate() {
        return date;
    }


    @Override
    public String toString() {
        return "Version{" +
                "version='" + version + '\'' +
                ", date=" + date +
                '}';
    }

    /**
     *  The factory method creates empty an instance
     * @return Composite object from the version in the form of a
     *         string and date in the form of LocalDate
     *         string - "-----"
     *         date - LocalDate.MIN
     */
    public static Version emptyVersion(){
       return new Version("-----", LocalDate.of(1970,1,1));
    }
}
