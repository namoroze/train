package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.ator.trainSE.devices.IOControllerDevice;
import ua.com.ator.trainSE.devices.components.exceptions.SensorTypeRuntimeException;
import ua.com.ator.trainSE.ui.interfaces.IndicatorViewable;
import ua.com.ator.trainSE.ui.interfaces.IoDebugViewable;
import ua.com.ator.trainSE.ui.interfaces.SensorViewFactory;
import ua.com.ator.trainSE.ui.property_adapters.SimpleBooleanPropertyAdapter;
import ua.com.ator.trainSE.ui.property_adapters.SimpleDoublePropertyAdapter;
import ua.com.ator.trainSE.ui.views.DoubleBarSensorView;
import ua.com.ator.trainSE.ui.views.SensorView;
import ua.com.ator.trainSE.ui.views.SingleBarSensorView;
import ua.com.ator.trainSE.utils.ForcedDoubleProperty;
import ua.com.ator.trainSE.utils.Timer;

import java.util.LinkedList;
import java.util.List;


/**
 * Class sensor. Can throw events on two changes such as
 * "Error state" and "Change value". This is one of the components
 * of the IO device. Has a built-in mechanisms for normalizing
 * value and for averaging value by algorithm
 *
 * @author Baevskiy Ilya
 */
public class Sensor implements IndicatorViewable, IoDebugViewable {

    static private final AverageAlgorithm DEFAULT_AVERAGE_ALGORITHM = listOfValue -> listOfValue
            .stream()
            .mapToDouble(Double::doubleValue)
            .average()
            .orElse(0);

    @Autowired
    IOControllerDevice ioControllerDevice;

    private final int errorValue;
    /* Input raw value */
    protected final DoubleProperty rawValue;
    /* Normalized average value */
    private final DoubleProperty averageValue;
    /* Normalized last value */
    protected  DoubleBinding lastValue;
    /* Error state */
    private final BooleanBinding isError;
    /* Timer for average value*/
    private final Timer averageTimer;

    /* Type of sensor (double/single bar or other)*/
    private final SensorType type;
    /* Minimal value of sensor */
    private final int minValue;
    /* Maximal value of sensor */
    private final int maxValue;

    /* Title of sensor */
    private final String title;
    /* Units of sensors mnemonic (t/C,A,V,etc)*/
    private final String units;

    /**
     * @param coefficient      - Coefficient for normalizing input value
     * @param offset           - Offset for normalizing input value
     * @param errorValue       - The value that identifies the sensor error
     * @param delayForAverage  - Delay for average, ms
     * @param averageAlgorithm - Algorithm for averaging of value
     */
    public Sensor( double coefficient, double offset, double coeffMaterial, int errorValue, int delayForAverage, String type, int minValue, int maxValue, String title, String units, AverageAlgorithm averageAlgorithm ) {
        this.errorValue = errorValue;
        this.type = SensorType.getType(type);
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.title = title;
        this.units = units;
        this.rawValue = new ForcedDoubleProperty(0);

        //Binding for normalize value by changing
        this.lastValue = rawValue.divide(coefficient).add(offset).multiply(coeffMaterial);
        //Binding for check error by changing
        this.isError = lastValue.greaterThanOrEqualTo(errorValue-1);


        //Averaging fields
        this.averageValue = new SimpleDoubleProperty(0);
        this.averageTimer = new Timer(delayForAverage);
        this.averageAlgorithm = averageAlgorithm;
//        lastValue.addListener(( observable, oldValue, newValue ) -> {
//            if ( oldValue.doubleValue() >= (errorValue -1)) {
//                values.clear();
//                averageTimer.stop();
//            }
//            if ( newValue.doubleValue() < (errorValue-1)  ){
//                if(values.isEmpty())
//                    averageValue.setValue(newValue);
//                averageValueCompute(newValue.doubleValue());}
//
//        });
    }
    public void updateAverageValue(){
        if ( lastValue.doubleValue() >= (errorValue)) {
            values.clear();
            averageTimer.stop();
        }
        if ( lastValue.doubleValue() < (errorValue)  ){
            if(values.isEmpty())
                averageValue.setValue(lastValue.doubleValue());
            averageValueCompute(lastValue.doubleValue());}
    }

    /**
     * Set new value (passes mandatory normalization)
     * and make arithmetic average value
     *
     * @param newValue - raw input value
     */
    public void setRawValue( double newValue ) {
        rawValue.set(newValue);
    }

    /** Return property for averageValue */
    public DoubleProperty averageValueProperty() {
        return averageValue;
    }
    /**
     * Return averageValue
     *
     * @return
     */
    public double getAverageValue() {
        return averageValue.get();
    }

    public DoubleBinding lastValueProperty() {
        return lastValue;
    }
    public BooleanBinding isErrorProperty() {
        return isError;
    }


    /**
     * VIEW INTERFACE IMPLEMENTATION
     */
    private SensorView view; // Instance of SensorView(working like singleton by getter)

    @Override
    public Node getIndicatorView() {

        return getViewInstance().getIndicatorView();
    }
    @Override
    public Node getDebugView() {
        return getViewInstance().getDebugView();
    }
    /**
     * Singletone for SensorView
     *
     * @return
     */
    private SensorView getViewInstance() {
        if ( view == null ) {
            view = type.factory.createProgressBar(
                    new SimpleBooleanPropertyAdapter(isError),
                    new SimpleBooleanPropertyAdapter(ioControllerDevice.connectionErrorProperty()),
                    new SimpleDoublePropertyAdapter(averageValue),
                    title, units, minValue, maxValue);
        }
        return view;
    }



    /**
     * Averaging
     */
    // Sum of last values for averaging
    private List<Double> values = new LinkedList<>();
    private AverageAlgorithm averageAlgorithm;
    /**
     * @param val last value
     */
    private void averageValueCompute( double val ) {
        values.add(val);
        if ( averageTimer.checkWithReset() ) {
            averageValue.set(averageAlgorithm.compute(values));
            values = new LinkedList<>();
        }
    }

    public void resetSensor(){
        rawValue.set(0);
        averageValue.set(0);
        averageTimer.stop();
    }

}


////////////////////////////////////////////////////////////////////////

/**
 * Типы прогрессбара сенсора для отображения
 * @author Baevskiy Ilya
 */
enum SensorType {
    SINGLE(SingleBarSensorView::new),
    DOUBLE(DoubleBarSensorView::new);

    final SensorViewFactory factory;


    /**
     * Constructor
     *
     * @param factory - SensorView ()
     */
    SensorType( SensorViewFactory factory ) {
        this.factory = factory;
    }
    public static SensorType getType( String typeName ) {
        try {
            SensorType sensorType = valueOf(typeName);
            return sensorType;
        } catch ( NullPointerException | IllegalArgumentException e ) {
            throw new SensorTypeRuntimeException("Sensor type error: + ".concat(typeName));
        }
    }
}

/**
 * Интерфейс алгоритма усреднения
 * @author Baevskiy Ilya
 */
@FunctionalInterface
interface AverageAlgorithm {
    double compute( List<Double> listOfValue );
}
/**
 * Класс реализующий алгоритм усреднения
 * стандартным арифметическим способом(сред.ар.)
 * @author Baevskiy Ilya
 */
@Component
class AverageAlgorithmImpl implements AverageAlgorithm {
    @Override
    public double compute( List<Double> listOfValue ) {
        return listOfValue
                .stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0);
    }
}

