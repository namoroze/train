package ua.com.ator.trainSE.devices.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Класс кондиционера
 *
 * @author Baevskiy Ilya
 */
public class Conditioner {

    @Autowired
    private Generator generator;

    // Флаг регистра работы кондиционера с учетом внешней сети/генератора
    private BooleanProperty conditionerIsWorkWithBoardNetwork = new SimpleBooleanProperty(false);

    // Флаг регистра работы охлаждения
    private BooleanProperty coolerState = new SimpleBooleanProperty(false);
    // Флаг регистра работы электрокаллорифера
    private BooleanProperty heaterState = new SimpleBooleanProperty(false);
    // Флаг регистра ошибка контура охлаждения№1
    private BooleanProperty firstCompressorError = new SimpleBooleanProperty(false);
    // Флаг регистра ошибка контура охлаждения№2
    private BooleanProperty secondCompressorError = new SimpleBooleanProperty(false);
    private boolean isCombine;
    private BooleanProperty ventilation = new SimpleBooleanProperty(false);
    private BooleanProperty peregrivVk = new SimpleBooleanProperty(false);
    private BooleanProperty zasorFiltra = new SimpleBooleanProperty(false);
    private BooleanProperty avariaReleDavleniya = new SimpleBooleanProperty(false);
    private BooleanProperty ruchnoyRezhimConditionera = new SimpleBooleanProperty(false);
    private BooleanProperty avariyaPeretvoruvachaK1Ug2 = new SimpleBooleanProperty(false);
    private BooleanProperty avariyaPeretvoruvachaK2Ug3 = new SimpleBooleanProperty(false);
    private BooleanProperty avariyaPeretvoruvachaPvVkUg1 = new SimpleBooleanProperty(false);
    private BooleanProperty peregrivNagrivachivConditionera = new SimpleBooleanProperty(false);
    private BooleanProperty avariyaPv = new SimpleBooleanProperty(false);

    public Conditioner(boolean isCombine) {
        this.isCombine = isCombine;
    }

    public void init() {
        conditionerIsWorkWithBoardNetwork.bind(generator.inputGeneratorStateProperty());
    }

    public void setPeregrivNagrivachivConditionera(boolean peregrivNagrivachivConditionera) {
        this.peregrivNagrivachivConditionera.set(peregrivNagrivachivConditionera);
    }

    public void setAvariyaPv(boolean avariyaPv) {
        this.avariyaPv.set(avariyaPv);
    }

    public void setCoolerState(boolean coolerState) {
        this.coolerState.set(coolerState);
    }

    public void setHeaterState(boolean heaterState) {
        this.heaterState.set(heaterState);
    }

    public void setFirstCompressorError(boolean firstCompressorError) {
        this.firstCompressorError.set(firstCompressorError);
    }

    public void setSecondCompressorError(boolean secondCompressorError) {
        this.secondCompressorError.set(secondCompressorError);
    }

    public void setVentilation(boolean ventilation) {
        this.ventilation.set(ventilation);
    }

    public void setPeregrivVk(boolean peregrivVk) {
        this.peregrivVk.set(peregrivVk);
    }

    public void setZasorFiltra(boolean zasorFiltra) {
        this.zasorFiltra.set(zasorFiltra);
    }

    public void setAvariaReleDavleniya(boolean avariaReleDavleniya) {
        this.avariaReleDavleniya.set(avariaReleDavleniya);
    }

    public void setRuchnoyRezhimConditionera(boolean ruchnoyRezhimConditionera) {
        this.ruchnoyRezhimConditionera.set(ruchnoyRezhimConditionera);
    }

    public void setAvariyaPeretvoruvachaK1Ug2(boolean avariyaPeretvoruvachaK1Ug2) {
        this.avariyaPeretvoruvachaK1Ug2.set(avariyaPeretvoruvachaK1Ug2);
    }

    public void setAvariyaPeretvoruvachaK2Ug3(boolean avariyaPeretvoruvachaK2Ug3) {
        this.avariyaPeretvoruvachaK2Ug3.set(avariyaPeretvoruvachaK2Ug3);
    }

    public void setAvariyaPeretvoruvachaPvVkUg1(boolean avariyaPeretvoruvachaPvVkUg1) {
        this.avariyaPeretvoruvachaPvVkUg1.set(avariyaPeretvoruvachaPvVkUg1);
    }

    public BooleanProperty peregrivNagrivachivConditioneraProperty() {
        return peregrivNagrivachivConditionera;
    }
    public BooleanProperty avariyaPvProperty() {
        return avariyaPv;
    }

    public boolean getCoolerState() {
        return coolerState.get();
    }

    public BooleanProperty coolerStateProperty() {
        return coolerState;
    }

    public boolean getHeaterState() {
        return heaterState.get();
    }

    public BooleanProperty heaterStateProperty() {
        return heaterState;
    }

    public boolean getFirstCompressorError() {
        return firstCompressorError.get();
    }

    public BooleanProperty firstCompressorErrorProperty() {
        return firstCompressorError;
    }

    public boolean getSecondCompressorError() {
        return secondCompressorError.get();
    }

    public BooleanProperty secondCompressorErrorProperty() {
        return secondCompressorError;
    }

    public boolean getIsCombine() {
        return isCombine;
    }

    public BooleanProperty ventilationProperty() {
        return ventilation;
    }

    public BooleanProperty peregrivVkProperty() {
        return peregrivVk;
    }

    public BooleanProperty zasorFiltraProperty() {
        return zasorFiltra;
    }

    public BooleanProperty avariaReleDavleniyaProperty() {
        return avariaReleDavleniya;
    }

    public BooleanProperty ruchnoyRezhimConditioneraProperty() {
        return ruchnoyRezhimConditionera;
    }

    public BooleanProperty avariyaPeretvoruvachaK1Ug2Property() {
        return avariyaPeretvoruvachaK1Ug2;
    }

    public BooleanProperty avariyaPeretvoruvachaK2Ug3Property() {
        return avariyaPeretvoruvachaK2Ug3;
    }

    public BooleanProperty avariyaPeretvoruvachaPvVkUg1Property() {
        return avariyaPeretvoruvachaPvVkUg1;
    }

}
