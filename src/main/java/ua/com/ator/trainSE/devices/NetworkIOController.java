package ua.com.ator.trainSE.devices;

import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.WriteMultipleRegistersRequest;
import net.wimpi.modbus.procimg.SimpleRegister;
import net.wimpi.modbus.util.BitVector;
import ua.com.ator.trainSE.connectivity.ModbusCommunicationService;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static ua.com.ator.trainSE.configuration.IOControllerConfiguration.*;


/**
 * Класс отвечающий за комуникацию с БВВ
 *
 * @author Baevskiy Ilya
 */
class NetworkIOController {

    private final ModbusCommunicationService communicationService;
    private final ModbusRequest POLLING_REQUEST;
    private final ModbusRequest VERSION_REQUEST;
    private final WriteMultipleRegistersRequest INSTALL_REQUEST;
    private volatile boolean stoped;

    private IOControllerInstallPacket lastInstallPackage = new IOControllerInstallPacket();


    /* Executor for device inner task */
    private final ScheduledExecutorService attemptSchedulerExecutor = Executors.newSingleThreadScheduledExecutor((runnable) -> new Thread(runnable, "IO Controller"));


    private final static PacketFactory<PollingIOControllerObject, ModbusResponse> pullingPacketFactory = PollingIOControllerObject::create;
    private final static PacketFactory<Version, ModbusResponse> versionPacketFactory = Version::create;


    NetworkIOController(ModbusCommunicationService communicationService, int deviceAddress) {
        this.communicationService = communicationService;

        /*Initialization of pulling request */
        POLLING_REQUEST = new ReadInputRegistersRequest(POLLING_REGISTER_START, POLLING_REGISTERS_COUNT);
        POLLING_REQUEST.setUnitID(deviceAddress);

        /*Initialization of version request */
        VERSION_REQUEST = new ReadInputRegistersRequest(VERSION_REGISTER_START, VERSION_REGISTERS_COUNT);
        VERSION_REQUEST.setUnitID(deviceAddress);

        /*Initialization of install request */
        INSTALL_REQUEST = new WriteMultipleRegistersRequest();
        INSTALL_REQUEST.setUnitID(deviceAddress);
        INSTALL_REQUEST.setReference(INSTALL_FIRST_REGISTER);
    }

    void disconectDevice() {
        stoped = true;
    }

    void connectDevice(
            Consumer<Version> versionConsumer,
            Consumer<PollingIOControllerObject> pollConsumer,
            Consumer<Throwable> exceptionHandler

    ) {
        stoped = false;
        connect(versionConsumer, pollConsumer, exceptionHandler);
    }

    /**
     * Шлет настройки в устройство через МодБас
     *
     * @param newPacket
     * @param exceptionConsumer
     */
    void sendSettings(IOControllerInstallPacket newPacket, Consumer<Throwable> exceptionConsumer) {
        if (lastInstallPackage.equals(newPacket))
            return;

        lastInstallPackage = newPacket;

        INSTALL_REQUEST.setRegisters(createSettingsRegisters(newPacket));
        communicationService.execute(
                INSTALL_REQUEST,
                (response, throwable) -> {
                    if (throwable != null) {
                        exceptionConsumer.accept(throwable);
                    }
                    return response;
                });
    }

    /**
     * Собирает регистры для установки на устройство
     *
     * @param packet
     * @return
     */
    private SimpleRegister[] createSettingsRegisters(IOControllerInstallPacket packet) {

        SimpleRegister discreteRegister = new SimpleRegister();
        BitVector values = new BitVector(16);

        values.setBit(INSTALL_REGISTER_HEATING1, packet.isHeatingElement1());
        values.setBit(INSTALL_REGISTER_HEATING2, packet.isHeatingElement2());
        values.setBit(INSTALL_REGISTER_PUMP, packet.isPumpState());
        values.setBit(INSTALL_REGISTER_LOW_VOLTAGE_HEATING, packet.isLowVoltageHeating());
        values.setBit(INSTALL_REGISTER_CRASH, packet.isCrash());
        values.setBit(7, false); // на будущее
        discreteRegister.setValue(values.getBytes()[0]);

        return new SimpleRegister[]{discreteRegister};
    }

    /**
     * Запрашивает версию и если версия удачно получена запускает пуллинг,
     * если не получена то повторяет попытку получить версию устройства
     *
     * @param versionConsumer
     * @param pollConsumer
     * @param exceptionConsumer
     */
    private void connect(
            Consumer<Version> versionConsumer,
            Consumer<PollingIOControllerObject> pollConsumer,
            Consumer<Throwable> exceptionConsumer) {
        if (!stoped) {
            getVersion(version -> {
                versionConsumer.accept(version);
                poll(pollConsumer, exceptionConsumer);
            }, throwable -> {
                exceptionConsumer.accept(throwable);
                //recursive attempt connection
                Timer timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        connect(versionConsumer, pollConsumer, exceptionConsumer);
                    }
                };
                timer.schedule(timerTask, 2000);
            });
        }

    }

    /**
     * Обработчик успешного ответа устройства
     *
     * @param consumer
     * @param result
     * @param <T>
     */
    private <T> void requestSuccessHandler(Consumer<T> consumer, T result) {
        consumer.accept(result);
    }

    /**
     * Обработчик Exception запроса устройству
     *
     * @param exceptionConsumer
     * @param throwable
     */
    private void requestExceptionHandler(Consumer<Throwable> exceptionConsumer, Throwable throwable) {
        exceptionConsumer.accept(throwable);
    }

    /**
     * Метод для получения пакета версии от устройства
     *
     * @param consumer
     * @param exceptionConsumer
     */
    private void getVersion(Consumer<Version> consumer, Consumer<Throwable> exceptionConsumer) {
        createTask(
                VERSION_REQUEST,
                consumer,
                exceptionConsumer,
                versionPacketFactory::create,
                null
        );
    }

    /**
     * Метод пулинга пакетов
     *
     * @param consumer
     * @param exceptionConsumer
     */
    private void poll(Consumer<PollingIOControllerObject> consumer, Consumer<Throwable> exceptionConsumer) {
        createTask(
                POLLING_REQUEST,
                consumer,
                exceptionConsumer,
                pullingPacketFactory::create,
                () -> {
                    if (!stoped)
                        attemptSchedulerExecutor.schedule(
                                () -> poll(consumer, exceptionConsumer),
                                POLLING_INTERVAL,
                                TimeUnit.MILLISECONDS);
                }
        );
    }

    /**
     * Создать таск в модбас сервисе
     *
     * @param request
     * @param consumer
     * @param exceptionConsumer
     * @param factory
     * @param callback
     * @param <T>
     */
    private <T> void createTask(ModbusRequest request, Consumer<T> consumer, Consumer<Throwable> exceptionConsumer, Function<ModbusResponse, T> factory, Runnable callback) {
        communicationService.execute(
                request,
                getResponseHandler(consumer, exceptionConsumer, factory, callback));
    }


    private <T> BiFunction<ModbusResponse, Throwable, ModbusResponse> getResponseHandler(Consumer<T> consumer, Consumer<Throwable> exceptionConsumer, Function<ModbusResponse, T> factory, Runnable callback) {
        return (response, throwable) -> {
            if (response != null) {
                requestSuccessHandler(consumer, factory.apply(response));
            }
            if (throwable != null) {
                requestExceptionHandler(exceptionConsumer, throwable);
            }
            if (callback != null) {
                callback.run();
            }
            return response;
        };
    }

    public void close() {
        attemptSchedulerExecutor.shutdownNow();
    }

}
