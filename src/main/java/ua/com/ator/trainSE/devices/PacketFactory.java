package ua.com.ator.trainSE.devices;

/**
 * Интерфейс абстрактная фабрика входящих пакетов
 * @author Baevskiy Ilya
 */
@FunctionalInterface
public interface PacketFactory<R, T>{
    R create(T response);
}
