package ua.com.ator.trainSE.devices;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.WriteSingleRegisterRequest;
import net.wimpi.modbus.procimg.SimpleRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.connectivity.ModbusCommunicationService;
import ua.com.ator.trainSE.devices.components.Sensor;

import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


/**
 * Класс для работы с платой кондиционера
 * @author Baevskiy Ilya
 */
public class ConditionerBoard {
    @Autowired
    @Qualifier( "establishedTemperatureSensor" )
    private Sensor establishedTemperatureSensor;
    private final NetworkConditionerController networkController;
    private final ObjectProperty<Version> version = new SimpleObjectProperty<>(Version.emptyVersion());


    public ConditionerBoard( ModbusCommunicationService communicationService, final int deviceAddress, final int versionRegisterStart, final int versionRegisterStop, final int temperatureRegister ) {
        networkController = new NetworkConditionerController(communicationService, deviceAddress, versionRegisterStart, versionRegisterStop, temperatureRegister);
    }
    public ConditionerBoard( ModbusCommunicationService communicationService, Properties properties ) {
        this(
                communicationService,
                Integer.parseInt(properties.getProperty("DEVICE_ADDRESS", "16")),
                Integer.parseInt(properties.getProperty("VERSION_START_REGISTER", "28")),
                Integer.parseInt(properties.getProperty("VERSION_REGISTERS_COUNT", "4")),
                Integer.parseInt(properties.getProperty("ESTABLISHED_TEMPERATURE_REGISTER", "1"))
        );
    }
    public void init() {
        // Биндимся на событие при изменении температуры уставки
//        establishedTemperatureSensor.averageValueProperty().addListener(
//                ( observable, oldValue, newValue ) -> networkController.sendTemperature(newValue.byteValue()));
        // Пытаемся соединится с платой запрашивая версию
        networkController.connect();
    }

    // Гетер для версии
    public Version getVersion() {
        return version.get();
    }
    /**
     * Проперти для объекта версии устройства
     *
     * @return объект версии
     */
    public ObjectProperty<Version> versionProperty() {
        return version;
    }
    /**
     * Пропертя отображающее есть ли ошибка в соединении с устройством.
     *
     * @return
     */
    public BooleanProperty connectionErrorProperty() {
        return networkController.connectionError;
    }


    /**
     * Класс обвертка для Modbus сервиса у которого есть необходимые
     * методы для отправки и получения данных для конкретного устройства,
     * в данном случае промежуточной платы кондиционера
     * Умеет получать версию
     * Умеет устанавливать температуру
     */
    private class NetworkConditionerController {
        private final ModbusCommunicationService communicationService;
        private final WriteSingleRegisterRequest TEMPERATURE_SET_REQUEST;
        private final ModbusRequest VERSION_REQUEST;
        private final BooleanProperty connectionError = new SimpleBooleanProperty(true);
        private final ScheduledExecutorService attemptSchedulerExecutor = Executors.newSingleThreadScheduledExecutor((runnable ) -> new Thread(runnable, "Conditioner"));


        NetworkConditionerController( ModbusCommunicationService communicationService, int deviceAddress, int versionRegisterStart, int versionRegisterCount, int temperatureRegister ) {
            this.communicationService = communicationService;
            VERSION_REQUEST = new ReadInputRegistersRequest(versionRegisterStart, versionRegisterCount);
            VERSION_REQUEST.setUnitID(deviceAddress);
            TEMPERATURE_SET_REQUEST = new WriteSingleRegisterRequest();
            TEMPERATURE_SET_REQUEST.setUnitID(deviceAddress);
            TEMPERATURE_SET_REQUEST.setReference(temperatureRegister);
        }

        private void connect() {

            getVersion(
                    ( ver ) -> {
                        version.setValue(ver);
                        sendTemperature();
                    },
                    throwable -> {
                        Timer timer = new Timer();
                        TimerTask timerTask = new TimerTask() {
                            @Override
                            public void run() {
                                connect();
                            }
                        };
                        timer.schedule(timerTask, 2000);
                    }

            );
        }
        /**
         * Отправляет новую температуру уставки в плату кондиционера
         *
         */
        void sendTemperature() {
            TEMPERATURE_SET_REQUEST.setRegister(new SimpleRegister((short)(Math.round(establishedTemperatureSensor.getAverageValue()))));
            communicationService.execute(
                    TEMPERATURE_SET_REQUEST,
                    ( response, throwable ) -> {
                        if ( throwable != null ) {
                            connectionError.setValue(true);
                        } else {
                            connectionError.setValue(false);
                        }
                        attemptSchedulerExecutor.schedule(this::sendTemperature,1000, TimeUnit.MILLISECONDS);
                        return null;
                    });

        }
        /**
         * Получает версию с платы кондиционера
         *
         * @param consumer
         * @param exceptionConsumer
         */
        void getVersion( Consumer<Version> consumer, Consumer<Throwable> exceptionConsumer ) {
            communicationService.execute(
                    VERSION_REQUEST,
                    ( response, throwable ) -> {
                        if ( throwable != null ) {
                            connectionError.setValue(true);
                            exceptionConsumer.accept(throwable);
                        }
                        if ( response != null ) {
                            connectionError.setValue(false);
                            consumer.accept(Version.create(response));
                        }
                        return response;
                    });
        }


    }
}

