package ua.com.ator.trainSE.devices.components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Компонент насос.
 * Включается в случаее если температару ниже
 * переданной в конструкторе и состояние ВВ
 * отопления "Включено"
 *
 * Зависимости: Сенсор наружней температуры (*С)
 *              используется усредненное значение для анализа.
 *
 * Выходные регистры: outputState - состояние насоса (Включен/отключен)
 *
 * @author Baevskiy Ilya
 */
public class Pump {


    @Autowired
    @Qualifier( "outsideTemperatureSensor" )
    private Sensor outsideTemperatureSensor;

    @Autowired
    HighVoltageHeating highVoltageHeating;
    /**
     * Биндинг для состояния насоса
     */
    private BooleanProperty outputState = new SimpleBooleanProperty(false);
    /**
     * Температура ниже которой должен включатся насос
     * при включенном ВВ отоплении
     */
    private final int MAX_TEMPERATURE_FOR_SWITCH_ON;


    public Pump( int pumpMaxTemperatureForSwitchOn ) {
        this.MAX_TEMPERATURE_FOR_SWITCH_ON = pumpMaxTemperatureForSwitchOn;
    }

    /**
     * Метод предназначен для инициализации
     * биндингов, запускаемый при
     * создании компонента
     */
    public void init() {
        outputState.bind(highVoltageHeating.heatingStateProperty()
                .and(outsideTemperatureSensor.averageValueProperty()
                        .lessThan(MAX_TEMPERATURE_FOR_SWITCH_ON)));
    }
    /**
     * Вовзращает исходящее состояние насоса
     * @return - состояние насоса
     *                          true  - включен
     *                          false - отключен
     */
    public boolean getOutputState() {
        return outputState.get();
    }

    public BooleanProperty outputStateProperty() {
        return outputState;
    }
}
