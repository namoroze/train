package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.utils.Timer;

/**
 * Компонент бортовая сеть
 * Зависимости: Сенсор напряжения бортовой сети (Вольты)
 * События: Ошибка изоляции
 *          Высокое напряжение
 *
 * Входные регистры: insulationResistanceError - R - повреждение изоляции
 *                   highVoltageInputError - Umax - превышено напряжение
 *
 * Выходные регистры: highVoltageOutputError - Umax - превышено напряжение
 *                    outputRPN1Error - RPN1 - (Не работает)
 *
 * @author Baevskiy Ilya
 */
public class BoardNetwork {

    /**
     * Предельное значение напряжения выше которого срабатывает
     * ошибка "Превышено напряжение борт сети"
     */
    private final int HIGH_VOLTAGE_ERROR_VALUE;


    /**
     * Сенсор напряжения от которого зависят
     * состояния ошибок.
     */
    @Autowired
    @Qualifier( "networkVoltageSensor" )
    private Sensor voltageBoardSensor;

    /**
     * Ошибка "R изоляции < 1кОм" (устанавливается входящим регистром (Rmin))
     */
    private final BooleanProperty insulationResistanceError = new SimpleBooleanProperty(false);
    /**
     * Ошибка высокое напряжение(Umax) (Umax ZRK устанавливается
     * входящим регистром).
     */
    private final BooleanProperty highVoltageInputError = new SimpleBooleanProperty(false);
    /**
     * Исходящее состоянее ошибки "Высокое напряжение"  (Umax биндинг
     * на сенсор напряжения) определенное програмным путем.
     */
    private BooleanBinding highVoltageOutputError;

    /**
     * Биндинг на ошибку высокого напряжения (highVoltageInputError | highVoltageOutputError)
     *  true - если highVoltageOutputError == true
     *           или highVoltageInputError == true
     */
    private BooleanBinding highVoltageError;
    /**
     * Таймер запускаемый при повышенном напряжении,
     * для ожидания нормализации значения напряжения.
     * И сбрасываемый после того как значение напряжения
     * нормализовалось.
     */
    private final Timer highVoltageErrorTimer;

    /**
     * @param valueOfHighVoltageError - Напряжение значение выше которого свидетельствует об ошибке "Высокое напряжение"
     * @param delayOfHighVoltageError - Задержка на срабатывание ошибки "Высокое напряжение", ms
     */
    public BoardNetwork( int valueOfHighVoltageError, int delayOfHighVoltageError ) {
        this.HIGH_VOLTAGE_ERROR_VALUE = valueOfHighVoltageError;
        this.highVoltageErrorTimer = new Timer(delayOfHighVoltageError);
    }

    /**
     * Метод предназначен для инициализации
     * биндингов ошибок, запускаемый при
     * создании компонента
     */
    public void init() {
        //Биндинг ошибки "высокое напряжение бортовой сети"
        highVoltageOutputError = Bindings
                .createBooleanBinding(
                        () -> highVoltageErrorTimer.check(() -> voltageBoardSensor.lastValueProperty().get() >= HIGH_VOLTAGE_ERROR_VALUE),
                        voltageBoardSensor.lastValueProperty())
                .and(voltageBoardSensor.isErrorProperty().not());

        this.highVoltageError = highVoltageInputError.or(highVoltageOutputError);
    }


    /**
     * Устанавливает входящее значение регистра "Высокое напряжение" (Umax)
     * @param highVoltageInputError - состояние входящего регистра
     */
    public void setHighVoltageInputError( boolean highVoltageInputError ) {
        this.highVoltageInputError.set(highVoltageInputError);
    }
    /**
     * Property входящего значения регистра "Высокое напряжение" (Umax)
     * @return - объект BooleanProperty
     */
    public BooleanProperty highVoltageInputErrorProperty() {
        return highVoltageInputError;
    }
    /**
     * Возвращает Binding на логическое "или"  входящего и
     * исходящего состояния ошибки "Высокое напряжение"
     * @return - Биндинг highVoltageInputError | highVoltageOutputError
     */
    public BooleanBinding highVoltageErrorProperty() {
        return highVoltageError;
    }

    /**
     * Возвращает Property состояния ошибки "R изоляции < 1кОм"
     * основанной на входящем значении входного регистра(Rmin)
     * @return Property с ошибкой
     */
    public BooleanProperty insulationResistanceErrorProperty() {
        return insulationResistanceError;
    }
    /**
     * Устанавливает состояния ошибки "R изоляции < 1кОм"
     * @param insulationResistanceError - состояние регистра
     */
    public void setInsulationResistanceError( boolean insulationResistanceError ) {
        this.insulationResistanceError.set(insulationResistanceError);
    }

    /**
     * Возвращает исходящее состояние ошибки "Высокое напряжение (Umax)"
     * @return - true  ошибка
     *           false нет ошибки
     */
    public BooleanBinding outputHighVoltageError() {
        return highVoltageOutputError;
    }

    public void reset(){
        highVoltageErrorTimer.stop();
    }

}


