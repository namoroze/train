package ua.com.ator.trainSE.devices.components;


import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.utils.Timer;

/**
 * Компонент генератор
 *
 * Зависимости: currentBatterySensor - сенсор тока батареи(А)
 *              currentBatterySensor - сенсор напряжения борт сети(В)
 *
 * События: dischargedError - "Батарея разряжена"
 *          highCurrentError - "Превышен ток заряда батареи"
 *
 * @author Baevskiy Ilya
 */
public class AccumulatorBattery {

    /**
     * Предельное значение тока заряда батареи, выше которого
     * срабатывает ошибка "Превышен ток заряда батареи"
     */
    private final int HIGH_CURRENT_ERROR_VALUE;

    /**
     * Предельное значение напряжения бортсети, ниже
     * которого срабатывает ошибка "Батарея разряжена"
     */
    private final int DISCHARGE_ERROR_VOLTAGE_VALUE;

    /**
     * Сенсор тока батареи
     */
    @Autowired
    @Qualifier( "currentBatterySensor" )
    private Sensor currentBatterySensor;
    /**
     * Сенсор напряжения борт сети
     */
    @Autowired
    @Qualifier( "networkVoltageSensor" )
    private Sensor boardVoltageSensor;


    /**
     * Таймер запускаемый при пониженном напряжении бортсети,
     * для ожидания нормализации напряжения бортсети.
     * И сбрасываемый после того как значение напряжения нормализовалось
     * Используется для индикации ошибки "Батарея разряжена"
     */
    private Timer dischargedErrorTimer;
    /**
     * Таймер запускаемый при повышенном токе заряда батареи,
     * для ожидания нормализации тока заряда батареи.
     * И сбрасываемый после того как значение тока заряда батареи
     * Используется для индикации ошибки "Превышен ток заряда батареи"
     */
    private Timer highCurrentErrorTimer;


    /**
     * Биндинг указывающий на ошибку "Батарея разряжена"
     */
    private BooleanBinding dischargedError;
    /**
     * Биндинг указывающий на ошибку "Превышен ток заряда батареи"
     */
    private BooleanBinding highCurrentError;

    /**
     * @param voltageDischargeBattery - минимальное напряжение указывающее на ошибку "Батарея разряжена"
     * @param batteryDischargeErrorDelay - задержка при срабатывании ошибки "Батарея разряжена"
     * @param valueOfHighCurrentErrorBattery - максимальное значение тока заряда батарии указывающее
     *                                       на ошибку "Превышен ток заряда батареи"
     * @param batteryHighCurrentDelay - задержка при срабатывании ошибки "Превышен ток заряда батареи"
     */
    public AccumulatorBattery( int voltageDischargeBattery, int batteryDischargeErrorDelay, int valueOfHighCurrentErrorBattery, int batteryHighCurrentDelay ) {

        this.HIGH_CURRENT_ERROR_VALUE = valueOfHighCurrentErrorBattery;
        this.DISCHARGE_ERROR_VOLTAGE_VALUE = voltageDischargeBattery;

        this.dischargedErrorTimer = new Timer(batteryDischargeErrorDelay);
        this.highCurrentErrorTimer = new Timer(batteryHighCurrentDelay);
    }

    /**
     * Метод предназначен для инициализации
     * биндингов ошибок, запускаемый при
     * создании компонента
     */
    public void init() {
        //Биндинг разряда батареи
        dischargedError = Bindings
                .createBooleanBinding(
                        () -> dischargedErrorTimer.check(() -> boardVoltageSensor.lastValueProperty().get() < DISCHARGE_ERROR_VOLTAGE_VALUE),
                        boardVoltageSensor.lastValueProperty())
                .and(boardVoltageSensor.isErrorProperty().not());

        //Биндинг высокого тока
        highCurrentError = Bindings
                .createBooleanBinding(
                        () -> highCurrentErrorTimer.check(() -> currentBatterySensor.lastValueProperty().get() >= HIGH_CURRENT_ERROR_VALUE),
                        currentBatterySensor.lastValueProperty())
                .and(currentBatterySensor.isErrorProperty().not());
    }


    /**
     * Возвращает состояние ошибки "Батарея разряжена"
     *
     * @return состояние ошибки "Батарея разряжена"
     *                  true - ошибка
     *                  false - нет ошибки
     */
    public boolean getDischargedError() {
        return dischargedError.get();
    }
    /**
     * Возвращает Binding указывающий на состояние ошибки "Батарея разряжена"
     * @return - Binding ошибки "Батарея разряжена"
     */
    public BooleanBinding dischargedErrorProperty() {
        return dischargedError;
    }
    /**
     * Вовзвращает состояние ошибки "Превышен ток заряда батареи"
     * @return состояние ошибки "Превышен ток заряда батареи"
     *              true - ошибка
     *              false - нет ошибки
     */
    public boolean isHighCurrentError() {
        return highCurrentError.get();
    }
    /**
     * Возвращает Binding указывающий на состояние
     * ошибки "Превышен ток заряда батареи"
     * @return - Binding ошибки "Превышен ток заряда батареи"
     */
    public BooleanBinding highCurrentErrorProperty() {
        return highCurrentError;
    }

    public void reset(){
        dischargedErrorTimer.stop();
        highCurrentErrorTimer.stop();
    }

}
