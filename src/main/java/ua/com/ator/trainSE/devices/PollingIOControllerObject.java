package ua.com.ator.trainSE.devices;

import net.wimpi.modbus.msg.ModbusResponse;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;

import java.io.Serializable;

import static ua.com.ator.trainSE.configuration.IOControllerConfiguration.*;

/**
 * Класс входящий пакет от БВВ
 *
 * @author Baevskiy Ilya
 */
final class PollingIOControllerObject implements Serializable {//NOPMD

    private final long timestamp = System.currentTimeMillis();

    /**
     * Discrete data
     */
    private final boolean automaticMode; // State of automatic mode
    private final boolean shutdownSignal; // Signal for shutdown ?????

    /* Equipment state */
    private final boolean highVoltageHeatingElement1; // State of high voltage heating element 1
    private final boolean highVoltageHeatingElement2; // State of high voltage heating element 2

    private final boolean lowVoltageHeating;// Low voltage state

    /* Conditioner */
    private final boolean compressorM1; // State of M1 compressor conditioner
    private final boolean compressorM2; // State of M2 compressor conditioner
    private final boolean peregrivNagrivachivConditionera; // State of forced ventilation
    private final boolean isFreezing; // Freezing state
    private final boolean electricalCallorifier; // State of electrical callorifier

    private final boolean avariyaPv; // Conditioner state

    /* Electric state */
    private final boolean highVoltage; // High voltage state
    private final boolean outerOrGeneratorNetwork; // Outer or generator network state(on/off)

    /* Electric error */
    private final boolean crashError; // Signal of crash electrical equipment
    private final boolean highVoltageError; // Voltage more 150V
    private final boolean insulationDamageError; // Damage to insulation (resistance < 1kOm)

    ///////////////////////////private final boolean lowVoltageError; // Voltage less 85V


    private boolean ventilation;
    private boolean peregrivVk;
    private boolean zasorFiltra;
    private boolean avariaReleDavleniya;
    private boolean ruchnoyRezhimConditionera;
    private boolean avariyaPeretvoruvachaK1Ug2;
    private boolean avariyaPeretvoruvachaK2Ug3;
    private boolean avariyaPeretvoruvachaPvVkUg1;
    /**
     * Analog data
     */
    /* Temperatures */
    private final double insideTemperature; // Coupe temperature
    private final double outsideTemperature; // Outside temperature
    private final double airFlowTemperature; // Temperature of air flow
    private final double boilerTemperature; // Boiler temperature

    /* Electrics */
    private final double batteryCurrent; // Current of battery
    private final double generatorCurrent; // Current of generator
    private final double onBoardVoltage; // Voltage of board network


    public PollingIOControllerObject(boolean automaticMode,
                                     boolean shutdownSignal,
                                     boolean highVoltageHeatingElement1,
                                     boolean highVoltageHeatingElement2,
                                     boolean lowVoltageHeating,
                                     boolean compressorM1,
                                     boolean compressorM2,
                                     boolean peregrivNagrivachivConditionera,
                                     boolean isFreezing,
                                     boolean electricalCallorifier,
                                     boolean avariyaPv,
                                     boolean highVoltage,
                                     boolean outerOrGeneratorNetwork,
                                     boolean crashError,
                                     boolean highVoltageError,
                                     boolean insulationDamageError,
                                     boolean ventilation,
                                     boolean peregrivVk,
                                     boolean zasorFiltra,
                                     boolean avariaReleDavleniya,
                                     boolean ruchnoyRezhimConditionera,
                                     boolean avariyaPeretvoruvachaK1Ug2,
                                     boolean avariyaPeretvoruvachaK2Ug3,
                                     boolean avariyaPeretvoruvachaPvVkUg1,
                                     double insideTemperature,
                                     double outsideTemperature,
                                     double airFlowTemperature,
                                     double boilerTemperature,
                                     double batteryCurrent,
                                     double generatorCurrent,
                                     double onBoardVoltage) {
        this.automaticMode = automaticMode;
        this.shutdownSignal = shutdownSignal;
        this.highVoltageHeatingElement1 = highVoltageHeatingElement1;
        this.highVoltageHeatingElement2 = highVoltageHeatingElement2;
        this.lowVoltageHeating = lowVoltageHeating;
        this.compressorM1 = compressorM1;
        this.compressorM2 = compressorM2;
        this.peregrivNagrivachivConditionera = peregrivNagrivachivConditionera;
        this.isFreezing = isFreezing;
        this.electricalCallorifier = electricalCallorifier;
        this.avariyaPv = avariyaPv;
        this.highVoltage = highVoltage;
        this.outerOrGeneratorNetwork = outerOrGeneratorNetwork;
        this.crashError = crashError;
        this.highVoltageError = highVoltageError;
        this.insulationDamageError = insulationDamageError;
        this.ventilation = ventilation;
        this.peregrivVk = peregrivVk;
        this.zasorFiltra = zasorFiltra;
        this.avariaReleDavleniya = avariaReleDavleniya;
        this.ruchnoyRezhimConditionera = ruchnoyRezhimConditionera;
        this.avariyaPeretvoruvachaK1Ug2 = avariyaPeretvoruvachaK1Ug2;
        this.avariyaPeretvoruvachaK2Ug3 = avariyaPeretvoruvachaK2Ug3;
        this.avariyaPeretvoruvachaPvVkUg1 = avariyaPeretvoruvachaPvVkUg1;

        this.insideTemperature = insideTemperature;
        this.outsideTemperature = outsideTemperature;
        this.airFlowTemperature = airFlowTemperature;
        this.boilerTemperature = boilerTemperature;
        this.batteryCurrent = batteryCurrent;
        this.generatorCurrent = generatorCurrent;
        this.onBoardVoltage = onBoardVoltage;
    }


    static public PollingIOControllerObject create(ModbusResponse modbusResponse) {
        ReadInputRegistersResponse response = (ReadInputRegistersResponse) modbusResponse;
        int discreteRegister = response.getRegister(POLLING_DISCRETE_REGISTER).getValue();
        int discreteRegister2 = response.getRegister(POLLING_DISCRETE_REGISTER_RESERVE).getValue();

        return new PollingIOControllerObject(

                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_ATOMATIC_MODE),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_SHUTDOWN_SIG),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_HEATING1),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_HEATING2),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_LOW_HEATING),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_COMPRESSOR_M1),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_COMPRESSOR_M2),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_PEREGRIV_NAGRIVACHIV_CONDITIONERA),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_FREEZING),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_ELECTRO_CALLORIFIER),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_AVARIYA_PV),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_HIGH_VOLTAGE),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_NETWORK_OR_GEN),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_CRASH_ERR),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_HIGH_VOLTAGE_ERR),
                getBitFromInt(discreteRegister, POLLING_DISCRETE_BIT_INSULATION_ERR),

                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_VENTILATION),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_PERGRIV_VK),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_ZASOR_FILTRA),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_AVRIA_RELE_DAVLENIYA),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_RUCHNOY_REZHIM_CONDITIONERA),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K1_UG2),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K2_UG3),
                getBitFromInt(discreteRegister2, POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_PV_VK_UG1),

                response.getRegister(POLLING_ANALOG_REGISTER_INSIDE_TEMP).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_OUTSIDE_TEMP).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_AIR_FLOW_TEMP).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_BOILER_TEMP).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_BATTERY_CURRENT).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_GENERATOR_CURRENT).toShort(),
                response.getRegister(POLLING_ANALOG_REGISTER_NETWORK_VOLTAGE).toShort()

        );
    }


    static private boolean getBitFromInt(int value, int bitIndex) {

        return (0 != ((value & (1 << bitIndex))));
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean isAutomaticMode() {
        return automaticMode;
    }

    public boolean isShutdownSignal() {
        return shutdownSignal;
    }

    public boolean isHighVoltageHeatingElement1() {
        return highVoltageHeatingElement1;
    }

    public boolean isHighVoltageHeatingElement2() {
        return highVoltageHeatingElement2;
    }

    public boolean isLowVoltageHeating() {
        return lowVoltageHeating;
    }

    public boolean isCompressorM1() {
        return compressorM1;
    }

    public boolean isCompressorM2() {
        return compressorM2;
    }

    public boolean isPeregrivNagrivachivConditionera() {
        return peregrivNagrivachivConditionera;
    }

    public boolean isFreezing() {
        return isFreezing;
    }

    public boolean isElectricalCallorifier() {
        return electricalCallorifier;
    }

    public boolean isAvariyaPv() {
        return avariyaPv;
    }

    public boolean isHighVoltage() {
        return highVoltage;
    }

    public boolean isOuterOrGeneratorNetwork() {
        return outerOrGeneratorNetwork;
    }

    public boolean isCrashError() {
        return crashError;
    }

    public boolean isHighVoltageError() {
        return highVoltageError;
    }

    public boolean isInsulationDamageError() {
        return insulationDamageError;
    }

    public boolean isVentilation() {
        return ventilation;
    }

    public boolean isPeregrivVk() {
        return peregrivVk;
    }

    public boolean isZasorFiltra() {
        return zasorFiltra;
    }

    public boolean isAvariaReleDavleniya() {
        return avariaReleDavleniya;
    }

    public boolean isRuchnoyRezhimConditionera() {
        return ruchnoyRezhimConditionera;
    }

    public boolean isAvariyaPeretvoruvachaK1Ug2() {
        return avariyaPeretvoruvachaK1Ug2;
    }

    public boolean isAvariyaPeretvoruvachaK2Ug3() {
        return avariyaPeretvoruvachaK2Ug3;
    }

    public boolean isAvariyaPeretvoruvachaPvVkUg1() {
        return avariyaPeretvoruvachaPvVkUg1;
    }

    public double getInsideTemperature() {
        return insideTemperature;
    }

    public double getOutsideTemperature() {
        return outsideTemperature;
    }

    public double getAirFlowTemperature() {
        return airFlowTemperature;
    }

    public double getBoilerTemperature() {
        return boilerTemperature;
    }

    public double getBatteryCurrent() {
        return batteryCurrent;
    }

    public double getGeneratorCurrent() {
        return generatorCurrent;
    }

    public double getOnBoardVoltage() {
        return onBoardVoltage;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PollingIOControllerObject{");
        sb.append("timestamp=").append(timestamp);
        sb.append(", automaticMode=").append(automaticMode);
        sb.append(", shutdownSignal=").append(shutdownSignal);
        sb.append(", highVoltageHeatingElement1=").append(highVoltageHeatingElement1);
        sb.append(", highVoltageHeatingElement2=").append(highVoltageHeatingElement2);
        sb.append(", lowVoltageHeating=").append(lowVoltageHeating);
        sb.append(", compressorM1=").append(compressorM1);
        sb.append(", compressorM2=").append(compressorM2);
        sb.append(", peregrivNagrivachivConditionera=").append(peregrivNagrivachivConditionera);
        sb.append(", isFreezing=").append(isFreezing);
        sb.append(", electricalCallorifier=").append(electricalCallorifier);
        sb.append(", avariyaPv=").append(avariyaPv);
        sb.append(", highVoltage=").append(highVoltage);
        sb.append(", outerOrGeneratorNetwork=").append(outerOrGeneratorNetwork);
        sb.append(", crashError=").append(crashError);
        sb.append(", highVoltageError=").append(highVoltageError);
        sb.append(", insulationDamageError=").append(insulationDamageError);
        sb.append(", ventilation=").append(ventilation);
        sb.append(", peregrivVk=").append(peregrivVk);
        sb.append(", zasorFiltra=").append(zasorFiltra);
        sb.append(", avariaReleDavleniya=").append(avariaReleDavleniya);
        sb.append(", ruchnoyRezhimConditionera=").append(ruchnoyRezhimConditionera);
        sb.append(", avariyaPeretvoruvachaK1Ug2=").append(avariyaPeretvoruvachaK1Ug2);
        sb.append(", avariyaPeretvoruvachaK2Ug3=").append(avariyaPeretvoruvachaK2Ug3);
        sb.append(", avariyaPeretvoruvachaPvVkUg1=").append(avariyaPeretvoruvachaPvVkUg1);
        sb.append(", insideTemperature=").append(insideTemperature);
        sb.append(", outsideTemperature=").append(outsideTemperature);
        sb.append(", airFlowTemperature=").append(airFlowTemperature);
        sb.append(", boilerTemperature=").append(boilerTemperature);
        sb.append(", batteryCurrent=").append(batteryCurrent);
        sb.append(", generatorCurrent=").append(generatorCurrent);
        sb.append(", onBoardVoltage=").append(onBoardVoltage);
        sb.append('}');
        return sb.toString();
    }
}
