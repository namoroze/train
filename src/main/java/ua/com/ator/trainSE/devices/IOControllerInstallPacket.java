package ua.com.ator.trainSE.devices;

import net.wimpi.modbus.procimg.Register;
import net.wimpi.modbus.procimg.SimpleRegister;
import net.wimpi.modbus.util.BitVector;

/**
 * Входящий пакет от устройства
 * @author Baevskiy Ilya
 */

class IOControllerInstallPacket implements RegistersReadable {


    private BitVector values = new BitVector(16);


    private boolean heatingElement1;
    private boolean heatingElement2;
    private boolean pumpState;
    private boolean lowVoltageHeating;
    private boolean crash;


    IOControllerInstallPacket() {
    }
    IOControllerInstallPacket( boolean heatingElement1, boolean heatingElement2, boolean lowVoltageHeating, boolean pumpState, boolean crash) {
        this.heatingElement1 = heatingElement1;
        this.heatingElement2 = heatingElement2;
        this.lowVoltageHeating = lowVoltageHeating;
        this.pumpState = pumpState;
        this.crash = crash;
    }

    @Override
    public Register[] getRegisters() {
        Register register = new SimpleRegister();
        register.setValue(values.getBytes());
        return new Register[]{
                register
        };
    }
    public boolean equals(IOControllerInstallPacket packet) {
        if (heatingElement1 != packet.heatingElement1) return false;
        if (heatingElement2 != packet.heatingElement2) return false;
        if (pumpState != packet.pumpState) return false;
        if (lowVoltageHeating != packet.lowVoltageHeating) return false;
        return crash == packet.crash;
    }


    boolean isHeatingElement1() {
        return heatingElement1;
    }
    boolean isHeatingElement2() {
        return heatingElement2;
    }
    boolean isPumpState() {
        return pumpState;
    }
    boolean isLowVoltageHeating() {
        return lowVoltageHeating;
    }
    boolean isCrash() {
        return crash;
    }
}


