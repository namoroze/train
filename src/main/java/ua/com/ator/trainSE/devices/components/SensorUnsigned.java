package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.When;

/**
 * @author Dmitry Shokolov on 27.04.2018.
 */
public class SensorUnsigned extends Sensor{
    /**
     * @param coefficient      - Coefficient for normalizing input value
     * @param offset           - Offset for normalizing input value
     * @param errorValue       - The value that identifies the sensor error
     * @param delayForAverage  - Delay for average, ms
     * @param type
     * @param minValue
     * @param maxValue
     * @param title
     * @param units
     * @param averageAlgorithm - Algorithm for averaging of value
     */
    public SensorUnsigned(double coefficient, double offset, double coeffMaterial,int errorValue, int delayForAverage, String type, int minValue, int maxValue, String title, String units, AverageAlgorithm averageAlgorithm) {
        super(coefficient, offset, coeffMaterial, errorValue, delayForAverage, type, minValue, maxValue, title, units, averageAlgorithm);
        //Binding for normalize value by changing

        this.lastValue = (DoubleBinding) new When(rawValue.divide(coefficient).add(offset).multiply(coeffMaterial).greaterThanOrEqualTo(0))
        .then(rawValue.divide(coefficient).add(offset).multiply(coeffMaterial))
        .otherwise(0);
    }
}
