package ua.com.ator.trainSE.devices.components;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.utils.Timer;

/**
 * Компонент генератор
 *
 * Зависимости: currentGeneratorSensor - сенсор тока генератора(А)
 *
 * События: "Превышен ток генератора"
 *
 * Входные регистры: inputGeneratorState - в работе ли генератора(борт сеть)
 *
 * Выходные регистры: generatorHighCurrentError - Imax - превышен тока генератора
 *
 * @author Baevskiy Ilya
 */
public class Generator {

    /**
     * Предельное значение тока выше которого срабатывает
     * ошибка "Превышен ток генератора"
     */
    private final int HIGH_CURRENT_ERROR_VALUE;

    /**
     * Сенсор тока генератора
     */
    @Autowired
    @Qualifier( "currentGeneratorSensor" )
    private Sensor currentGeneratorSensor;
    /**
     * Биндинг показывающий на состояние ошибки "Превышен ток генератора"
     */
    private BooleanBinding generatorHighCurrentError;
    /**
     * Таймер запускаемый при повышенном токе,
     * для ожидания нормализации значения тока.
     * И сбрасываемый после того как значение тока нормализовалось
     */
    private Timer generatorHighCurrentErrorTimer;
    /**
     * Property для  входящего значения состояния регистра
     * "генератор или бортсеть - есть/нет".
     */
    private final BooleanProperty inputGeneratorState = new SimpleBooleanProperty(false);

    /**
     * @param generatorHighCurrentErrorValue - Значение тока выше которого свидетельствует
     *                                       о наличии ошибки "Повышеный ток генератора" A
     * @param generatorHighCurrentErrorDelay - Задержка на срабатывание ошибки "Повышеный ток генератора", ms
     */
    public Generator( int generatorHighCurrentErrorValue, int generatorHighCurrentErrorDelay ) {
        this.HIGH_CURRENT_ERROR_VALUE = generatorHighCurrentErrorValue;
        this.generatorHighCurrentErrorTimer = new Timer(generatorHighCurrentErrorDelay);
    }

    /**
     * Метод предназначен для инициализации
     * биндингов ошибок, запускаемый при
     * создании компонента
     */
    public void init(){
        generatorHighCurrentError = Bindings
                .createBooleanBinding(
                        () -> generatorHighCurrentErrorTimer.check(() -> currentGeneratorSensor.lastValueProperty().get() >= HIGH_CURRENT_ERROR_VALUE),
                        currentGeneratorSensor.lastValueProperty())
                .and(currentGeneratorSensor.isErrorProperty().not());
    }

    /**
     * Возвращает входящее состояние генератора или бортсети
     * основывается на последних данных из входящего регистра
     * @return - состояниее генератора или борт сети:
     *              true - есть генератор или бортсеть
     *              false - нет генератора и борт сети
     */
    public boolean getInputGeneratorState() {
        return inputGeneratorState.get();
    }
    /**
     * Возвращает Property входящего состояния генератора или бортсети
     * @return Property входящего значения генератора или борт сети
     * основанное на последних данных из входящего регистра
     */
    public BooleanProperty inputGeneratorStateProperty() {
        return inputGeneratorState;
    }
    /**
     * Устанавливает состояние входящего регистра "генератор или борт сеть"
     * @param inputGeneratorState - true - есть генератор или бортсеть
     *                              false - нет генератора и борт сети
     */
    public void setInputGeneratorState( boolean inputGeneratorState ) {
        this.inputGeneratorState.set(inputGeneratorState);
    }

    /**
     * Вовзвращает Binding ошибки "Превышен ток генератора" Imax
     * @return - Binding ошибки "Превышен ток генератора"
     */
    public BooleanBinding highCurrentErrorProperty() {
        return generatorHighCurrentError;
    }
    public void reset(){
        generatorHighCurrentErrorTimer.stop();
    }
}
