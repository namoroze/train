package ua.com.ator.trainSE.devices;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableBooleanValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.com.ator.trainSE.connectivity.ModbusCommunicationService;
import ua.com.ator.trainSE.devices.components.*;
import ua.com.ator.trainSE.ui.property_adapters.SimpleBooleanPropertyAdapter;
import ua.com.ator.trainSE.utils.Timer;

import java.util.stream.Stream;

import static ua.com.ator.trainSE.configuration.IOControllerConfiguration.DEVICE_ADDRESS;

/**
 * Класс платы ввода-вывода (БВВ)
 *
 * @author Baevskiy Ilya
 */
public final class IOControllerDevice {
    private SimpleBooleanProperty insulationResistanceIsErrorProperty;


    //Modbus communication component of device
    private NetworkIOController networkDevice;
    /**
     * Modbus connection error state:
     * false - normal state
     * true - connection error
     */
    private final BooleanProperty connectionError = new SimpleBooleanProperty(true);
    private final BooleanProperty shutdown = new SimpleBooleanProperty(false);
    private BooleanBinding isErrorTemperatureSensor;
    /**********************************************
     * Sensors
     **********************************************/
    /* Temperatures */
    @Autowired
    @Qualifier("insideTemperatureSensor")
    private Sensor insideTemperatureSensor; // Coupe temperature sensor
    @Autowired
    @Qualifier("outsideTemperatureSensor")
    private Sensor outsideTemperatureSensor; // Outside temperature sensor
    @Autowired
    @Qualifier("airflowTemperatureSensor")
    private Sensor airFlowTemperatureSensor; // Temperature of air flow sensor
    @Autowired
    @Qualifier("boilerTemperatureSensor")
    private Sensor boilerTemperatureSensor; // Boiler temperature sensor
    @Autowired
    @Qualifier("establishedTemperatureSensor")
    private Sensor establishedTemperatureSensor; // Established temperature sensor

    /* Electrical */
    @Autowired
    @Qualifier("currentBatterySensor")
    private Sensor currentBatterySensor; // Battery current sensor
    @Autowired
    @Qualifier("currentGeneratorSensor")
    private Sensor currentGeneratorSensor; // Generator current sensor
    @Autowired
    @Qualifier("networkVoltageSensor")
    private Sensor boardNetworkVoltageSensor; // Board network voltage sensor
    /* SENSORS **************************************/


    //--------------------------------------
    @Autowired
    private AccumulatorBattery accumulatorBattery;
    @Autowired
    private BoardNetwork boardNetwork;
    @Autowired
    private Boiler boiler;
    @Autowired
    private Conditioner conditioner;
    @Autowired
    private Generator generator;
    @Autowired
    private Pump pump;

    //Входящий регистр АВАРИЯ ЭО
    private final BooleanProperty isCrash = new SimpleBooleanProperty(false);

    //Програмный биндинг собирает со всех компонентов ошибки связаные с АВАРИЕЙ ЭО
    private BooleanBinding isProgramCrash;


    //Входящий регистр АВТО РЕЖИМ
    private final BooleanProperty isAutomaticMode = new SimpleBooleanProperty(false);
    private final BooleanProperty isWorkMode = new SimpleBooleanProperty(false);

    //Не возможна работа НВО в автоматическом режиме
    private BooleanBinding isNVOAutomaticModeNotPossible;
    //Не возможна работа ВВО в автоматическом режиме
    private BooleanBinding isVVOAutomaticModeNotPossible;
    // Нет 3кв
    private BooleanBinding no3Kv;


    /**
     * Low voltage heating component
     */
    @Autowired
    private LowVoltageHeating lowVoltageHeating;

    @Autowired
    private HighVoltageHeating highVoltageHeating;

    private ObservableBooleanValue manualHeatingProperty;


    /**
     * Version packet received from device
     */
    private final ObjectProperty<Version> version = new SimpleObjectProperty<>(Version.emptyVersion());

    public IOControllerDevice(ModbusCommunicationService modbusCommunicationService) {
        this.networkDevice = new NetworkIOController(modbusCommunicationService, DEVICE_ADDRESS);
        this.insulationResistanceIsErrorProperty = new SimpleBooleanProperty(true);
    }

    public IOControllerDevice(ModbusCommunicationService modbusCommunicationService,
                              boolean insulationResistanceIsError) {
        this.networkDevice = new NetworkIOController(modbusCommunicationService, DEVICE_ADDRESS);
        this.insulationResistanceIsErrorProperty = new SimpleBooleanProperty(insulationResistanceIsError);
    }

    public IOControllerDevice() {
    }

    public void connect() {
        networkDevice.connectDevice(this::versionObjectHandler, this::pollingObjectHandler, this::exceptionHandler);
    }

    private BooleanBinding accumulatorHighCurrentErrorBinding;
    private BooleanBinding dischargeErrorBinding;
    private BooleanBinding highVoltageInputErrorBinding;
    private BooleanBinding insulationResistanceErrorBinding;
    private BooleanBinding generatorHighCurrentErrorBinding;
    private BooleanBinding outputHighVoltageBinding;
    private BooleanBinding highTemperatureErrorBinding;
    private BooleanBinding insulationResistanceIsErrorBinding;
    private BooleanBinding peregrevPVBindingMainView;
    private BooleanBinding avariyaPVBindingMainView;

    public void init() {
        accumulatorHighCurrentErrorBinding = accumulatorBattery.highCurrentErrorProperty().and(connectionError.not());
        dischargeErrorBinding = accumulatorBattery.dischargedErrorProperty().and(connectionError.not());
        highVoltageInputErrorBinding = boardNetwork.highVoltageInputErrorProperty().and(connectionError.not());
        insulationResistanceErrorBinding = boardNetwork.insulationResistanceErrorProperty().and(connectionError.not());
        generatorHighCurrentErrorBinding = generator.highCurrentErrorProperty().and(connectionError.not());
        outputHighVoltageBinding = boardNetwork.outputHighVoltageError().and(connectionError.not());
        highTemperatureErrorBinding = boiler.highTemperatureErrorProperty().and(connectionError.not());

        /*
          Биндинг на предмет некорректного значения или обрыва датчика
         */
        isErrorTemperatureSensor =
                insideTemperatureSensor.isErrorProperty()
                        .or(outsideTemperatureSensor.isErrorProperty())
                        .or(boilerTemperatureSensor.isErrorProperty())
                        .or(airFlowTemperatureSensor.isErrorProperty());
        /*
          Биндинг на события связанные с аварией
         */
        isProgramCrash = isCrash
                .or(boardNetwork.highVoltageInputErrorProperty())
                .or(boardNetwork.highVoltageErrorProperty())
                .or(accumulatorBattery.dischargedErrorProperty())
                .or(accumulatorBattery.highCurrentErrorProperty())
                .or(generator.highCurrentErrorProperty())
                .or(boardNetwork.insulationResistanceErrorProperty());

        /*
          Бинды на возможность автоматического режима
         */
        // Невозможен авто режими НВО
        isNVOAutomaticModeNotPossible = isErrorTemperatureSensor.or(lowVoltageHeating.errorProperty()).and(isAutomaticMode);
        // Невозможен  авто режими ВВО
        isVVOAutomaticModeNotPossible = isErrorTemperatureSensor.or(highVoltageHeating.group1ErrorStateProperty().and(highVoltageHeating.group2ErrorStateProperty())).and(isAutomaticMode);

        // ручной режим отопления
        manualHeatingProperty = isAutomaticMode.not().and(
                lowVoltageHeating.inputStateProperty()
                        .or(highVoltageHeating.group1InputStateProperty())
                        .or(highVoltageHeating.group2InputStateProperty()));

        // Биндим отсутствие 3кв
        no3Kv = highVoltageHeating.no3KvProperty()
                .and(isAutomaticMode.and(isVVOAutomaticModeNotPossible.not()));


        insulationResistanceIsErrorBinding = insulationResistanceErrorBinding
                .and(insulationResistanceIsErrorProperty);

        crashOutput =
                boardNetwork.outputHighVoltageError()
                        .or(accumulatorBattery.dischargedErrorProperty())
                        .or(accumulatorBattery.highCurrentErrorProperty())
                        .or(generator.highCurrentErrorProperty())
                        .or(insulationResistanceIsErrorBinding);

        highCurrentError.bind(accumulatorBattery.highCurrentErrorProperty().or(generator.highCurrentErrorProperty()));
        establishedTemperatureSensor.updateAverageValue();
        establishedTemperatureSensor.lastValueProperty().addListener((observable, oldValue, newValue) -> establishedTemperatureSensor.updateAverageValue());
        isWorkMode.bind(isAutomaticMode.or(conditioner.ruchnoyRezhimConditioneraProperty()));
        peregrevPVBindingMainView = isWorkMode.and(conditioner.peregrivNagrivachivConditioneraProperty());
        avariyaPVBindingMainView = isWorkMode.and(conditioner.avariyaPvProperty());
        connect();
    }


    /**
     * Handler of input packet of device
     *
     * @param pollingObject
     */
    private void pollingObjectHandler(PollingIOControllerObject pollingObject) {
        connectionError.set(false);
        // Заполняем данные с входящего пакета
        fillDataHandler(pollingObject);
        // Обновляем значения датчиков
        updateSensorAverageValue();
        componentProcess();
        networkDevice.sendSettings(
                new IOControllerInstallPacket(
                        highVoltageHeating.group1OutputStateProperty().getValue(),
                        highVoltageHeating.group2OutputStateProperty().getValue(),
                        lowVoltageHeating.outputStateProperty().getValue(),
                        pump.getOutputState(),
                        crashOutput().getValue()
                ), this::exceptionHandler
        );
    }

    /**
     * Handler of Version packet of device
     *
     * @param version
     */
    private void versionObjectHandler(Version version) {
        connectionError.set(false);
        this.version.setValue(version);
    }

    /**
     * Обработчик нет соединения с устройством
     *
     * @param ex
     */
    private Timer errorConnectionTimer = new Timer(3000);

    private void exceptionHandler(Throwable ex) {
        //Нет соединения с устройством
        if (errorConnectionTimer.check()) {
            connectionError.set(true);
            reset();
        }
    }

    private void reset() {
        Stream.of(
                insideTemperatureSensor,
                outsideTemperatureSensor,
                airFlowTemperatureSensor,
                boilerTemperatureSensor,
                currentBatterySensor,
                currentGeneratorSensor,
                boardNetworkVoltageSensor).forEach(Sensor::resetSensor);

        isCrash.setValue(false);
        boardNetwork.setInsulationResistanceError(false);
        boardNetwork.setHighVoltageInputError(false);
        generator.setInputGeneratorState(false);

        shutdown.setValue(false);

        isAutomaticMode.setValue(false);

        lowVoltageHeating.setInputState(false);
        highVoltageHeating.v3kVInputStateProperty().setValue(false);
        highVoltageHeating.group1InputStateProperty().setValue(false);
        highVoltageHeating.group2InputStateProperty().setValue(false);

        conditioner.setFirstCompressorError(false);
        conditioner.setSecondCompressorError(false);
        conditioner.setCoolerState(false);
        conditioner.setHeaterState(false);

        accumulatorBattery.reset();
        boiler.reset();
        boardNetwork.reset();
        generator.reset();
    }

    /**
     * Filling data from io controller to components
     *
     * @param pollingObject
     */
    private void fillDataHandler(PollingIOControllerObject pollingObject) {
        errorConnectionTimer.stop();
        shutdown.setValue(pollingObject.isShutdownSignal());
        isCrash.setValue(pollingObject.isCrashError());
        boardNetwork.setInsulationResistanceError(pollingObject.isInsulationDamageError());
        boardNetwork.setHighVoltageInputError(pollingObject.isHighVoltageError());
        generator.setInputGeneratorState(pollingObject.isOuterOrGeneratorNetwork());


        lowVoltageHeating.setInputState(pollingObject.isLowVoltageHeating());
        highVoltageHeating.v3kVInputStateProperty().setValue(pollingObject.isHighVoltage());
        highVoltageHeating.group1InputStateProperty().setValue(pollingObject.isHighVoltageHeatingElement1());
        highVoltageHeating.group2InputStateProperty().setValue(pollingObject.isHighVoltageHeatingElement2());

        conditioner.setPeregrivNagrivachivConditionera(!pollingObject.isPeregrivNagrivachivConditionera());
        conditioner.setAvariyaPv(!pollingObject.isAvariyaPv());
        conditioner.setFirstCompressorError(pollingObject.isCompressorM1());
        conditioner.setSecondCompressorError(pollingObject.isCompressorM2());
        conditioner.setCoolerState(pollingObject.isFreezing());
        conditioner.setHeaterState(pollingObject.isElectricalCallorifier());
        conditioner.setVentilation(pollingObject.isVentilation());
        conditioner.setPeregrivVk(!pollingObject.isPeregrivVk());
        conditioner.setZasorFiltra(!pollingObject.isZasorFiltra());
        conditioner.setAvariaReleDavleniya(!pollingObject.isAvariaReleDavleniya());
        conditioner.setRuchnoyRezhimConditionera(pollingObject.isRuchnoyRezhimConditionera());
        conditioner.setAvariyaPeretvoruvachaK1Ug2(pollingObject.isAvariyaPeretvoruvachaK1Ug2());
        conditioner.setAvariyaPeretvoruvachaK2Ug3(pollingObject.isAvariyaPeretvoruvachaK2Ug3());
        conditioner.setAvariyaPeretvoruvachaPvVkUg1(pollingObject.isAvariyaPeretvoruvachaPvVkUg1());

        insideTemperatureSensor.setRawValue(pollingObject.getInsideTemperature());
        outsideTemperatureSensor.setRawValue(pollingObject.getOutsideTemperature());
        airFlowTemperatureSensor.setRawValue(pollingObject.getAirFlowTemperature());
        boilerTemperatureSensor.setRawValue(pollingObject.getBoilerTemperature());
        currentBatterySensor.setRawValue(pollingObject.getBatteryCurrent());
        currentGeneratorSensor.setRawValue(pollingObject.getGeneratorCurrent());
        boardNetworkVoltageSensor.setRawValue(pollingObject.getOnBoardVoltage());

        isAutomaticMode.setValue(pollingObject.isAutomaticMode());

    }

    private void updateSensorAverageValue() {
        Stream.of(
                insideTemperatureSensor,
                outsideTemperatureSensor,
                airFlowTemperatureSensor,
                boilerTemperatureSensor,
                currentBatterySensor,
                currentGeneratorSensor,
                boardNetworkVoltageSensor).forEach(Sensor::updateAverageValue);
    }

    private void componentProcess() {
        // Если можем работать в авто режиме
        if (isAutomaticMode.get() && !isNVOAutomaticModeNotPossible.get()) {
            lowVoltageHeating.process();
        }
        if (isAutomaticMode.get() && !isVVOAutomaticModeNotPossible.get()) {
            highVoltageHeating.process();
        }
        // Если не можем работать в авто режиме или чтото с сенсорами
        if (!isAutomaticMode.get() || isErrorTemperatureSensor.get()) {
            lowVoltageHeating.reset();
            highVoltageHeating.reset();
        }
    }


    /////////////////////////////////////////////////////////////////////
    /* Getters of sensors */
    /////////////////////////////////////////////////////////////////////

    // Пропертя сигнализирует об ошибке соединения
    public ObservableBooleanValue connectionErrorProperty() {
        return new SimpleBooleanPropertyAdapter(connectionError);
    }
    // Версию вертает

    public ObjectProperty<Version> versionProperty() {
        return version;
    }

    // Сигнализирует что необходимо отключатся
    public BooleanProperty shutdownProperty() {
        return shutdown;
    }

    // Пропертя сигнализирует о автоматическом режиме
    public ObservableBooleanValue isAutomaticModeProperty() {
        return isAutomaticMode;
    }

    // Показывает что невозможно НВО в автоматическом режиме
    public ObservableBooleanValue isNVOAutomaticModeNotPossibleProperty() {
        return isNVOAutomaticModeNotPossible;
    }

    // Показывает что невозможно ВВО в автоматическом режиме
    public ObservableBooleanValue isVVOAutomaticModeNotPossibleProperty() {
        return isVVOAutomaticModeNotPossible;
    }

    // Сигнализирует об Аварии ЭО
    public ObservableBooleanValue isCrashProperty() {
        return isCrash;
    }

    /* Property от акккумулятора */
    // Показывает разряжена ли батарея
    public ObservableBooleanValue dischargedErrorProperty() {
        return dischargeErrorBinding;
    }

    // Показывает если ток превышен
    public ObservableBooleanValue accumulatorHighCurrentErrorProperty() {
        return accumulatorHighCurrentErrorBinding;
    }

    private BooleanProperty highCurrentError = new SimpleBooleanProperty(false);

    // Сигнализирует о том что гдето превышен ток(АКБ/генератор)
    public ObservableBooleanValue highCurrentErrorProperty() {
        return highCurrentError;
    }
    //----------------------------

    //Высокое на входящем регистре
    /* Property от бортовой сети */
    public ObservableBooleanValue highVoltageInputErrorProperty() {
        return highVoltageInputErrorBinding;
    }

    // Высокое напряжение вычисленное
    public ObservableBooleanValue highVoltageErrorProperty() {
        return boardNetwork.highVoltageErrorProperty();
    }

    // Ошибка сопротивления на входном регистре
    public ObservableBooleanValue insulationResistanceErrorProperty() {
        return insulationResistanceErrorBinding;
    }

    public ObservableBooleanValue highVoltageOutputErrorProperty() {
        return outputHighVoltageBinding;
    }

    /* Property от котла */
    //----------------------------
    public ObservableBooleanValue boilerHighTemperatureErrorProperty() {
        return highTemperatureErrorBinding;
    }

    //----------------------------
    /* Property от генератора */
    // Показывает включен ли генератор
    public ObservableBooleanValue inputGeneratorStateProperty() {
        return generator.inputGeneratorStateProperty();
    }

    // Высокий ток работы генератора
    public ObservableBooleanValue generatorHighCurrentErrorProperty() {
        return generatorHighCurrentErrorBinding;
    }
    //----------------------------


    /* Property от кондиционера */
    // Кондиционер в работе
    public ObservableBooleanValue ruchnoyRezhimConditioneraProperty() {
        return conditioner.ruchnoyRezhimConditioneraProperty();
    }

    /* Перегрев нагревателя кондиционера */
    public BooleanProperty peregrivNagrivachivConditioneraProperty() {
        return conditioner.peregrivNagrivachivConditioneraProperty();
    }

    public ObservableBooleanValue peregrivNagrivachivConditioneraPropertyMainView() {
        return peregrevPVBindingMainView;
    }

    // Перегрев нагревателя
    public BooleanProperty avariyaPvProperty() {
        return conditioner.avariyaPvProperty();
    }

    public BooleanBinding avariyaPvPropertyMainView() {
        return avariyaPVBindingMainView;
    }

    // Охлаждение
    public ObservableBooleanValue coolerStateProperty() {
        return conditioner.coolerStateProperty();
    }

    // Электрокаллорифер
    public ObservableBooleanValue conditionerHeaterStateProperty() {
        return conditioner.heaterStateProperty();
    }

    // Отказ первого контура охлаждения
    public ObservableBooleanValue firstCompressorErrorProperty() {
        return conditioner.firstCompressorErrorProperty();
    }

    // Отказ второго контура охлаждения
    public ObservableBooleanValue secondCompressorErrorProperty() {
        return conditioner.secondCompressorErrorProperty();
    }

    // Вентиляция
    public ObservableBooleanValue conditionerVentilationProperty() {
        return conditioner.ventilationProperty();
    }

    public BooleanProperty peregrivVkProperty() {
        return conditioner.peregrivVkProperty();
    }

    public BooleanProperty zasorFiltraProperty() {
        return conditioner.zasorFiltraProperty();
    }

    public BooleanProperty avariaReleDavleniyaProperty() {
        return conditioner.avariaReleDavleniyaProperty();
    }


    public BooleanProperty avariyaPeretvoruvachaK1Ug2Property() {
        return conditioner.avariyaPeretvoruvachaK1Ug2Property();
    }

    public BooleanProperty avariyaPeretvoruvachaK2Ug3Property() {
        return conditioner.avariyaPeretvoruvachaK2Ug3Property();
    }

    public BooleanProperty avariyaPeretvoruvachaPvVkUg1Property() {
        return conditioner.avariyaPeretvoruvachaPvVkUg1Property();
    }

    //----------------------------
    /* Property от НВО */
    // Низковольтное отопление на входном регистре
    public ObservableBooleanValue lowVoltageHeatingInputStateProperty() {
        return lowVoltageHeating.inputStateProperty();
    }

    // Ошибка низковольтного отопления(ошибка включения)
    public ObservableBooleanValue lowVoltageHeatingErrorProperty() {
        return lowVoltageHeating.errorProperty();
    }

    /* Property от ВВО */
    // 3KV
    public ObservableBooleanValue voltage3kvInputState() {
        return highVoltageHeating.v3kVInputStateProperty();
    }

    // ВВО первой группы тенов на входном регистре
    public ObservableBooleanValue highVoltageHeatingFirstGroupInputStateProperty() {
        return highVoltageHeating.group1InputStateProperty();
    }

    // ВВО второй группы тенов на входном регистре
    public ObservableBooleanValue highVoltageHeatingSecondGroupInputStateProperty() {
        return highVoltageHeating.group2InputStateProperty();
    }

    // Ошибка включения первой группы ВВО
    public ObservableBooleanValue highVoltageHeatingFirstGroupErrorProperty() {
        return highVoltageHeating.group1ErrorStateProperty();
    }

    // Ошибка включения второй группы ВВО
    public ObservableBooleanValue highVoltageHeatingSecondGroupErrorProperty() {
        return highVoltageHeating.group2ErrorStateProperty();
    }

    // Ошибка нет 3Кв для включения ВВО отопления
    public ObservableBooleanValue no3kvProperty() {
        return no3Kv;
    }

    /* Property от котла */
    //----------------------------
    public ObservableBooleanValue getManualHeatingProperty() {
        return manualHeatingProperty;
    }

    //----------------------------

    /* Геттеры сенсоров */
    public Sensor getInsideTemperatureSensor() {
        return insideTemperatureSensor;
    }

    public Sensor getOutsideTemperatureSensor() {
        return outsideTemperatureSensor;
    }

    public Sensor getAirFlowTemperatureSensor() {
        return airFlowTemperatureSensor;
    }

    public Sensor getBoilerTemperatureSensor() {
        return boilerTemperatureSensor;
    }

    public Sensor getCurrentBatterySensor() {
        return currentBatterySensor;
    }

    public Sensor getCurrentGeneratorSensor() {
        return currentGeneratorSensor;
    }

    public Sensor getBoardNetworkVoltageSensor() {
        return boardNetworkVoltageSensor;
    }


    /* OUTPUTS */
    public ObservableBooleanValue pumpWorkingProperty() {
        return pump.outputStateProperty();
    }

    public ObservableBooleanValue highVoltageHeatingGroup1OutputStateProperty() {
        return highVoltageHeating.group1OutputStateProperty();
    }

    public ObservableBooleanValue highVoltageHeatingGroup2OutputStateProperty() {
        return highVoltageHeating.group2OutputStateProperty();
    }

    public BooleanProperty lowVoltageHeatingOutputStateProperty() {
        return lowVoltageHeating.outputStateProperty();
    }

    private ObservableBooleanValue crashOutput;

    public ObservableBooleanValue crashOutput() {
        return crashOutput;
    }

    //-----------------------------------------------------------------------------------
    public void finalize() {
        networkDevice.close();
    }


}

