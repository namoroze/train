package ua.com.ator.trainSE.devices;

import net.wimpi.modbus.procimg.Register;

/**
 * Интерфейс читает регистры
 * @author Baevskiy Ilya
 */
interface RegistersReadable {
    Register[] getRegisters();
}
