package ua.com.ator.trainSE;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import org.springframework.stereotype.Component;
import ua.com.ator.trainSE.ui.pages.PageSwitcher;

import static ua.com.ator.trainSE.keypad.KeypadButtons.*;

/**
 * @author Dmitry Sokolov on 27.10.2017.
 */
@Component
public class SystemObject {

    private final BooleanProperty workingProperty;

    public SystemObject() {
        this.workingProperty = new SimpleBooleanProperty(false);
    }

    private final KeyCodeCombination altX = new KeyCodeCombination(KeyCode.X, KeyCombination.ALT_DOWN);
    private final KeyCodeCombination altQ = new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN);

    public EventHandler<KeyEvent> keyEventEventHandler(PageSwitcher pageSwitcher) {

        return event -> {
            if (altX.match(event) || altQ.match(event)) {
                workingProperty.setValue(false);
            }

            if (event.getCode() == KeyCode.Z) {
                pageSwitcher.handle(F1);
            } else if (event.getCode() == KeyCode.X) {
                pageSwitcher.handle(F2);
            } else if (event.getCode() == KeyCode.C) {
                pageSwitcher.handle(F3);
            } else if (event.getCode() == KeyCode.V) {
                pageSwitcher.handle(F4);
            } else if (event.getCode() == KeyCode.UP) {
                pageSwitcher.getView().translateYProperty().set(pageSwitcher.getView().translateYProperty().get() - 1);
            } else if (event.getCode() == KeyCode.DOWN) {
                pageSwitcher.getView().translateYProperty().set(pageSwitcher.getView().translateYProperty().get() + 1);
            } else if (event.getCode() == KeyCode.LEFT) {
                pageSwitcher.getView().translateXProperty().set(pageSwitcher.getView().translateXProperty().get() - 1);
            } else if (event.getCode() == KeyCode.RIGHT) {
                pageSwitcher.getView().translateXProperty().set(pageSwitcher.getView().translateXProperty().get() + 1);
            } else if (event.getCode() == KeyCode.NUMPAD4) {
                pageSwitcher.getView().scaleXProperty().setValue(pageSwitcher.getView().scaleXProperty().get() - 0.001);
            } else if (event.getCode() == KeyCode.NUMPAD6) {
                pageSwitcher.getView().scaleXProperty().setValue(pageSwitcher.getView().scaleXProperty().get() + 0.001);
            } else if (event.getCode() == KeyCode.NUMPAD8) {
                pageSwitcher.getView().scaleYProperty().setValue(pageSwitcher.getView().scaleYProperty().get() + 0.001);
            } else if (event.getCode() == KeyCode.NUMPAD2) {
                pageSwitcher.getView().scaleYProperty().setValue(pageSwitcher.getView().scaleYProperty().get() - 0.001);
            } else if (event.getCode() == KeyCode.PAGE_UP) {
                pageSwitcher.getView().rotateProperty().setValue(pageSwitcher.getView().rotateProperty().get() - 0.1);
            } else if (event.getCode() == KeyCode.PAGE_DOWN) {
                pageSwitcher.getView().rotateProperty().setValue(pageSwitcher.getView().rotateProperty().get() + 0.1);
            }
        };
    }
    public BooleanProperty workingPropertyProperty() {
        return workingProperty;
    }
}
