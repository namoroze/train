package ua.com.ator.trainSE.utils;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Created by User on 18.07.2017.
 */
public final class PropertyUtils {
    public static BooleanProperty clonePropertyWithBinding( BooleanProperty property ){
        BooleanProperty newProperty = new SimpleBooleanProperty(property.get());
        newProperty.bind(property);
        return newProperty;
    }
    public static DoubleProperty clonePropertyWithBinding( DoubleProperty property ){
        DoubleProperty newProperty = new SimpleDoubleProperty(property.get());
        newProperty.bind(property);
        return newProperty;
    }


    public static BooleanProperty clonePropertyWithBinding( BooleanBinding property ){
        BooleanProperty newProperty = new SimpleBooleanProperty(property.get());
        newProperty.bind(property);
        return newProperty;
    }
    public static DoubleProperty clonePropertyWithBinding( DoubleBinding property ){
        DoubleProperty newProperty = new SimpleDoubleProperty(property.get());
        newProperty.bind(property);
        return newProperty;
    }
}
