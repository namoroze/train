package ua.com.ator.trainSE.utils;

import java.util.function.BooleanSupplier;

import static java.lang.System.currentTimeMillis;

/**
 * Класс таймер
 */
public final class Timer {
    private long i = currentTimeMillis();
    private long interval;
    private boolean state;

    public Timer( long interval ) {
        this.i = System.currentTimeMillis();
        this.interval = interval;
    }

    public Boolean check() {
        if ( !state ) {
            state = true;
            i = currentTimeMillis();
        }
        return ( currentTimeMillis() - i ) >= interval;
    }

    public boolean check( BooleanSupplier supplier ) {
        if ( !supplier.getAsBoolean() ) {
            stop();
        } else {
            return check();
        }
        return false;
    }

    public boolean checkWithReset( BooleanSupplier condition ){
        boolean result = check(condition);
        if(result) {
            stop();
        }
        return result;
    }

    public boolean checkWithReset(){
        boolean result = check();
        if(result) {
            stop();
        }
        return result;
    }

    public void stop() {
        state = false;
    }

}
