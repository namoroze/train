package ua.com.ator.trainSE.utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.StringJoiner;

/**
 * @author Shokolov Dmitry.
 */
public class OsCmdService {
    private static Logger logger = Logger.getLogger(OsCmdService.class);

    public static void systemMenu() {
        exec("initctl emit train-system-menu");
    }

    public static void shutdown() {
        exec("initctl emit train-system-shutdown");
    }

    /**
     * <p>method execute console command <tt>exec( consoleCommand )</tt>
     *
     * @param consoleCommand console command
     * @return system message
     */
    private static void exec(String consoleCommand) {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add(consoleCommand);
        String message;

        try {
            Process p = Runtime.getRuntime().exec(consoleCommand);

            try (Reader inputStream = new InputStreamReader(p.getInputStream());
                 Reader errorStream = new InputStreamReader(p.getErrorStream());
                 BufferedReader stdInput = new BufferedReader(inputStream);
                 BufferedReader stdError = new BufferedReader(errorStream)) {

                while ((message = stdInput.readLine()) != null) {
                    joiner.add(message);
                }

                while ((message = stdError.readLine()) != null) {
                    joiner.add(message);
                }
            }

            logger.info(joiner.toString());
        } catch (IOException e) {
            logger.error("Fail to execute " + joiner.toString(), e);
        }
    }
}