package ua.com.ator.trainSE.utils;

import javafx.beans.property.BooleanProperty;

import static java.lang.System.currentTimeMillis;

/**
 * @author Dmitry Sokolov on 15.01.2018.
 */
public class ShutdownService {

    private long shutdownTimestamp;
    private Timer shutdownTimer;
    private BooleanProperty workingProperty;

    public ShutdownService(long interval, BooleanProperty workingProperty) {
        shutdownTimer = new Timer(interval);
        this.workingProperty = workingProperty;
    }

    public void shutdown() {
        if ((currentTimeMillis() - shutdownTimestamp) < 1000) {

            if (shutdownTimer.check()) {
                workingProperty.set(false);
                OsCmdService.shutdown();
            }

        } else {
            shutdownTimer.stop();
        }

        shutdownTimestamp = currentTimeMillis();

    }
}
