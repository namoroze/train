package ua.com.ator.trainSE.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Dmitry Sokolov on 18.01.2018.
 */
public class DumpService {
    private static Logger logger = Logger.getLogger(DumpService.class);

    /**
     * зальет дамп базы если найдет
     */
    public static void checkDump() {
        String trainPath = System.getProperty("TRAIN_PATH", "/etc/train");
        String dumpPath = trainPath.concat("/dump.sql");

        if (Files.exists(Paths.get(dumpPath))) {
            try {
                Runtime.getRuntime().exec(trainPath + "/dump.sh").waitFor();
                Files.delete(Paths.get(dumpPath));
            } catch (IOException | InterruptedException e) {
                logger.error("Fail to execute " + trainPath + "/dump.sh", e);
            }
        }
    }
}
