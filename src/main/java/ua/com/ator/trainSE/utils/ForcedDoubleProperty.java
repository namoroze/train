package ua.com.ator.trainSE.utils;

import javafx.beans.property.SimpleDoubleProperty;

/**
 * SimpleDoubleProperty
 * Property with a forced event if the old
 * value is equal to the new value.
 *
 * @author Baevskiy Ilya
 */
public class ForcedDoubleProperty extends SimpleDoubleProperty {

    public ForcedDoubleProperty( double initialValue ) {
        super(initialValue);
    }
    @Override
    public void set( double newValue ){

        if ( get() == newValue ) {
            super.set(newValue + 0.0001);
//            super.set(newValue - 0.0001);
            super.set(newValue);
        } else {
            super.set(newValue);
        }
    }
}
