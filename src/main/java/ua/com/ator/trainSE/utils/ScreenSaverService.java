package ua.com.ator.trainSE.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Dmitry Sokolov on 04.12.2017.
 */
public class ScreenSaverService implements Runnable {
    private static Timer timer;

    private final int SCREEN_BRIGHTNESS;
    private final int SCREEN_BRIGHTNESS_SAVE;
    private final int SCREEN_SAVER_DELAY;

    private String trainPath;
    private boolean enable;

    public ScreenSaverService(boolean enable, int delay, int minBrightness, int maxBrightness) {
        this.enable = enable;

        trainPath = System.getProperty("TRAIN_PATH", "/etc/train");
        SCREEN_SAVER_DELAY = delay;
        SCREEN_BRIGHTNESS = maxBrightness;
        SCREEN_BRIGHTNESS_SAVE = minBrightness;

        exec(SCREEN_BRIGHTNESS);

        if (enable) {
            timer = createTimer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    exec(SCREEN_BRIGHTNESS_SAVE);
                }
            }, SCREEN_SAVER_DELAY);
        }
    }

    /**
     * добавит яркости экрану
     * и поставит задачу уменьшить яркость по истечению задержки
     */
    public synchronized void run() {
        if (enable) {
            timer.cancel();

            exec(SCREEN_BRIGHTNESS);

            timer = createTimer();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    exec(SCREEN_BRIGHTNESS_SAVE);
                }
            }, SCREEN_SAVER_DELAY);
        }

    }

    private void exec(int bright) {
        try {
            Runtime.getRuntime().exec(trainPath + "/brightness.sh " + bright).waitFor();
        } catch (IOException | InterruptedException e) {
            Logger.getLogger(ScreenSaverService.class).error("Fail to execute " + trainPath + "/brightness.sh", e);
        }
    }

    private static Timer createTimer() {
        return new Timer("ScreenSaverService Delay Timer", true);
    }
}
