package ua.com.ator.trainSE.utils;

import javafx.beans.property.SimpleDoubleProperty;

/**
 * SimpleDoubleProperty
 * Property with a forced event if the old
 * value is equal to the new value.
 * @author Baevskiy Ilya
 */
public class ForcedByChangeDoubleProperty extends SimpleDoubleProperty {

    public ForcedByChangeDoubleProperty( double initialValue ) {
        super(initialValue);
    }
    @Override
    public void set( double newValue ) {

        if (get() == newValue){
//            fireValueChangedEvent();
            //Можно попробывать вместо изменения значения
            //this.fireValueChangedEvent();
            super.set(newValue + 0.0001);
        }else {
            super.set(newValue);
        }
    }
}
