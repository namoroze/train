package ua.com.ator.trainSE.configuration;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

/**
 * @author Dmitry Shokolov on 04.01.2019.
 */
public class IOControllerConfiguration {

    public static final int DEVICE_ADDRESS;

    public static final int POLLING_INTERVAL;
    public static final int POLLING_REGISTER_START;
    public static final int POLLING_REGISTERS_COUNT;

    /* Input registers */
    public static final int POLLING_DISCRETE_REGISTER;
    public static final int POLLING_ANALOG_REGISTER_INSIDE_TEMP;
    public static final int POLLING_ANALOG_REGISTER_AIR_FLOW_TEMP;
    public static final int POLLING_ANALOG_REGISTER_OUTSIDE_TEMP;
    public static final int POLLING_ANALOG_REGISTER_BOILER_TEMP;

    public static final int POLLING_ANALOG_REGISTER_BATTERY_CURRENT;
    public static final int POLLING_ANALOG_REGISTER_GENERATOR_CURRENT;
    public static final int POLLING_ANALOG_REGISTER_NETWORK_VOLTAGE;

    public static final int POLLING_DISCRETE_BIT_HIGH_VOLTAGE;
    public static final int POLLING_DISCRETE_BIT_HEATING1;
    public static final int POLLING_DISCRETE_BIT_HEATING2;
    public static final int POLLING_DISCRETE_BIT_LOW_HEATING;
    public static final int POLLING_DISCRETE_BIT_NETWORK_OR_GEN;
    public static final int POLLING_DISCRETE_BIT_ATOMATIC_MODE;
    public static final int POLLING_DISCRETE_BIT_CRASH_ERR;
    public static final int POLLING_DISCRETE_BIT_SHUTDOWN_SIG;
    public static final int POLLING_DISCRETE_BIT_AVARIYA_PV;
    public static final int POLLING_DISCRETE_BIT_PEREGRIV_NAGRIVACHIV_CONDITIONERA;
    public static final int POLLING_DISCRETE_BIT_FREEZING;
    public static final int POLLING_DISCRETE_BIT_COMPRESSOR_M1;
    public static final int POLLING_DISCRETE_BIT_ELECTRO_CALLORIFIER;
    public static final int POLLING_DISCRETE_BIT_COMPRESSOR_M2;
    public static final int POLLING_DISCRETE_BIT_HIGH_VOLTAGE_ERR;
    public static final int POLLING_DISCRETE_BIT_INSULATION_ERR;

    public static final int POLLING_DISCRETE_REGISTER_RESERVE;
    public static final int POLLING_DISCRETE_BIT_VENTILATION;
    public static final int POLLING_DISCRETE_BIT_PERGRIV_VK;
    public static final int POLLING_DISCRETE_BIT_ZASOR_FILTRA;
    public static final int POLLING_DISCRETE_BIT_AVRIA_RELE_DAVLENIYA;
    public static final int POLLING_DISCRETE_BIT_RUCHNOY_REZHIM_CONDITIONERA;
    public static final int POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K1_UG2;
    public static final int POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K2_UG3;
    public static final int POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_PV_VK_UG1;


    public static final int VERSION_REGISTER_START;
    public static final int VERSION_REGISTERS_COUNT;

    public static final int VERSION_REGISTER_VERSION;
    public static final int VERSION_REGISTER_YEAR;
    public static final int VERSION_REGISTER_MONTH;
    public static final int VERSION_REGISTER_DAY;


    /* Output registers of equipment */
    public static final int INSTALL_FIRST_REGISTER;
    public static final int INSTALL_REGISTER_HEATING1;
    public static final int INSTALL_REGISTER_HEATING2;
    public static final int INSTALL_REGISTER_PUMP;
    public static final int INSTALL_REGISTER_LOW_VOLTAGE_HEATING;
    public static final int INSTALL_REGISTER_CRASH;


    /*Initiate ioController properties*/
    static {
        /* Load properties file*/
        Properties properties = new Properties();
        try (InputStream inStream = Files.newInputStream(Paths.get(System.getProperty("TRAIN_PATH") + "/io.properties"), StandardOpenOption.READ)) {
            properties.load(inStream);
        } catch (IOException e) {
            Logger.getLogger(IOControllerConfiguration.class).error("Can`t find configuration file", e);
        }

        DEVICE_ADDRESS = Integer.parseInt(properties.getProperty("DEVICE_ADDRESS", "8"));

        /*Pulling properties*/
        POLLING_REGISTER_START = Integer.parseInt(properties.getProperty("POLLING_REGISTER_START", "0"));
        POLLING_REGISTERS_COUNT = Integer.parseInt(properties.getProperty("POLLING_REGISTERS_COUNT", "9"));
        POLLING_INTERVAL = Integer.parseInt(properties.getProperty("POLLING_INTERVAL", "500"));



        /* Input registers */
        POLLING_DISCRETE_REGISTER = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_REGISTER", "0"));
        POLLING_DISCRETE_BIT_HIGH_VOLTAGE = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_HIGH_VOLTAGE", "0"));
        POLLING_DISCRETE_BIT_HEATING1 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_HEATING1", "1"));
        POLLING_DISCRETE_BIT_HEATING2 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_HEATING2", "2"));
        POLLING_DISCRETE_BIT_LOW_HEATING = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_LOW_HEATING", "3"));
        POLLING_DISCRETE_BIT_NETWORK_OR_GEN = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_NETWORK_OR_GEN", "4"));
        POLLING_DISCRETE_BIT_ATOMATIC_MODE = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_ATOMATIC_MODE", "5"));
        POLLING_DISCRETE_BIT_CRASH_ERR = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_CRASH_ERR", "6"));
        POLLING_DISCRETE_BIT_SHUTDOWN_SIG = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_SHUTDOWN_SIG", "7"));
        POLLING_DISCRETE_BIT_AVARIYA_PV = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_AVARIYA_PV", "8"));
        POLLING_DISCRETE_BIT_COMPRESSOR_M2 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_COMPRESSOR_M2", "9"));
        POLLING_DISCRETE_BIT_COMPRESSOR_M1 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_COMPRESSOR_M1", "10"));
        POLLING_DISCRETE_BIT_PEREGRIV_NAGRIVACHIV_CONDITIONERA = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_PEREGRIV_NAGRIVACHIV_CONDITIONERA", "11"));
        POLLING_DISCRETE_BIT_FREEZING = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_FREEZING", "12"));
        POLLING_DISCRETE_BIT_ELECTRO_CALLORIFIER = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_ELECTRO_CALLORIFIER", "13"));
        POLLING_DISCRETE_BIT_HIGH_VOLTAGE_ERR = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_HIGH_VOLTAGE_ERR", "14"));
        POLLING_DISCRETE_BIT_INSULATION_ERR = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_INSULATION_ERR", "15"));

        POLLING_DISCRETE_REGISTER_RESERVE = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_REGISTER_RESERVE", "1"));
        POLLING_DISCRETE_BIT_VENTILATION = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_VENTILATION", "0"));
        POLLING_DISCRETE_BIT_PERGRIV_VK = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_PERGRIV_VK", "1"));
        POLLING_DISCRETE_BIT_ZASOR_FILTRA = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_ZASOR_FILTRA", "2"));
        POLLING_DISCRETE_BIT_AVRIA_RELE_DAVLENIYA = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_AVARIA_RELE_DAVLENIYA", "3"));
        POLLING_DISCRETE_BIT_RUCHNOY_REZHIM_CONDITIONERA = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_RUCHNOY_REZHIM_CONDITIONERA", "4"));
        POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K1_UG2 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K1_UG2", "5"));
        POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K2_UG3 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_K2_UG3", "6"));
        POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_PV_VK_UG1 = Integer.parseInt(properties.getProperty("POLLING_DISCRETE_BIT_AVARIYA_PERETVORUVACHA_PV_VK_UG1", "7"));


        POLLING_ANALOG_REGISTER_INSIDE_TEMP = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_INSIDE_TEMP", "2"));
        POLLING_ANALOG_REGISTER_AIR_FLOW_TEMP = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_AIR_FLOW_TEMP", "3"));
        POLLING_ANALOG_REGISTER_OUTSIDE_TEMP = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_OUTSIDE_TEMP", "4"));
        POLLING_ANALOG_REGISTER_BOILER_TEMP = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_BOILER_TEMP", "5"));
        POLLING_ANALOG_REGISTER_BATTERY_CURRENT = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_BATTERY_CURRENT", "6"));
        POLLING_ANALOG_REGISTER_GENERATOR_CURRENT = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_GENERATOR_CURRENT", "7"));
        POLLING_ANALOG_REGISTER_NETWORK_VOLTAGE = Integer.parseInt(properties.getProperty("POLLING_ANALOG_REGISTER_NETWORK_VOLTAGE", "8"));



        /*Version registers properties*/
        VERSION_REGISTERS_COUNT = Integer.parseInt(properties.getProperty("VERSION_REGISTERS_COUNT", "4"));
        VERSION_REGISTER_START = Integer.parseInt(properties.getProperty("VERSION_REGISTER_START", "28"));
        VERSION_REGISTER_VERSION = Integer.parseInt(properties.getProperty("VERSION_REGISTER_VERSION", "0"));
        VERSION_REGISTER_YEAR = Integer.parseInt(properties.getProperty("VERSION_REGISTER_YEAR", "1"));
        VERSION_REGISTER_MONTH = Integer.parseInt(properties.getProperty("VERSION_REGISTER_MONTH", "2"));
        VERSION_REGISTER_DAY = Integer.parseInt(properties.getProperty("VERSION_REGISTER_DAY", "3"));


        INSTALL_FIRST_REGISTER = Integer.parseInt(properties.getProperty("INSTALL_FIRST_REGISTER", "0"));
        INSTALL_REGISTER_HEATING1 = Integer.parseInt(properties.getProperty("INSTALL_REGISTER_HEATING1", "0"));
        INSTALL_REGISTER_HEATING2 = Integer.parseInt(properties.getProperty("INSTALL_REGISTER_HEATING2", "1"));
        INSTALL_REGISTER_PUMP = Integer.parseInt(properties.getProperty("INSTALL_REGISTER_PUMP", "2"));
        INSTALL_REGISTER_LOW_VOLTAGE_HEATING = Integer.parseInt(properties.getProperty("INSTALL_REGISTER_LOW_VOLTAGE_HEATING", "3"));
        INSTALL_REGISTER_CRASH = Integer.parseInt(properties.getProperty("INSTALL_REGISTER_CRASH", "4"));
    }
}
