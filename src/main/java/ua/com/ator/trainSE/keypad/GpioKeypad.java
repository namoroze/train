package ua.com.ator.trainSE.keypad;


import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Optional;

import static ua.com.ator.trainSE.keypad.KeypadButtons.KeypadSettings.GPIO_ENABLE;
import static ua.com.ator.trainSE.keypad.KeypadButtons.KeypadSettings.INTERVAL_CHECK;

/**
 * Класс для GPIO
 * @author Baevskiy Ilya
 * @author Dmitry Sokolov
 */
@Component
public class GpioKeypad extends AbstractKeypad {

    @Override
    @PostConstruct
    public void start() {
        // Выходим если GPIO отключено
        if(!GPIO_ENABLE)
            return;

        ScheduledService<Optional<KeypadButtons>> service = new ScheduledService<Optional<KeypadButtons>>() {
            @Override
            protected Task<Optional<KeypadButtons>> createTask() {
                return new Task<Optional<KeypadButtons>>() {
                    @Override
                    protected Optional<KeypadButtons> call() throws Exception {
                        return Arrays.stream(KeypadButtons.values())
                                .filter(KeypadButtons::checkButton)
                                .findFirst();
                    }
                };
            }
        };

        service.setOnSucceeded(
                event -> service
                        .getValue()
                        .ifPresent(this::emit));


        service.setOnFailed(event -> Logger.getRootLogger().error(event.getSource().getException()));
        service.setPeriod(Duration.millis(INTERVAL_CHECK));
        service.start();
    }
}
