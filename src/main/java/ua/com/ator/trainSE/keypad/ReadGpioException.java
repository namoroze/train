package ua.com.ator.trainSE.keypad;

/**
 * @author Dmitry Sokolov on 13.10.2017.
 */
class ReadGpioException extends RuntimeException {

    ReadGpioException(String message, Throwable cause) {
        super(message, cause);
    }
}
