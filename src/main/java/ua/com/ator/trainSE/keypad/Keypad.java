package ua.com.ator.trainSE.keypad;

/**
 * @author Baevskiy Ilya
 * @author Dmitry Sokolov
 */
public interface Keypad {
    void addKeyListener(Runnable eventListener, KeypadButtons key);
    void addKeyListenerForAllKey(Runnable eventListener);
    void start();
}
