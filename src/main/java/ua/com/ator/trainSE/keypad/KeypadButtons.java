package ua.com.ator.trainSE.keypad;

import org.apache.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static java.util.Locale.US;
import static ua.com.ator.trainSE.keypad.KeypadButtons.KeypadSettings.*;

/**
 * GPIO пины
 *
 * @author Baevskiy Ilya
 * @author Dmitry Sokolov
 */
public enum KeypadButtons {
    F1(40),
    F2(42),
    F3(44),
    F4(46),
    SHUTDOWN(202);
    private static final int CHAR_0 = 48;
    private static final int CHAR_1 = 49;
    private RandomAccessFile gpio;
    private int pin;
    private String GPIO_PATH = "/sys/class/gpio/";
    private String GPIO_EXPORT = GPIO_PATH + "export";
    static private long lastTimePush;

    static class KeypadSettings {
        static final int DEBOUNCE_TIME; //ms
        static final int INTERVAL_CHECK; //ms
        static final int DEFAULT_PIN_VALUE; //ms
        static final boolean GPIO_ENABLE;

        static {

            Properties properties = new Properties();
            try ( InputStream inStream = Files.newInputStream(Paths.get(System.getProperty("TRAIN_PATH") + "/gpio.properties"), StandardOpenOption.READ) ) {
                properties.load(inStream);
            } catch ( IOException e ) {
                Logger.getLogger(KeypadSettings.class).error("Can`t find gpio configuration", e);
            }
            DEBOUNCE_TIME = Integer.parseInt(properties.getProperty("DEBOUNCE_TIME", "500")); //ms
            INTERVAL_CHECK = Integer.parseInt(properties.getProperty("INTERVAL_CHECK", "200")); //ms
            DEFAULT_PIN_VALUE = Integer.parseInt(properties.getProperty("DEFAULT_PIN_VALUE", "0")); //ms
            GPIO_ENABLE = Boolean.parseBoolean(properties.getProperty("GPIO_ENABLE", "true"));
        }


    }


    KeypadButtons( int pin ) {
        this.pin = pin;
        if ( GPIO_ENABLE )
            init();
    }


    private void init() {
        try {
            CompletableFuture.runAsync(() -> {
                if ( Files.notExists(Paths.get(GPIO_PATH.concat("gpio" + pin))) ) {
                    try {
                        Runtime.getRuntime().exec(String.format("%s/gpioinit.sh %d", System.getProperty("TRAIN_PATH"), pin)).waitFor();
                        TimeUnit.SECONDS.sleep(1);
                    } catch ( IOException | InterruptedException e ) {
                        e.printStackTrace();
                    }
                }
            })
                    .thenRun(() -> {
                        try {
                            this.gpio = new RandomAccessFile("/sys/class/gpio/gpio" + pin + "/value", "r");
                        } catch ( FileNotFoundException e ) {
                            throw new RuntimeException("GPIO: can`t open pin: " + pin);
                        }
                    }).get();
        } catch ( InterruptedException | ExecutionException e ) {
            e.printStackTrace();
        }
    }

    boolean checkButton() {
        if ( ( System.currentTimeMillis() - lastTimePush ) < DEBOUNCE_TIME ) {
            return false;
        }
        try {
            gpio.seek(0);
            // TODO ::// Сделать проверку на дефалтное значение GPIO
            if ( gpio.readByte() == ( ( DEFAULT_PIN_VALUE == 0 ) ? CHAR_0 : CHAR_1 ) ) {
                lastTimePush = System.currentTimeMillis();
                return true;
            }
            return false;
        } catch ( IOException e ) {
            throw new ReadGpioException("GPIO: can`t read pin:" + pin, e);
        }
    }

}
