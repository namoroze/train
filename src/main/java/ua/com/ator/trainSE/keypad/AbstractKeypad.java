package ua.com.ator.trainSE.keypad;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author Baevskiy Ilya
 * @author Dmitry Sokolov
 */
abstract class AbstractKeypad implements Keypad {

    private EnumMap<KeypadButtons, List<Runnable>> listenerMap = new EnumMap<>(KeypadButtons.class);

    @Override
    public void addKeyListener( Runnable eventListener, KeypadButtons key ) {
        getConsumerListByKey(key).add(eventListener);
    }
    @Override
    public void addKeyListenerForAllKey( Runnable eventListener ) {
        Stream.of(KeypadButtons.values())
                .map(key -> new Pair<>(key, listenerMap.getOrDefault(key, new ArrayList<>())))
                .peek(pairKeyList -> pairKeyList.getValue().add(eventListener))
                .filter(pairKeyList -> listenerMap.containsValue(pairKeyList.getValue()))
                .forEach(pairKeyList -> listenerMap.put(pairKeyList.getKey(), pairKeyList.getValue()));
    }

    private List<Runnable> getConsumerListByKey( KeypadButtons key ) {
        List<Runnable> consumerList = listenerMap.getOrDefault(key, new ArrayList<>());
        listenerMap.put(key, consumerList);
        return consumerList;
    }

    protected void emit( KeypadButtons eventType ) {
        Optional.ofNullable(listenerMap.get(eventType)).ifPresent(listenerList -> listenerList.forEach(Runnable::run));
    }
}

