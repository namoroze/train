package ua.com.ator.trainSE.connectivity.exceptions;

/**
 * Exception for modbus connection open
 * ModbusConnection throws it
 */
public final class ConnectionOpenException extends RuntimeException{
    public ConnectionOpenException(Exception e) {
        super(e);
    }
}
