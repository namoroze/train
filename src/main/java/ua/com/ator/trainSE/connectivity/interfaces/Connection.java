package ua.com.ator.trainSE.connectivity.interfaces;

import ua.com.ator.trainSE.connectivity.exceptions.ConnectionOpenException;

/**
 * Created by User on 13.06.2017.
 */
public interface Connection extends AutoCloseable{

    void open() throws ConnectionOpenException;
}
