package ua.com.ator.trainSE.connectivity.exceptions;


/**
 * Exception for ModbusCommunicationService
 * throws when can`t receive ModbusResponse after request
 */
public final class DeviceCommunicationException extends RuntimeException {
    public DeviceCommunicationException(String s) {
        super(s);
    }
}
