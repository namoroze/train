package ua.com.ator.trainSE.connectivity;

import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.msg.ModbusRequest;
import net.wimpi.modbus.msg.ModbusResponse;
import ua.com.ator.trainSE.connectivity.exceptions.DeviceCommunicationException;
import ua.com.ator.trainSE.connectivity.interfaces.CommunicationService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiFunction;


/**
 * Сервис коммуникации по протоколу Modbus(RTU),
 * работает с ModbusConnection
 * имеет внутренний однопоточный Executor, используется для
 * общения с устройствами Modbus.
 */
public final class ModbusCommunicationService implements CommunicationService<ModbusConnection, ModbusRequest, ModbusResponse> {

    private final ModbusConnection connection;
//    private final ModbusSerialTransaction transaction;

    /* Single-thread executor for modbus connection with priority queue*/
    private final ExecutorService connectionExecutorService = Executors.newSingleThreadExecutor((runnable) -> new Thread(runnable, "ModbusCommunicationService"));

    public ModbusCommunicationService(ModbusConnection newConnection) {

        this.connection = newConnection;
//        transaction =  new ModbusSerialTransaction(connection);
    }

    /**
     * Add new task for connection
     *
     * @param modbusRequest   - requset for modBus transaction
     * @param responseHandler - Handler of response modBus transaction
     */
    @Override
    public void execute(ModbusRequest modbusRequest,
                        BiFunction<ModbusResponse, Throwable, ModbusResponse> responseHandler) {
        addTask(modbusRequest, responseHandler);
    }




    /**
     * Add task to connection executor
     *
     * @param modbusRequest
     * @param responseHandler
     */
    private void addTask(
            ModbusRequest modbusRequest,
            BiFunction<ModbusResponse, Throwable, ModbusResponse> responseHandler) {

        connectionExecutorService.execute(() -> {
            try {
                ModbusResponse modbusResponse = sendRequest(modbusRequest);
                responseHandler.apply(modbusResponse, null);
            } catch (DeviceCommunicationException e) {
                responseHandler.apply(null, e);
            }

        });
    }


    /**
     * @param request - ModbusRequest to modbus device(net.wimpi.modbus)
     * @return ModbusResponse - response from modbus device
     * @throws DeviceCommunicationException
     */
    private ModbusResponse sendRequest(ModbusRequest request) throws DeviceCommunicationException {
        // Create transaction for request
        ModbusSerialTransaction transaction = new ModbusSerialTransaction(connection);
        transaction.setRequest(request);
        try {
            transaction.execute();
        } catch (ModbusException e) {
            throw new DeviceCommunicationException("Device communicate Error!");
        }
        return transaction.getResponse();
    }

    @Override
    protected void finalize() throws Throwable {
        connectionExecutorService.shutdownNow();
        connection.close();
    }

}

