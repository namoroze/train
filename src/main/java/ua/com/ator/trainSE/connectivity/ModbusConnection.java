package ua.com.ator.trainSE.connectivity;

import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.util.SerialParameters;
import org.springframework.beans.factory.InitializingBean;
import ua.com.ator.trainSE.connectivity.exceptions.ConnectionOpenException;
import ua.com.ator.trainSE.connectivity.interfaces.Connection;


/**
 * Created by User on 12.06.2017.
 */
public final class ModbusConnection extends SerialConnection implements Connection, InitializingBean {
    public ModbusConnection(SerialParameters parameters) {
        super(parameters);
    }

    @Override
    public void open() throws ConnectionOpenException {
        try {
            super.open();
            getSerialPort().setDTR(false);
        } catch (Exception e) {
            throw new ConnectionOpenException(e);
        }
    }

    @Override
    public void close() {
        super.close();
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        open();
    }
}

