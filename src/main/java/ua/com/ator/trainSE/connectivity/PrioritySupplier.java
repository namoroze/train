package ua.com.ator.trainSE.connectivity;

import ua.com.ator.trainSE.connectivity.interfaces.WithPriority;

import java.util.function.Supplier;

public final class PrioritySupplier<T> implements Supplier<T>, WithPriority {
    private final int priority;
    private final Supplier<T> supplier;
    public PrioritySupplier(int priority, Supplier<T> supplier) {
        this.priority = priority;
        this.supplier = supplier;
    }
    @Override
    public T get() {
        return supplier.get();
    }
    @Override
    public int priority() {
        return priority;
    }
}
