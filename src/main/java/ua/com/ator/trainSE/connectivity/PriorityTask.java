package ua.com.ator.trainSE.connectivity;

/**
 * Created by User on 20.06.2017.
 */
final class PriorityTask {
    public static final int LOW_PRIORITY = 0;
    public static final int NORMAL_PRIORITY = 1;
    public static final int HIGH_PRIORITY = 2;
}
