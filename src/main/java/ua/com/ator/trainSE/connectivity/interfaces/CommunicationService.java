package ua.com.ator.trainSE.connectivity.interfaces;



import java.util.function.BiFunction;

/**
 * Created by User on 13.06.2017.
 */
public interface CommunicationService<T extends Connection, U, R> {

//    ScheduledFuture<?> schedule(long pullingInterval,
//                                int priority,
//                                U modbusRequest,
//                                BiFunction<R, Throwable, R> responseHandler);

    void execute(U modbusRequest,
                 BiFunction<R, Throwable, R> responseHandler

    );
}



