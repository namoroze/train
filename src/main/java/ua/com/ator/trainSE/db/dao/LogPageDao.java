package ua.com.ator.trainSE.db.dao;

import org.apache.log4j.Logger;
import ua.com.ator.trainSE.db.entity.LogPageViewMetaInfo;
import ua.com.ator.trainSE.db.entity.LogRow;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static ua.com.ator.trainSE.db.ConnectionDB.createConnection;

/**
 * @author Shokolov Dmitry
 */
public class LogPageDao implements ILogPageDao {
    private final Logger logger = Logger.getLogger(LogPageDao.class);

    private static String countFormat = "select count(*) from %s";

    private static String getPageRowsQuery = "select l.`timestamp`, l.`value`, m.`is_format`, m.`mnemonic` " +
            "from %s as l " +
            "left outer join `messages` as m on l.`code` = m.`id` " +
            "order by l.`timeStamp` " +
            "desc limit ?, ?";

    private static String selectViews = "select  v.view_name, v.page_name,  GROUP_CONCAT(vm.message_id) as ids " +
            "from views v " +
            "LEFT JOIN view_messages vm ON v.id = vm.view_id " +
            "group by v.id";

    private static String createViews = "create or replace view %s as select * from logs as l where code in (%s)";

    private static String deleteOldLogs = "delete from logs where timestamp < (NOW() - INTERVAL ? DAY)";

    public LogPageDao() {
        Properties properties = new Properties();
        try ( InputStream inStream = Files.newInputStream(Paths.get(System.getProperty("TRAIN_PATH") + "/config.properties"), StandardOpenOption.READ) ) {
            properties.load(inStream);
        } catch ( IOException e ) {
            logger.error("Can`t find controller configuration", e);
        }

        deleteOldLogs(Integer.parseInt(properties.getProperty("LOG_STORE_DAYS", "60")));
    }

    /**
     * вернет доступные вью для страницы лога
     *
     * @return список вью
     */
    @Override
    public List<LogPageViewMetaInfo> getAvailableViews() {

        List<LogPageViewMetaInfo> logPageViews = new LinkedList<>();
        try ( Connection connection = createConnection();
              Statement statement = connection.createStatement() ) {
            ResultSet resultSet = statement.executeQuery(selectViews);

            while ( resultSet.next() ) {
                logPageViews.add(getView(resultSet));
            }

        } catch ( SQLException e ) {
            logger.error(e);
        }
        return logPageViews;
    }

    /**
     * удалит старые логи
     *
     * @param beforeDays количество дней за которые будут оставлены логи
     */
    private void deleteOldLogs( int beforeDays ) {
        try ( Connection connection = createConnection();
              PreparedStatement statement = connection.prepareStatement(deleteOldLogs) ) {
            statement.setInt(1, beforeDays);

            statement.executeUpdate();

        } catch ( SQLException e ) {
            logger.error(e);
        }
    }

    /**
     * создаст новые представления для страниц лога
     *
     * @param logPageViews список необходимых представлений
     */
    @Override
    public void createViews( List<LogPageViewMetaInfo> logPageViews ) {


        try ( Connection connection = createConnection();
              Statement statement = connection.createStatement() ) {

            logPageViews.stream()
                    .map(logPageView -> String.format(createViews, logPageView.getViewName(), logPageView.getCodes()))
                    .forEach(query -> {
                        try {
                            statement.addBatch(query);
                        } catch ( SQLException e ) {
                            throw new RuntimeException(e);
                        }
                    });

            statement.executeBatch();

        } catch ( SQLException | RuntimeException e ) {
            logger.error(e);
        }
    }

    /**
     * найдет деоюходимое количество записей для определенной страницы логов
     *
     * @param logViewName   имя страницы
     * @param countPageRows количество строк
     * @param pageNumber    ноиер страницы
     * @return список строк
     */
    @Override
    public List<LogRow> findLogRows( String logViewName, int countPageRows, int pageNumber ) {
        List<LogRow> errors = new LinkedList<>();

        try ( Connection connection = createConnection();
              PreparedStatement statement = connection.prepareStatement(getPageRowsQuery.replace("%s", logViewName)); ) {

            statement.setInt(1, getStartForLimit(pageNumber, countPageRows));
            statement.setInt(2, countPageRows);
            try ( ResultSet resultSet = statement.executeQuery() ) {
                while ( resultSet.next() ) {
                    errors.add(getLogRow(resultSet));
                }
            }
        } catch ( SQLException e ) {
            logger.error(e);
        }
        return errors;
    }

    /**
     * определит количество страниц
     *
     * @param logViewName   имя страницы логов
     * @param countPageRows количество строк на странице
     * @return количество страниц
     */
    @Override
    public int getNumberPages( String logViewName, int countPageRows ) {
        int count = count(logViewName);
        return count / countPageRows + ( ( count % countPageRows == 0 ) ? 0 : 1 );
    }

    private LogRow getLogRow( ResultSet resultSet ) throws SQLException {
        return new LogRow(
                resultSet.getTimestamp("timestamp"),
                resultSet.getDouble("value"),
                resultSet.getString("mnemonic"),
                resultSet.getBoolean("is_format")
        );
    }

    /**
     * определит номер первой стоки страницы
     *
     * @param pageNumber номер страницы
     * @param rowPerPage количество строк на странице
     * @return номер строки
     */
    private static int getStartForLimit( int pageNumber, int rowPerPage ) {
        return pageNumber != 1 ? ( pageNumber - 1 ) * rowPerPage : 0;
    }

    /**
     * вернет общее количество для страницы лога
     *
     * @param logViewName имя страницы
     * @return количество строк
     */
    private int count( String logViewName ) {
        int count = 0;


        try ( Connection connection = createConnection();
              PreparedStatement statementCount = connection.prepareStatement(String.format(countFormat, logViewName));
              ResultSet resultSet = statementCount.executeQuery() ) {

            if ( resultSet.next() ) {
                count = resultSet.getInt(1);
            }

        } catch ( SQLException e ) {
            logger.error(e);
        }
        return count;
    }

    /**
     * создаст объект LogPageView
     *
     * @param rs ResultSet
     * @return LogPageView instance
     * @throws SQLException if the columnLabel is not valid;
     *                      if a database access error occurs or this method is
     *                      called on a closed result set
     */
    private LogPageViewMetaInfo getView( ResultSet rs ) throws SQLException {
        return new LogPageViewMetaInfo(
                rs.getString("view_name"),
                rs.getString("page_name"),
                rs.getString("ids")
        );
    }
}
