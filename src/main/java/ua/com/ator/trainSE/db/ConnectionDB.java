package ua.com.ator.trainSE.db;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import javafx.application.Platform;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import static java.lang.System.getProperty;
// TODO :: / ПЕРЕСМОТРЕТЬ ДБ

/**
 * @author Shokolov Dmitry
 */
public abstract class ConnectionDB {

    private final static Logger logger = Logger.getLogger(ConnectionDB.class);
    private static String url;
    private static String port;
    private static String user;
    private static String password;
    private static String driver;
    private static int poolSize;
    private static String name;

    private static DataSource dataSource;

    static {
        url = getProperty("authorisation.db.url", "127.0.0.1");
        port = getProperty("authorisation.db.port", "3306");
        user = getProperty("authorisation.db.user", "root");
        password = getProperty("authorisation.db.password", "");
        driver = getProperty("authorisation.db.driver", "com.mysql.jdbc.Driver");
        poolSize = Integer.parseInt(getProperty("authorisation.db.pool_size", "1"));
        name = getProperty("authorisation.db.name", "trainse");

        try {
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException e) {
            logger.error("Driver is not exist.");
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error(e);
        }
        dataSource = getDataSource();
    }

    public static Connection createConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            logger.error(e);
            Platform.exit();
        }
        return null;
    }

    private static DataSource getDataSource() {
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.getCheckoutTimeout();
        try {
            cpds.setDriverClass(driver);
        } catch (PropertyVetoException e) {
            logger.error(e);
        }
        cpds.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s", url, port, name));
        cpds.setUser(user);
        cpds.setPassword(password);

        cpds.setInitialPoolSize(1);
        cpds.setMinPoolSize(1);

        cpds.setMaxIdleTime(60);
        cpds.setAcquireIncrement(1);

        cpds.setMaxPoolSize(poolSize);
        cpds.setMaxStatements(180);

        return cpds;
    }
}
