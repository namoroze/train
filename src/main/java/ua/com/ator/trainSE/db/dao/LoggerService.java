package ua.com.ator.trainSE.db.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;
import ua.com.ator.trainSE.db.entity.State;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ua.com.ator.trainSE.db.ConnectionDB.createConnection;

/**
 * сервис для логиравания изменения состояния системы
 *
 * @author Dmitry Sokolov on 01.08.2017.
 */
@Component
public class LoggerService implements DisposableBean{

    private static String createLogQuery = "insert into logs (code, timestamp, value) values (?, ?, ?)";

    private final Logger logger = Logger.getLogger(LoggerService.class);
    private final ExecutorService service;

    public LoggerService() {
        this.service = Executors.newSingleThreadExecutor((r -> new Thread(r, "LogServiceExecutor")));
    }

    /**
     * Will save the state change to the database
     *
     * @param state state
     */
    public void log(State state) {
        service.execute(() -> {

            try (Connection connection = createConnection();
                 PreparedStatement statement = connection.prepareStatement(createLogQuery)) {
                statement.setInt(1, state.getCode());
                statement.setTimestamp(2, state.getTimestamp());
                statement.setDouble(3, state.getValue());
                statement.executeUpdate();
            } catch (SQLException e) {
                logger.error(e);
            }
        });
    }

    @Override
    public void destroy() throws Exception {
        service.shutdownNow();
    }
}
