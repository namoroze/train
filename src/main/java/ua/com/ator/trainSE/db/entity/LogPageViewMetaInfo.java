package ua.com.ator.trainSE.db.entity;

/**
 * информация о странице лога:
 * имя вью, имя страницы отображаемой пользователю,
 * идентификаторы сообщений отображаемых на странице
 *
 * @author Dmitry Sokolov on 26.07.2017.
 */
public class LogPageViewMetaInfo {
    private final String viewName;
    private final String pageName;
    private final String codes;

    /**
     * конструктор
     *
     * @param viewName имя вью
     * @param pageName имя страницы
     * @param codes коды
     */
    public LogPageViewMetaInfo(String viewName, String pageName, String codes) {
        this.viewName = viewName;
        this.pageName = pageName;
        this.codes = codes;
    }

    /**
     * вернет имя представления в бд
     * @return  вью
     */
    public String getViewName() {
        return viewName;
    }
    /**
     * вернет имя отображаемое на странице
     * @return  имя
     */
    public String getPageName() {
        return pageName;
    }
    /**
     * вернет коды сообщений на странице
     * @return  коды
     */
    public String getCodes() {
        return codes;
    }
}
