package ua.com.ator.trainSE.db.dao;

import ua.com.ator.trainSE.db.entity.LogPageViewMetaInfo;
import ua.com.ator.trainSE.db.entity.LogRow;

import java.util.List;

/**
 * @author Dmitry Sokolov on 23.11.2017.
 */
public interface ILogPageDao {
    /**
     * вернет доступные вью для страницы лога
     *
     * @return список вью
     */
    List<LogPageViewMetaInfo> getAvailableViews();
    /**
     * создаст новые представления для страниц лога
     *
     * @param logPageViews список необходимых представлений
     */
    void createViews(List<LogPageViewMetaInfo> logPageViews);
    /**
     * найдет деоюходимое количество записей для определенной страницы логов
     *
     * @param logViewName   имя страницы
     * @param countPageRows количество строк
     * @param pageNumber    ноиер страницы
     * @return список строк
     */
    List<LogRow> findLogRows(String logViewName, int countPageRows, int pageNumber);
    /**
     * определит количество страниц
     *
     * @param logViewName   имя страницы логов
     * @param countPageRows количество строк на странице
     * @return количество страниц
     */
    int getNumberPages(String logViewName, int countPageRows);
}
