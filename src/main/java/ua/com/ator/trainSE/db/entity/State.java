package ua.com.ator.trainSE.db.entity;

import java.sql.Timestamp;

/**
 * строка лога в бд
 *
 * @author Shokolov Dmitry
 */
public class State {

    private int code;
    private final Timestamp timestamp;
    private double value;

    public State(int code, double value) {
        this.code = code;
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.value = value;
    }
    public State(int code) {
        this(code,0);
    }

    public int getCode() {
        return code;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }
}
