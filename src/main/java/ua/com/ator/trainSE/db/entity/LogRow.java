package ua.com.ator.trainSE.db.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * одна строка на странице логов
 *
 * @author Dmitry Sokolov on 25.07.2017.
 */
public class LogRow {
    private final Timestamp timestamp;
    private final double value;
    private final String mnemonic;
    private final boolean isFormat;
    private final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss  dd-MM-yyyy");

    public LogRow(Timestamp timestamp, double value, String mnemonic, boolean isFormat) {
        this.timestamp = timestamp;
        this.value = value;
        this.mnemonic = mnemonic;
        this.isFormat = isFormat;
    }

    @Override
    public String toString() {
        return formatter.format(timestamp) + "     " + (isFormat ? String.format(mnemonic, value) : mnemonic);
    }
}
