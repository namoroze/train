package ua.com.ator.trainSE.loggger;

import javafx.beans.property.BooleanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.ator.trainSE.db.dao.LoggerService;
import ua.com.ator.trainSE.db.entity.State;
import ua.com.ator.trainSE.devices.ConditionerBoard;
import ua.com.ator.trainSE.devices.IOControllerDevice;

import javax.annotation.PostConstruct;

import static ua.com.ator.trainSE.loggger.Logger.Codes.*;


/**
 * логер, с помощью LoggerService логирует изменения состояния в бд
 *
 * @author Dmitry Sokolov on 27.10.2017.
 */
@Component
public class Logger {
    @Autowired
    private static LoggerService service = new LoggerService();
    @Autowired
    private IOControllerDevice device;
    @Autowired
    private ConditionerBoard conditionerBoard;

    private boolean canBeLogged = false;

    /**
     * конструктор
     */
    public Logger() {
    }

    public void logWorking(BooleanProperty workingProperty) {
// увімкнення | вимкнення
        workingProperty.addListener((observable, oldValue, newValue) -> {
            service.log(new State(newValue ? ON : OFF));
            this.canBeLogged = newValue;
        });
    }

    @PostConstruct
    public void init() {
// Аварія
        device.isCrashProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue) service.log(new State(CRASH));
                });

// Автоматичний режим включено | Автоматичний режим вимкнено
// &
// Ручний режим включено | Ручний режим вимкнено
        device.isAutomaticModeProperty()
                .addListener((observable, oldValue, newValue) -> {
                    service.log(new State(newValue ? AUTO_ON : AUTO_OFF));
                    service.log(new State(newValue ? MANUAL_OFF : MANUAL_ON));
                });

// Відсутність зв'язку з блоком введення-виведення
        device.connectionErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue) service.log(new State(CONNECTION_IO_ERROR));
                });
        /**
         * кондиционер
         */
// Відсутність зв'язку з кондиціонером
        conditionerBoard.connectionErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue)
                        service.log(new State(CONDITIONER_CONNECTION_ERROR));
                });
// Кондиционер РОБОТА увімкнено | Кондиционер РОБОТА вимкнено
        device.avariyaPvProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ? CONDITIONER_AVARIYA_PV_ON : CONDITIONER_AVARIYA_PV_OFF))
                );
// Вентиляція увімкнено | Вентиляція вимкнено
        device.conditionerVentilationProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ?
                                (device.isAutomaticModeProperty().get() ? CONDITIONER_VENTILATION_ON : MANUAL_CONDITIONER_MODE_ON)
                                : (device.isAutomaticModeProperty().get() ? CONDITIONER_VENTILATION_OFF : MANUAL_CONDITIONER_MODE_OFF)))
                );
// Кондиционер ВІДМОВА увімкнено | Кондиционер ВІДМОВА вимкнено
        device.peregrivNagrivachivConditioneraProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue & !oldValue ? CONDITIONER_PEREGRIV_NAGRIVACHIV_ON : CONDITIONER_PEREGRIV_NAGRIVACHIV_OFF))
                );
// ВІДМОВА конт.охол.1 увімкнено | ВІДМОВА конт.охол.1 вимкнено
        device.firstCompressorErrorProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue & !oldValue ? CONDITIONER_COMPRESSOR_1_GR_ERROR_ON : CONDITIONER_COMPRESSOR_1_GR_ERROR_OFF))
                );
// ВІДМОВА конт.охол.2 увімкнено | ВІДМОВА конт.охол.2 вимкнено
        device.secondCompressorErrorProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue & !oldValue ? CONDITIONER_COMPRESSOR_2_GR_ERROR_ON : CONDITIONER_COMPRESSOR_2_GR_ERROR_OFF))
                );
// Охолодження увімкнено | Охолодження вимкнено
        device.coolerStateProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue & !oldValue ? CONDITIONER_COOLER_ON : CONDITIONER_COOLER_OFF))
                );
// Ел.Калорифер увімкнено | Ел.Калорифер вимкнено
        device.conditionerHeaterStateProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue & !oldValue ? CONDITIONER_HEATER_ON : CONDITIONER_HEATER_OFF))
                );

        device.peregrivVkProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? PERGRIV_VK_ON : PERGRIV_VK_OFF));
                    }
                });

        device.zasorFiltraProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? ZASOR_FILTRA_ON : ZASOR_FILTRA_OFF));
                    }
                });

       device.avariaReleDavleniyaProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? AVRIA_RELE_DAVLENIYA_ON : AVRIA_RELE_DAVLENIYA_OFF));
                    }
                });


       device.avariyaPeretvoruvachaK1Ug2Property()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? AVARIYA_PERETVORUVACHA_K1_UG2_ON : AVARIYA_PERETVORUVACHA_K1_UG2_OFF));
                    }
                });


       device.avariyaPeretvoruvachaK2Ug3Property()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? AVARIYA_PERETVORUVACHA_K2_UG3_ON : AVARIYA_PERETVORUVACHA_K2_UG3_OFF));
                    }
                });


       device.avariyaPeretvoruvachaPvVkUg1Property()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != oldValue) {
                        service.log(new State(newValue ? AVARIYA_PERETVORUVACHA_PV_VK_UG1_ON : AVARIYA_PERETVORUVACHA_PV_VK_UG1_OFF));
                    }
                });


/**
 * єлектричество
 */
// I max батареї ХХХА
        device.accumulatorHighCurrentErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(I_MAX_BATTERY, device.getCurrentBatterySensor().lastValueProperty().get()));
                });
// I max генератора ХХХА
        device.generatorHighCurrentErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(I_MAX_GENERATOR, device.getCurrentGeneratorSensor().lastValueProperty().get()));
                });
// Umax ЗРК=ХХХВ
        device.highVoltageInputErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(U_MAX_ZRK, device.getBoardNetworkVoltageSensor().lastValueProperty().get()));
                });
// Umax БВВ=ХХХВ
        device.highVoltageOutputErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(U_MAX_IO, device.getBoardNetworkVoltageSensor().lastValueProperty().get()));
                });


// Umin АБ==ХХХВ
        device.dischargedErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(U_MIN_BATTERY, device.getBoardNetworkVoltageSensor().lastValueProperty().get()));
                });
// R ізоляції < 1 кОм
        device.insulationResistanceErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(INSULATION_RESISTANCE_ERROR));
                });
/**
 * датчики
 */
// Обрив датчика температури котла.
        device.getBoilerTemperatureSensor().isErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(BOILER_TEMP_SENSOR_ERROR));
                });
// Обрив датчика температури приточного повітря.
        device.getAirFlowTemperatureSensor().isErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(AIRFLOW_TEMP_SENSOR_ERROR));
                });
// Обрив датчика зовнішньої температури.
        device.getOutsideTemperatureSensor().isErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(OUTSIDE_TEMP_SENSOR_ERROR));
                });
// Обрив датчика температури салону.
        device.getInsideTemperatureSensor().isErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(INSIDE_TEMP_SENSOR_ERROR));
                });

/**
 * отопление
 */

// Температура води в котлі > ХХ°C

        device.boilerHighTemperatureErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue && this.canBeLogged)
                        service.log(new State(
                                device.isAutomaticModeProperty().get() ?
                                        HIGH_BOILER_TEMPERATURE : MANUAL_HIGH_BOILER_TEMPERATURE,
                                device.getBoilerTemperatureSensor().lastValueProperty().get()));
                });
// Увімкнено НВО | Вимкнено НВО
        device.lowVoltageHeatingInputStateProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ?
                                (device.isAutomaticModeProperty().get() ? NVO_ON : MANUAL_NVO_ON)
                                : (device.isAutomaticModeProperty().get() ? NVO_OFF : MANUAL_NVO_OFF))));

// Відсутній відгук від НВО
        device.lowVoltageHeatingErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue)
                        service.log(new State(device.isAutomaticModeProperty().get() ?
                                NVO_ERROR : MANUAL_NVO_ERROR));
                });

// Увімкнено ВВО 1 гр. | Вимкнено ВВО 1 гр.
        device.highVoltageHeatingFirstGroupInputStateProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ?
                                (device.isAutomaticModeProperty().get() ? VVO_1_GR_ON : MANUAL_VVO_1_GR_ON)
                                : (device.isAutomaticModeProperty().get() ? VVO_1_GR_OFF : MANUAL_VVO_1_GR_OFF))));

// Увімкнено ВВО 2 гр. | Вимкнено ВВО 2 гр.
        device.highVoltageHeatingSecondGroupInputStateProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ?
                                (device.isAutomaticModeProperty().get() ? VVO_2_GR_ON : MANUAL_VVO_2_GR_ON)
                                : (device.isAutomaticModeProperty().get() ? VVO_2_GR_OFF : MANUAL_VVO_2_GR_OFF))));
// Відсутній відгук від ВВО 1 гр
        device.highVoltageHeatingFirstGroupErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        service.log(new State(device.isAutomaticModeProperty().get() ?
                                VVO_1_GR_ERROR : MANUAL_VVO_1_GR_ERROR));
                    }
                });
// Відсутній відгук від ВВО 2 гр
        device.highVoltageHeatingSecondGroupErrorProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        service.log(new State(device.isAutomaticModeProperty().get() ?
                                VVO_2_GR_ERROR : MANUAL_VVO_2_GR_ERROR));
                    }
                });
// Насос опалення увімкнено | Насос опалення вимкнено
        device.pumpWorkingProperty()
                .addListener((observable, oldValue, newValue) ->
                        service.log(new State(newValue ?
                                (device.isAutomaticModeProperty().get() ? PUMP_ON : MANUAL_PUMP_ON)
                                : (device.isAutomaticModeProperty().get() ? PUMP_OFF : MANUAL_PUMP_OFF))));

// Відсутня напруга 3000В
        device.no3kvProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        service.log(new State(device.isAutomaticModeProperty().get() ?
                                NO_3kV_ERROR : MANUAL_NO_3kV_ERROR));
                    }
                });
    }

    /**
     * ids сообщений в бд
     */
    static class Codes {
        static final int
                I_MAX_BATTERY = 1,
                HIGH_BOILER_TEMPERATURE = 2,
                NO_3kV_ERROR = 3,
                U_MAX_IO = 4,
                CONNECTION_IO_ERROR = 5,
                U_MAX_ZRK = 6,
                INSULATION_RESISTANCE_ERROR = 7,
                VVO_1_GR_ON = 8,
                VVO_2_GR_ON = 9,
                VVO_1_GR_OFF = 10,
                VVO_2_GR_OFF = 11,
                NVO_ON = 12,
                NVO_OFF = 13,
                PUMP_ON = 14,
                PUMP_OFF = 15,
                AUTO_ON = 16,
                AUTO_OFF = 17,
                OFF = 18,
                ON = 19,
                CRASH = 20,
                OUTSIDE_TEMP_SENSOR_ERROR = 21,
                BOILER_TEMP_SENSOR_ERROR = 22,
                AIRFLOW_TEMP_SENSOR_ERROR = 23,
                INSIDE_TEMP_SENSOR_ERROR = 24,
                I_MAX_GENERATOR = 25,
                U_MIN_BATTERY = 26,
                CONDITIONER_PEREGRIV_NAGRIVACHIV_ON = 27,
                CONDITIONER_PEREGRIV_NAGRIVACHIV_OFF = 28,
                CONDITIONER_AVARIYA_PV_ON = 29,
                CONDITIONER_AVARIYA_PV_OFF = 30,
                CONDITIONER_COMPRESSOR_1_GR_ERROR_ON = 31,
                CONDITIONER_COMPRESSOR_1_GR_ERROR_OFF = 32,
                CONDITIONER_COMPRESSOR_2_GR_ERROR_ON = 33,
                CONDITIONER_COMPRESSOR_2_GR_ERROR_OFF = 34,
                CONDITIONER_COOLER_ON = 35,
                CONDITIONER_COOLER_OFF = 36,
                CONDITIONER_HEATER_ON = 37,
                CONDITIONER_HEATER_OFF = 38,
                CONDITIONER_CONNECTION_ERROR = 39,
                NVO_ERROR = 40,
                VVO_1_GR_ERROR = 41,
                VVO_2_GR_ERROR = 42,
                MANUAL_HIGH_BOILER_TEMPERATURE = 43,
                MANUAL_VVO_1_GR_ON = 44,
                MANUAL_VVO_2_GR_ON = 45,
                MANUAL_VVO_1_GR_OFF = 46,
                MANUAL_VVO_2_GR_OFF = 47,
                MANUAL_NVO_ON = 48,
                MANUAL_NVO_OFF = 49,
                MANUAL_PUMP_ON = 50,
                MANUAL_PUMP_OFF = 51,
                MANUAL_ON = 52,
                MANUAL_OFF = 53,
                MANUAL_NVO_ERROR = 54,
                MANUAL_VVO_1_GR_ERROR = 55,
                MANUAL_VVO_2_GR_ERROR = 56,
                MANUAL_NO_3kV_ERROR = 57,
                CONDITIONER_VENTILATION_ON = 58,
                CONDITIONER_VENTILATION_OFF = 59,
                MANUAL_CONDITIONER_MODE_ON = 60,
                MANUAL_CONDITIONER_MODE_OFF = 61,
                PERGRIV_VK_ON = 62,
                PERGRIV_VK_OFF = 63,
                ZASOR_FILTRA_ON = 64,
                ZASOR_FILTRA_OFF = 65,
                AVRIA_RELE_DAVLENIYA_ON = 66,
                AVRIA_RELE_DAVLENIYA_OFF = 67,
                AVARIYA_PERETVORUVACHA_K1_UG2_ON = 68,
                AVARIYA_PERETVORUVACHA_K1_UG2_OFF = 69,
                AVARIYA_PERETVORUVACHA_K2_UG3_ON = 70,
                AVARIYA_PERETVORUVACHA_K2_UG3_OFF = 71,
                AVARIYA_PERETVORUVACHA_PV_VK_UG1_ON = 72,
                AVARIYA_PERETVORUVACHA_PV_VK_UG1_OFF = 73;

//            (62, 'Перегрів ВК увімкнено', 0),
//            (63, 'Перегрів ВК вимкнено', 0),
//            (64, 'Засмітився фильтр увімкнено', 0),
//            (65, 'Засмітився фильтр вимкнено', 0),
//            (66, 'Авария реле давления увімкнено', 0),
//            (67, 'Авария реле давления вимкнено', 0),
//            (68, 'Аварія перетворювача K1(UG2) увімкнено', 0),
//            (69, 'Аварія перетворювача K1(UG2) вимкнено', 0),
//            (70, 'Аварія перетворювача K2(UG3) увімкнено', 0),
//            (71, 'Аварія перетворювача K2(UG3) вимкнено', 0),
//            (72, 'Аварія перетворювача ПВ,ВК(UG1) увімкнено', 0),
//            (73, 'Аварія перетворювача ПВ,ВК(UG1) вимкнено', 0);


    }
}
