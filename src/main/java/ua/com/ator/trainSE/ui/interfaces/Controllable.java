package ua.com.ator.trainSE.ui.interfaces;

import ua.com.ator.trainSE.keypad.KeypadButtons;

/**
 * класс имплементируюший этот интерфейс
 * получит метод для обработки событий
 *
 * @author Dmitry Sokolov on 27.07.2017.
 */
public interface Controllable {
    /**
     * запустит обработчик по типу
     * @param key button
     */
    void handle(KeypadButtons key);

}
