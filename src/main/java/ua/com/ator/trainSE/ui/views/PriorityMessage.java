package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.ui.interfaces.Message;

/**
 * View-контроллер реализация обвертка для разделения
 * приоритета сообщений
 * Вью синглтон
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
public class PriorityMessage extends AbstractMessage {

    // Приоритетное сообщение
    private Message masterMessage;
    // Второстепенное сообщение
    private Message slaveMessage;

    /**
     * конструктор
     *
     * @param masterMessage приоритетное сообщение
     * @param slaveMessage второстепенное сообщение
     */
    public PriorityMessage(Message masterMessage, Message slaveMessage) {
        super(new SimpleBooleanProperty());
        this.masterMessage = masterMessage;
        this.slaveMessage = slaveMessage;
    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {
        return (view != null) ? view : (view = new PriorityMessageView());
    }

    /**
     * вью с приоритетными сообщениями
     */
    private class PriorityMessageView extends AnchorPane {
        AnchorPane masterContainer = new AnchorPane(masterMessage.getView());
        AnchorPane slaveContainer = new AnchorPane(slaveMessage.getView());
        /**
         * конструктор
         */
        private PriorityMessageView() {
            getChildren().addAll(masterContainer, slaveContainer);
            setAnchors(masterContainer);
            setAnchors(slaveContainer);
            setAnchors(this);
            masterContainer.visibleProperty().bind(masterMessage.getVisible());
            slaveContainer.visibleProperty().bind(Bindings.and(slaveMessage.getVisible(), masterMessage.getVisible().not()));
        }
    }
}
