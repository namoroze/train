package ua.com.ator.trainSE.ui.property_adapters;

import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;

/**
 * обвертка проперти для запуска в потоке fx
 *
 * @author Dmitry Sokolov on 13.07.2017.
 */
public class SimpleDoublePropertyAdapter extends SimpleDoubleProperty {
    /**
     * конструктор
     *
     * @param property проперти
     */
    public SimpleDoublePropertyAdapter(ObservableDoubleValue property) {
        this.set(property.doubleValue());
        property.addListener((observable, oldValue, newValue) -> Platform.runLater(() -> this.setValue(newValue.doubleValue())));
    }
}

