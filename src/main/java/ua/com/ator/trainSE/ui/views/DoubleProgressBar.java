package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.transform.Rotate;
import ua.com.ator.trainSE.ui.interfaces.IProgressBar;

import static java.util.Objects.isNull;


/**
 * двойной прогрессбар
 *
 * @author Dmitry Sokolov on 10.07.2017.
 */
class DoubleProgressBar implements IProgressBar {
    private ProgressBarInfo progressBarInfo;

    private double dividerLeft;
    private double dividerRight;
    private BooleanProperty connectionError;
    private DoubleProperty value;
    private Node doubleProgressBar = null;

    /**
     * конструктор
     *
     * @param value значение
     * @param lowerLimit верхний предел
     * @param upperLimit нижний предел
     */
    DoubleProgressBar(BooleanProperty connectionError, DoubleProperty value, final double lowerLimit, final double upperLimit) {
        this.connectionError = connectionError;
        this.value = value;
        this.setProgressBarBoundary(lowerLimit, upperLimit);
        progressBarInfo = new ProgressBarInfo(lowerLimit, upperLimit);

    }
    /**
     * вернет вью
     * @return вью
     */
    @Override
    public Node getView() {

        if (isNull(doubleProgressBar)) {
            DoubleProgressBarView doubleProgressBarView = new DoubleProgressBarView();

            doubleProgressBarView.getLeftBarProperty().bind(
                    Bindings.createDoubleBinding(() -> compute(((value.get() * -1)) / dividerLeft), value));

            doubleProgressBarView.getRightBarProperty().bind(
                    Bindings.createDoubleBinding(() -> compute((value.get() / dividerRight)), value));

            doubleProgressBar = new ProgressBarFramework(progressBarInfo, doubleProgressBarView);
            doubleProgressBar.visibleProperty().bind(connectionError.not());
        }

        return doubleProgressBar;
    }

    private double compute(double value) {
        return (value <= 0) ? 0.0 : ((value >= 1) ? 1.0 : value);
    }

    private void setProgressBarBoundary(double lowerLimit, double upperLimit) {
        dividerLeft = lowerLimit * -1;
        dividerRight = upperLimit;
    }

    /**
     * вью двойного прогрессбара
     */
    private class DoubleProgressBarView extends GridPane {
        private ProgressBar progressBarLeft;
        private ProgressBar progressBarRight;

        private DoubleProgressBarView() {

            progressBarLeft = new ProgressBar();
            progressBarLeft.setPrefSize(196 * progressBarInfo.getLeft() / 100., 10);
            progressBarLeft.getTransforms().setAll(new Rotate(180, progressBarInfo.getLeft() - 4, 4.5));

            progressBarRight = new ProgressBar();
            progressBarRight.setPrefSize(196 * progressBarInfo.getRight() / 100., 10);

            ColumnConstraints columnConstraints1 = new ColumnConstraints();
            columnConstraints1.setPercentWidth(progressBarInfo.getLeft());
            ColumnConstraints columnConstraints2 = new ColumnConstraints();
            columnConstraints2.setPercentWidth(progressBarInfo.getRight());

            getColumnConstraints().addAll(columnConstraints1, columnConstraints2);

            add(progressBarLeft, 0, 0);
            add(progressBarRight, 1, 0);
        }

        private DoubleProperty getLeftBarProperty() {
            return progressBarLeft.progressProperty();
        }

        private DoubleProperty getRightBarProperty() {
            return progressBarRight.progressProperty();
        }
    }
}
