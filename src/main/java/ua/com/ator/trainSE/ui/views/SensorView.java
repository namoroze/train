package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import ua.com.ator.trainSE.ui.interfaces.IndicatorViewable;
import ua.com.ator.trainSE.ui.interfaces.IoDebugViewable;

import java.util.ResourceBundle;

/**
 * вью сенсора
 *
 * @author Dmitry Sokolov on 13.07.2017.
 */
public abstract class SensorView implements IoDebugViewable, IndicatorViewable {

    BooleanProperty isError;
    protected BooleanProperty connectionError;
    protected DoubleProperty value;
    String title;
    String units;
    final double lowerLimit;
    final double upperLimit;
    //TODO:// Сделать красным вьюху сенсора когда запредельные значения
    final BooleanBinding isOutOfLimit;



    //@Autowired
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("locale");
    /**
     * конструктор
     *
     * @param isError    состояние ошибки сенсора
     * @param value      значение
     * @param title      имя сенсора
     * @param units      единицы измерения
     * @param lowerLimit нижний предел
     * @param upperLimit верхний предел
     */
    SensorView(BooleanProperty isError,BooleanProperty connectionError, DoubleProperty value, String title, String units, double lowerLimit, double upperLimit) {
        this.isError = isError;
        this.connectionError = connectionError;
        this.value = value;
        this.title = resourceBundle.getString(title);
        this.units = resourceBundle.getString(units);
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        //Биндинг на превышение предела значений датчика
        isOutOfLimit = Bindings
                .when(value.lessThanOrEqualTo(upperLimit).and(value.greaterThanOrEqualTo(lowerLimit)))
                .then(false)
                .otherwise(true);
    }
    /**
     * вернет вью для страницы дэбага
     * @return вью
     */
    @Override
    public Node getDebugView() {
        DebugViewElement debugElement = new DebugViewElement(title);
        debugElement.getValueProperty().bind(
                new When(isError)
                        .then(resourceBundle.getString("sensor.damage.inline"))
                        .otherwise(new When(connectionError)
                        .then("--")
                        .otherwise(Bindings.format("%4.1f", value)))
        );
        return debugElement;
    }
}