package ua.com.ator.trainSE.ui.property_adapters;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;


public class ObjectPropertyAdapter<T> extends SimpleObjectProperty<T> {
    public ObjectPropertyAdapter( ObjectProperty<T> property ) {
        this.set(property.getValue());
        property.addListener(( observable, oldValue, newValue ) -> Platform.runLater(() -> this.set(newValue)));
    }
}
