package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.ui.interfaces.IProgressBar;

import java.util.ResourceBundle;


/**
 * вью индикатора
 *
 * @author Dmitry Sokolov on 07.07.2017.
 */
class IndicatorFrameworkView extends AnchorPane {


    private ResourceBundle bundle = ResourceBundle.getBundle("locale");

    /**
     * конструктор
     *
     * @param name         имя сенсора
     * @param units        единицы измерения
     * @param error        состояние ошибки сенсора
     * @param value        значение
     * @param progressBar  прогрессбар
     * @param isOutOfLimit выход за предел
     */
    IndicatorFrameworkView(BooleanProperty connectionError,
                           String name,
                           String units,
                           BooleanProperty error,
                           DoubleProperty value,
                           IProgressBar progressBar,
                           BooleanBinding isOutOfLimit) {
        this(connectionError,name, units, value, progressBar, isOutOfLimit);

        Label dangerLabel = new Label();
        dangerLabel.getStyleClass().addAll("danger-label");
        dangerLabel.setText("  " + bundle.getString("sensor.damage"));
        dangerLabel.visibleProperty().bind(error);

        AnchorPane.setBottomAnchor(dangerLabel, 5.0);
        AnchorPane.setLeftAnchor(dangerLabel, 5.0);
        AnchorPane.setRightAnchor(dangerLabel, 5.0);
        AnchorPane.setTopAnchor(dangerLabel, 30.0);

        getChildren().add(dangerLabel);
    }
    /**
     * конструктор
     *
     * @param name         имя сенсора
     * @param units        единицы измерения
     * @param value        значение
     * @param progressBar  прогрессбар
     * @param isOutOfLimit выход за предел
     */
    IndicatorFrameworkView(BooleanProperty connectionError,
                           String name,
                           String units,
                           DoubleProperty value,
                           IProgressBar progressBar,
                           BooleanBinding isOutOfLimit) {
        Label descriptionLabel = new Label();
        descriptionLabel.getStyleClass().addAll("description-label");
        descriptionLabel.setText(name);

        Label valueLabel = new Label();
        valueLabel.getStyleClass().addAll("value-label", isOutOfLimit.get() ? "out-of-limit" : "");
        valueLabel.textProperty().bind( new When(connectionError)
                .then("--")
                .otherwise(Bindings.createStringBinding(() -> String.format("%2.0f%s", value.get(), units), value))
        );

        isOutOfLimit.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                valueLabel.getStyleClass().add("out-of-limit");
                getStyleClass().add("out-of-limit");
            } else {
                valueLabel.getStyleClass().remove("out-of-limit");
                getStyleClass().remove("out-of-limit");
            }
        });

        AnchorPane.setLeftAnchor(descriptionLabel, 5.0);
        AnchorPane.setRightAnchor(descriptionLabel, 5.0);
        AnchorPane.setTopAnchor(descriptionLabel, 5.0);

        AnchorPane.setLeftAnchor(valueLabel, 5.0);
        AnchorPane.setRightAnchor(valueLabel, 5.0);
        AnchorPane.setTopAnchor(valueLabel, 30.0);

        AnchorPane.setLeftAnchor(progressBar.getView(), 5.0);
        AnchorPane.setRightAnchor(progressBar.getView(), 5.0);
        AnchorPane.setBottomAnchor(progressBar.getView(), 5.0);

        getChildren().addAll(descriptionLabel, valueLabel, progressBar.getView());
        setPrefWidth(196);
        setPrefHeight(126);
        getStyleClass().addAll("bordered-pane", isOutOfLimit.get() ? "out-of-limit" : "");
    }

}


