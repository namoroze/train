package ua.com.ator.trainSE.ui.pages;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import ua.com.ator.trainSE.devices.Version;
import ua.com.ator.trainSE.ui.property_adapters.ObjectPropertyAdapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static ua.com.ator.trainSE.keypad.KeypadButtons.F1;
import static ua.com.ator.trainSE.keypad.KeypadButtons.F2;

/**
 * Информационная страница
 *
 * @author Dmitry Sokolov on 01.08.2017.
 */
public class InfoPage extends Page {
    private String info;
    private InfoPageView pageView = null;
    private ResourceBundle bundle = ResourceBundle.getBundle("locale");
    private ObjectProperty<Version> versionIO;
    private ObjectProperty<Version> versionCondBoard;
    private IntegerProperty totalWorkHoursProperty;
    private StringProperty softVersionProperty;
    private StringProperty wagonNumberProperty;
    private StringProperty deployDateProperty;

    /**
     * Конструктор, при инициализации объекта загружается файл "info.txt" из корня проекта
     */
    public InfoPage( ObjectProperty<Version> versionIO,
                     ObjectProperty<Version> versionCondBoard,
                     IntegerProperty totalWorkHoursProperty,
                     String softVersionProperty,
                     String wagonNumberProperty,
                     String deployDateProperty
    ) {
        this.totalWorkHoursProperty = totalWorkHoursProperty;
        this.versionIO = new ObjectPropertyAdapter<>(versionIO);
        this.versionCondBoard = new ObjectPropertyAdapter<>(versionCondBoard);
        this.softVersionProperty = new SimpleStringProperty(softVersionProperty);
        this.wagonNumberProperty = new SimpleStringProperty(wagonNumberProperty);
        this.deployDateProperty = new SimpleStringProperty(deployDateProperty);

        try {
            info = Files.readAllLines(Paths.get(System.getProperty("TRAIN_PATH")+"/info.txt")).stream().collect(Collectors.joining("\n"));
        } catch ( IOException e ) {
            info = bundle.getString("info.page.warning.message");
        }
    }

    /**
     * Вернет вью страницы
     *
     * @return Node
     */
    @Override
    public Node getView() {
        if ( isNull(pageView) ) {
            pageView = new InfoPageView();
        }
        return pageView;
    }

    /**
     * Инициализация кнопок
     */
    @Override
    protected void initButtons() {
        buttonTray.setButtons(F1, defaultButtons.get(F1));
        buttonTray.setButtons(F2, defaultButtons.get(F2));
    }


    /**
     * вью информационной страницы
     */
    private class InfoPageView extends AnchorPane {

        private InfoPageView() {
            this.getChildren().addAll(getInfoPane(), getSoftPane(), buttonTray.getView());
            this.visibleProperty().setValue(false);
        }
    }

    private Node getInfoPane() {
        TextFlow infoPane = new TextFlow(new Text(info));
        infoPane.getStyleClass().addAll("bordered-pane", "info-text");
        infoPane.setPrefSize(583, 446);
        infoPane.setLayoutX(5);
        infoPane.setLayoutY(5);
        return infoPane;
    }

    private Node getSoftPane() {
        ImageView imageView = new ImageView("file:///"+System.getProperty("TRAIN_PATH")+"/logo.png");
        imageView.setLayoutY(10);

        AnchorPane anchorIOVersion = createVersionAnchorPane(bundle.getString("label.version.io_firmware"), versionIO);
        anchorIOVersion.setLayoutY(110);

        AnchorPane anchorConditionerVersion = createVersionAnchorPane(bundle.getString("label.version.io_conditioner"), versionCondBoard);
        anchorConditionerVersion.setLayoutY(180);

        AnchorPane anchorSoftVersion = createTextAnchorPane(bundle.getString("label.version.control"), softVersionProperty);
        anchorSoftVersion.setLayoutY(250);

        AnchorPane anchorWagonNumber = createTextAnchorPane(bundle.getString("label.wagon"), wagonNumberProperty);
        anchorWagonNumber.setLayoutY(300);

        AnchorPane anchorDeployDate = createTextAnchorPane(bundle.getString("label.deploy.date"), deployDateProperty);
        anchorDeployDate.setLayoutY(350);

        AnchorPane anchorWorkHours = createTextAnchorPane(bundle.getString("label.work.hours"), totalWorkHoursProperty.asString());
        anchorWorkHours.setLayoutY(400);


        AnchorPane softPane = new AnchorPane();
        softPane.getStyleClass().addAll("bordered-pane", "soft-info");
        softPane.setPrefSize(205, 446);
        softPane.setLayoutX(590);
        softPane.setLayoutY(5);

        softPane.getChildren().addAll(
                imageView,
                anchorIOVersion,
                anchorConditionerVersion,
                anchorSoftVersion,
                anchorWagonNumber,
                anchorDeployDate,
                anchorWorkHours
        );
        return softPane;
    }

    private AnchorPane createVersionAnchorPane( String versionName, ObjectProperty<Version> versionObjectProperty ) {
        AnchorPane pane = new AnchorPane();

        Label name = new Label(versionName);
        name.getStyleClass().addAll("strong");
        name.setLayoutY(0);

        Label version = new Label();

        version.textProperty()
                .bind(Bindings.createStringBinding(() -> versionObjectProperty.getValue().getVersion(), versionObjectProperty));

        version.setLayoutY(20);

        Label date = new Label(versionObjectProperty.getValue().getDate().toString());
        date.textProperty()
                .bind(Bindings.createStringBinding(() -> versionObjectProperty.getValue().getDate().toString(), versionObjectProperty));

        date.setLayoutY(40);

        pane.getChildren().addAll(name, version, date);

        AnchorPane.setRightAnchor(name, 0.0);
        AnchorPane.setLeftAnchor(name, 0.0);
        AnchorPane.setRightAnchor(version, 0.0);
        AnchorPane.setLeftAnchor(version, 0.0);
        AnchorPane.setRightAnchor(date, 0.0);
        AnchorPane.setLeftAnchor(date, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);

        return pane;
    }

    private AnchorPane createTextAnchorPane( String name, ObservableValue<String> text ) {
        AnchorPane pane = new AnchorPane();

        Label nameLabel = new Label(name);
        nameLabel.getStyleClass().addAll("strong");
        nameLabel.setLayoutY(0);

        Label textLabel = new Label();
        textLabel.textProperty().bind(text);
        textLabel.setLayoutY(20);

        pane.getChildren().addAll(nameLabel, textLabel);

        AnchorPane.setRightAnchor(nameLabel, 0.0);
        AnchorPane.setLeftAnchor(nameLabel, 0.0);
        AnchorPane.setRightAnchor(textLabel, 0.0);
        AnchorPane.setLeftAnchor(textLabel, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);

        return pane;
    }

}
