package ua.com.ator.trainSE.ui.views;

/**
 * вспомогательный класс для прогрессбара
 * расчитает нужную для создания прогрессбара информацию
 *
 * @author Dmitry Sokolov on 28.07.2017.
 */
class ProgressBarInfo {
    private final double left;
    private final double right;

    private final boolean withZero;
    private final double lowerLimit;
    private final double upperLimit;

    /**
     * конструктор
     *
     * @param lowerLimit нижний предел
     * @param upperLimit верхний предел
     */
    ProgressBarInfo(double lowerLimit, double upperLimit) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        withZero = !((lowerLimit < 0 && upperLimit <= 0) || (lowerLimit >= 0 && upperLimit > 0));

        left = leftPercentCompute(lowerLimit, upperLimit);
        right = 100 - left;
    }

    private double leftPercentCompute(double lowerLimit, double upperLimit) {
        if (withZero) {
            return (lowerLimit * -1) / (((lowerLimit * -1) + upperLimit) / 100);
        } else {
            return 50;
        }
    }

    boolean isWithZero() {
        return withZero;
    }

    double getLeft() {
        return left;
    }

    double getRight() {
        return right;
    }

    double getLowerLimit() {
        return lowerLimit;
    }

    double getUpperLimit() {
        return upperLimit;
    }
}