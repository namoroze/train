package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.BooleanExpression;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import ua.com.ator.trainSE.ui.interfaces.Message;
import ua.com.ator.trainSE.ui.interfaces.Viewable;

import java.util.List;

/**
 * View-контроллер с несколькими сообщениями
 * Обвертка для SingleMessage
 * Вью синглтон
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
public class MultiMessage extends AbstractMessage {

    // Сообщения в линии
    private List<Message> messages;

    /**
     * конструктор
     *
     * @param messages список соощений
     */
    public MultiMessage(List<Message> messages) {
        super(messages.stream()
                .map(Message::getVisible)
                .reduce(BooleanExpression::or)
                .get());

        this.messages = messages;
    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {
        return (view != null) ? view : (view = new MultiMessageView());
    }

    /**
     * вью с несколькими сообщениями
     */
    private class MultiMessageView extends GridPane {
        /**
         * конструктор
         */
        private MultiMessageView() {
            this.visibleProperty().bind(getVisible());

            //Добавление сообщений в линию
            addRow(0,
                    messages.stream()
                            .peek(message -> {
                                ColumnConstraints c = new ColumnConstraints();
                                c.setPercentWidth(100.0 / messages.size());
                                this.getColumnConstraints().add(c);
                            })
                            .map(Viewable::getView)
                            .toArray(Node[]::new));

            this.setHgap(2);
            MultiMessage.this.setAnchors(this);
        }

    }
}
