package ua.com.ator.trainSE.ui.views;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.Node;
import javafx.scene.control.Label;

import java.util.ResourceBundle;


/**
 * View-контроллер реализация одноэлементного сообщения
 * Вью синглтон
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
public class SingleMessage extends AbstractMessage {

    private ObjectProperty<MessageStyles> styles;
    private String text;

    public SingleMessage(String text, ObservableBooleanValue visibleProperty, MessageStyles styles) {
        super(visibleProperty);
        this.text = text;
        this.styles = new SimpleObjectProperty<>(styles);
    }
    /**
     * вернет стиль сообщения
     *
     * @return стиль сообщения
     */
    ObjectProperty<MessageStyles> stylesProperty() {
        return styles;
    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {
        return (view != null) ? view : (view = new SingleMessageView());
    }

    /**
     * вью одноэлементного сообщения
     */
    private class SingleMessageView extends Label {
        /**
         * конструктор
         */
        private SingleMessageView() {
            this.setText(ResourceBundle.getBundle("locale").getString(text));
            this.getStyleClass().addAll(styles.getValue().getStyleClasses());
            this.visibleProperty().bind(SingleMessage.this.getVisible());
            this.setPrefSize(586, 37);
            setAnchors(this);
            styles.addListener((observable, oldValue, newValue) -> setStyle(oldValue, newValue));
        }

        private void setStyle(MessageStyles oldStyle, MessageStyles newStyle) {
            this.getStyleClass().removeAll(oldStyle.getStyleClasses());
            this.getStyleClass().addAll(newStyle.getStyleClasses());
        }
    }
}
