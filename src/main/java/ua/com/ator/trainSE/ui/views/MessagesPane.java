package ua.com.ator.trainSE.ui.views;

import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.ui.interfaces.Message;
import ua.com.ator.trainSE.ui.interfaces.Viewable;

import java.util.List;

/**
 * панель сообщений
 *
 * @author Dmitry Sokolov on 04.07.2017.
 */
public class MessagesPane implements Viewable {

    private List<Message> messageList;
    private MessagePaneView view;

    /**
     * конструктор
     *
     * @param messageList список сообщений
     */
    public MessagesPane(List<Message> messageList) {
        this.messageList = messageList;
    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {
        return (view != null) ? view : (view = new MessagePaneView());
    }

    /**
     * вью панели сообщений
     */
    private class MessagePaneView extends AnchorPane {
        private double width = 592;
        private double height = 312;
        private int spacing = 2;
        private final double[] yPosition = {3};
        private double rowHeight = (height - ((messageList.size() - 1) * spacing)) / messageList.size();

        /**
         * конструктор
         */
        private MessagePaneView() {

            this.setPrefWidth(width);
            this.setPrefHeight(height);
            this.getStyleClass().addAll("bordered-pane", "message-pane");

            messageList.forEach(message -> {
                AnchorPane anchorPane = new AnchorPane();
                anchorPane.getChildren().add(message.getView());
                anchorPane.setPrefSize(width, rowHeight);
                anchorPane.setLayoutY(yPosition[0]);
                yPosition[0] = yPosition[0] + rowHeight + spacing;
                this.getChildren().add(anchorPane);
                AnchorPane.setLeftAnchor(anchorPane, 2.0);
                AnchorPane.setRightAnchor(anchorPane, 2.0);
            });
        }
    }
}
