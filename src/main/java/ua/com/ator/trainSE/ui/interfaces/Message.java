package ua.com.ator.trainSE.ui.interfaces;

import javafx.beans.binding.BooleanBinding;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

/**
 * @author Dmitry Sokolov on 18.10.2017.
 */
public interface Message extends Viewable {
    /**
     * Sets the left and right anchors for the child when contained by an anchor pane.
     */
    default void setAnchors(Node child){
        AnchorPane.setLeftAnchor(child, 0.0);
        AnchorPane.setRightAnchor(child, 0.0);
    }

    BooleanBinding getVisible();
}
