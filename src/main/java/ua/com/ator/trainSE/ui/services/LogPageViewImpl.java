package ua.com.ator.trainSE.ui.services;

import ua.com.ator.trainSE.db.dao.ILogPageDao;
import ua.com.ator.trainSE.db.entity.LogRow;
import ua.com.ator.trainSE.ui.interfaces.LogPageView;

import java.util.List;

/**
 * @author Dmitry Sokolov on 28.08.2017.
 */
public class LogPageViewImpl implements LogPageView {

    private ILogPageDao dao;
    private String pageName;
    private String viewName;

    public LogPageViewImpl(ILogPageDao dao, String pageName, String viewName) {
        this.dao = dao;
        this.pageName = pageName;
        this.viewName = viewName;
    }

    /**
     * вернет количество страниц
     *
     * @param countPageRows количество записей на странице
     * @return количество страниц
     */
    @Override
    public int getNumberPages(int countPageRows) {
        return dao.getNumberPages(viewName, countPageRows);
    }

    /**
     * вернет нужное количество записей для указанной страницы
     *
     * @param countPageRows количество записей
     * @param pageNumber    номер страницы
     * @return список записей
     */
    @Override
    public List<LogRow> getRows(int countPageRows, int pageNumber) {
        return dao.findLogRows(viewName, countPageRows, pageNumber);
    }

    /**
     * вернет имя страницы
     *
     * @return имя страницы
     */
    @Override
    public String getPageName() {
        return pageName;
    }
}
