package ua.com.ator.trainSE.ui.services;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

import java.util.prefs.Preferences;

/**
 * Preferences потокобезопасны и не требуют внешней синхронизации
 * <p>
 * Created by Dmitry Sokolov on 27.07.2017.
 */
public class WorkHoursService {

    /**
     * Хранилище моточасов
     */
    private Preferences storage = Preferences.userNodeForPackage(WorkHoursService.class);

    private IntegerProperty totalWorkHoursProperty = new SimpleIntegerProperty();


    /**
     * Конструктор запустит поток-демон автоматического обновления тиков моточасов
     */
    public WorkHoursService() {
        ScheduledService<Void> dateTimeScheduledService = new ScheduledService<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        return null;
                    }
                };
            }
        };

        dateTimeScheduledService.setOnScheduled(event -> incrementTotalWorkHours());
        dateTimeScheduledService.setRestartOnFailure(true);
        dateTimeScheduledService.setPeriod(Duration.seconds(60));
        dateTimeScheduledService.start();
    }

    /**
     * Увеличит на 1 текущее значени моточасов
     */
    private void incrementTotalWorkHours() {
        int current = storage.getInt("totalWorkHours", 0);
        storage.putInt("totalWorkHours", ++current);
        totalWorkHoursProperty.setValue(current / 60);
    }

    public IntegerProperty getTotalWorkHoursProperty() {
        return totalWorkHoursProperty;
    }
}
