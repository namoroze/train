package ua.com.ator.trainSE.ui.pages;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import ua.com.ator.trainSE.ui.interfaces.IoDebugViewable;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static ua.com.ator.trainSE.keypad.KeypadButtons.F1;
import static ua.com.ator.trainSE.keypad.KeypadButtons.F2;

/**
 * страница отображения всех входов и выходов
 *
 * @author Dmitry Sokolov on 17.07.2017.
 */
public class StatusPage extends Page {
    private StatusPageView statusPageView = null;
    private ResourceBundle bundle = ResourceBundle.getBundle("locale");
    private List<Node> analogInputs = new ArrayList<>();
    private List<Node> discreteInputs = new ArrayList<>();
    private List<Node> outputs = new ArrayList<>();
    public final static int COLUMN_COUNT = 2;

    /**
     * Вернет вью страницы
     *
     * @return Node
     */
    @Override
    public Node getView() {
        if (isNull(statusPageView)) {
            statusPageView = new StatusPageView();
        }
        return statusPageView;
    }

    /**
     * добавит элемент в поле аналоговых входов
     *
     * @param debugViewable DebugViewElement
     */
    public void addAnalogInput(IoDebugViewable debugViewable) {
        analogInputs.add(debugViewable.getDebugView());
    }

    /**
     * Сетер для листа сенсоров(аналоговых)
     *
     * @param list
     */
    public void setAnalogInputs(List<IoDebugViewable> list) {
        analogInputs = list.stream()
                .map(IoDebugViewable::getDebugView)
                .collect(toList());
    }

    /**
     * добавит элемент в поле дискреиных входов
     *
     * @param debugViewable DebugViewElement
     */

    public void addDiscreteInput(IoDebugViewable debugViewable) {
        discreteInputs.add(debugViewable.getDebugView());
    }

    public void setDiscreteInputs(List<IoDebugViewable> discreteInputs) {
        this.discreteInputs = discreteInputs.stream()
                .map(IoDebugViewable::getDebugView)
                .collect(toList());
    }

    /**
     * добавит элемент в поле дискретных выходов
     *
     * @param debugViewable DebugViewElement
     */

    public void addOutputs(IoDebugViewable debugViewable) {
        outputs.add(debugViewable.getDebugView());
    }

    /**
     * Сетер для листа дискретных выходов
     *
     * @param outputs
     */
    public void setOutputs(List<IoDebugViewable> outputs) {
        this.outputs = outputs
                .stream()
                .map(IoDebugViewable::getDebugView)
                .collect(toList());
    }

    /**
     * Инициализация кнопок
     */
    @Override
    protected void initButtons() {
        buttonTray.setButtons(F1, defaultButtons.get(F1));
        buttonTray.setButtons(F2, defaultButtons.get(F2));
    }

    private class StatusPageView extends AnchorPane {

        private VBox analogInputsBox = new VBox();
        private VBox discreteInputsBox = new VBox();
        private VBox outputsBox = new VBox();
        private AnchorPane pane = new AnchorPane();

        public StatusPageView() {
            this.getStyleClass().add("status-page");
            pane.setPrefWidth(790);
            pane.setPrefHeight(446);
            pane.setLayoutX(5);
            pane.setLayoutY(5);
            pane.getStyleClass().addAll("bordered-pane");


            Label inputsLabel = new Label(bundle.getString("label.inputs"));
            inputsLabel.getStyleClass().addAll("io-value", "strong");
            inputsLabel.setLayoutX(10);
            inputsLabel.setLayoutY(10 - 7);

            analogInputsBox.getChildren().addAll(getGrig(analogInputs));
            analogInputsBox.setAlignment(Pos.CENTER);
            analogInputsBox.setLayoutX(0);
            analogInputsBox.setLayoutY(10 + 25 - 7);

            discreteInputsBox.getChildren().addAll(getGrig(discreteInputs));
            discreteInputsBox.setAlignment(Pos.CENTER);
            discreteInputsBox.setLayoutX(0);
            discreteInputsBox.setLayoutY(3 * 16 + 80 - 12);

            Label outputsLabel = new Label(bundle.getString("label.outputs"));
            outputsLabel.getStyleClass().addAll("io-value", "strong");
            outputsLabel.setLayoutX(10);
            outputsLabel.setLayoutY(335 + 20);

            outputsBox.getChildren().addAll(getGrig(outputs));
            outputsBox.setAlignment(Pos.CENTER);
            outputsBox.setLayoutX(0);
            outputsBox.setLayoutY(335 + 25 + 20);

            pane.getChildren().addAll(inputsLabel, outputsLabel, analogInputsBox, discreteInputsBox, outputsBox);
            this.getChildren().addAll(pane, buttonTray.getView());
            this.visibleProperty().setValue(false);

        }

        private Node getGrig(List<Node> nodes) {
            GridPane box = new GridPane();
            box.setPrefWidth(790);
            box.getStyleClass().addAll("status-page-horizontal");
            box.setGridLinesVisible(true);

            range(0, nodes.size() % COLUMN_COUNT == 0 ? nodes.size() : nodes.size() + (COLUMN_COUNT - nodes.size() % COLUMN_COUNT))
                    .boxed()
                    .collect(groupingBy(index -> index / COLUMN_COUNT))
                    .entrySet()
                    .stream()
                    .map(entry -> {
                        box.addRow(entry.getKey(), entry.getValue()
                                .stream()
                                .map(index -> {
                                    if (index < nodes.size()) {
                                        return nodes.get(index);
                                    } else {
                                        Label label = new Label();
                                        label.setMinWidth(790. / COLUMN_COUNT);
                                        return label;
                                    }
                                })
                                .toArray(Node[]::new));
                        return box;
                    })
                    .collect(toList());
            return box;
        }

    }

}
