package ua.com.ator.trainSE.ui.interfaces;

import ua.com.ator.trainSE.db.entity.LogRow;

import java.util.List;

/**
 * @author Dmitry Sokolov on 28.08.2017.
 */
public interface LogPageView {
    /**
     * вернет количество страниц
     *
     * @param countPageRows количество записей на странице
     * @return количество страниц
     */
    int getNumberPages(int countPageRows);

    /**
     * вернет нужное количество записей для указанной страницы
     *
     * @param countPageRows количество записей
     * @param pageNumber    номер страницы
     * @return список записей
     */
    List<LogRow> getRows(int countPageRows, int pageNumber);

    /**
     * вернет имя страницы
     *
     * @return имя страницы
     */
    String getPageName();
}
