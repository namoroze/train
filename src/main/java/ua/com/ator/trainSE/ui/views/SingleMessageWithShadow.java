package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;

/**
 * View-контроллер реализация одноэлементного сообщения,
 * которое не пропадает полностью в выключеном режиме, а остается тень.
 * Вью синглтон
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
public class SingleMessageWithShadow extends SingleMessage {

    // Цвет при включенном состоянии
    MessageStyles color;

    /**
     * @param text - текст сообщения
     * @param visibleProperty - показывает включено сообщение или выключено
     * @param styles - цвет при включенном состоянии
     */
    public SingleMessageWithShadow( String text, ObservableBooleanValue visibleProperty, MessageStyles styles ) {
        super(text, visibleProperty, styles);

        // Запоминаем первоначальный заданный цвет сообщения
        color = styles;
        //Бинд на изменения видимости для изменения стиля
        //  ( в выключенном состоянии просто тень)
        stylesProperty()
                .bind(
                        Bindings.when(sourceProperty())
                                .then(color)
                                .otherwise(MessageStyles.OFF));
    }
    @Override
    public BooleanBinding getVisible() {
        // Для этого типа сообщения состояние видимости постоянно
        // TRUE в связи с тем, что сообщение при выключении не пропадает,
        // а просто изменяется его цвет (полупрозрачная тень)
        return Bindings.when(new SimpleBooleanProperty(true)).then(true).otherwise(false);
    }
}
