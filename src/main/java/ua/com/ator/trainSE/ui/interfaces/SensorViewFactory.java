package ua.com.ator.trainSE.ui.interfaces;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import ua.com.ator.trainSE.ui.views.SensorView;

/**
 * Типы прогрессбара сенсора для отображения
 *
 * @author Baevskiy Ilya
 */
@FunctionalInterface
public interface SensorViewFactory {
    SensorView createProgressBar( BooleanProperty isError,BooleanProperty connectionError, DoubleProperty value, String name, String units, double lowerLimit, double upperLimit );
}
