package ua.com.ator.trainSE.ui.pages;

import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.ui.interfaces.IndicatorViewable;
import ua.com.ator.trainSE.ui.views.Button;
import ua.com.ator.trainSE.ui.views.Clock;
import ua.com.ator.trainSE.ui.views.MessagesPane;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static ua.com.ator.trainSE.keypad.KeypadButtons.*;

/**
 * главная страница
 * @author Dmitry Sokolov on 27.07.2017.
 */
public class MainPage extends Page {

    private DoubleProperty establishedTemperatureProperty;

    private double minEstablishedTemperature;
    private double maxEstablishedTemperature;

    private List<Node> indicatorsList = new ArrayList<>();

    private Clock clock;
    private MessagesPane messagesPanel;
    private MainView pageView = null;

    public MainPage(DoubleProperty establishedTemperatureProperty) {
        this.establishedTemperatureProperty = establishedTemperatureProperty;
        minEstablishedTemperature = establishedTemperatureProperty.getValue() - 2;
        maxEstablishedTemperature = establishedTemperatureProperty.getValue() + 2;
        clock = new Clock();
    }

    /**
     * Вернет вью страницы
     * @return Node
     */
    @Override
    public Node getView() {
        if (isNull(pageView)) {
            pageView = new MainView();
        }
        return pageView;
    }

    /**
     * установит индикаторы
     * @param index индекс индикатора
     * @param indicator индикатор
     */
    public void setIndicator(int index, IndicatorViewable indicator) {
        if (0 <= index && index < 6) {
            indicatorsList.add(index, indicator.getIndicatorView());
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
    public void setIndicatorsList(List<IndicatorViewable> indicators){
        indicatorsList = indicators
                .stream()
                .map(IndicatorViewable::getIndicatorView)
                .collect(toList());
    }

    /**
     * установит панель сообщений
     * @param messagesPanel панель сообщений
     */
    public void setMessagesPanel(MessagesPane messagesPanel) {
        this.messagesPanel = messagesPanel;
    }
    /**
     *  Инициализация кнопок
     */
    @Override
    protected void initButtons() {
        buttonTray.setButtons(F1, defaultButtons.get(F1));
        buttonTray.setButtons(F2, defaultButtons.get(F2));
        buttonTray.setButtons(F3, new Button("-", () -> {
            if (establishedTemperatureProperty.get() > minEstablishedTemperature) {
                establishedTemperatureProperty.setValue(establishedTemperatureProperty.get() - 1);
            }
        }));
        buttonTray.setButtons(F4, new Button("+", () -> {
            if (establishedTemperatureProperty.get() < maxEstablishedTemperature) {
                establishedTemperatureProperty.setValue(establishedTemperatureProperty.get() + 1);
            }
        }));
    }

    private class MainView extends AnchorPane {
        private MainView() {
            indicatorsList.get(0).setLayoutX(5);
            indicatorsList.get(1).setLayoutX(203);
            indicatorsList.get(2).setLayoutX(401);
            indicatorsList.get(3).setLayoutX(599);
            indicatorsList.get(4).setLayoutX(599);
            indicatorsList.get(5).setLayoutX(599);
            clock.getView().setLayoutX(599);
            messagesPanel.getView().setLayoutX(5);

            indicatorsList.get(0).setLayoutY(5);
            indicatorsList.get(1).setLayoutY(5);
            indicatorsList.get(2).setLayoutY(5);
            indicatorsList.get(3).setLayoutY(5);
            indicatorsList.get(4).setLayoutY(133);
            indicatorsList.get(5).setLayoutY(261);
            clock.getView().setLayoutY(389);
            messagesPanel.getView().setLayoutY(133);

            this.getChildren().addAll(
                    indicatorsList.get(0),
                    indicatorsList.get(1),
                    indicatorsList.get(2),
                    indicatorsList.get(3),
                    indicatorsList.get(4),
                    indicatorsList.get(5),
                    clock.getView(),
                    messagesPanel.getView(),
                    buttonTray.getView());
        }
    }
}
