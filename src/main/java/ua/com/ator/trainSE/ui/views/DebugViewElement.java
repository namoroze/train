package ua.com.ator.trainSE.ui.views;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import static ua.com.ator.trainSE.ui.pages.StatusPage.COLUMN_COUNT;

/**
 * елемент для страницы дэбага
 *
 * @author Dmitry Sokolov on 17.07.2017.
 */
public class DebugViewElement extends AnchorPane {

    private Label valueLabel = new Label();

    public DebugViewElement(String name) {
        Label nameLabel = new Label();
        nameLabel.setText(name);

        this.setPrefWidth(790. / COLUMN_COUNT);
        this.getChildren().addAll(nameLabel, valueLabel);
        this.getStyleClass().addAll("debug-element");

        AnchorPane.setLeftAnchor(nameLabel, 3.);
        AnchorPane.setRightAnchor(valueLabel, 3.);
    }

    StringProperty getValueProperty() {
        return this.valueLabel.textProperty();
    }
}



