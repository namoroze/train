package ua.com.ator.trainSE.ui.pages;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import ua.com.ator.trainSE.db.entity.LogRow;
import ua.com.ator.trainSE.ui.interfaces.LogPageView;
import ua.com.ator.trainSE.ui.views.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Objects.isNull;
import static ua.com.ator.trainSE.keypad.KeypadButtons.*;

/**
 * Страница отображения изменений состояни оборудования
 *
 * @author Dmitry Sokolov on 31.07.2017.
 */
public class LogPage extends Page {
    private static final int countPageRows = 20;
    private int cursor = 0;
    private int pageNumber = 1;

    private ResourceBundle bundle = ResourceBundle.getBundle("locale");
    private List<LogPageView> logPageViews;

    private LogView logView = null;

    /**
     * конструктор
     */
    public LogPage(List<LogPageView> logPageViews) {
        this.logPageViews = logPageViews;
    }

    /**
     * Вернет вью страницы
     *
     * @return Node
     */
    @Override
    public Node getView() {
        if (isNull(logView)) {
            logView = new LogView();
            setCurrentView();
        }
        return logView;
    }

    /**
     * Инициализация кнопок
     */
    protected void initButtons() {
        buttonTray.setButtons(F1, new Button(defaultButtons.get(F1).getName(), () -> {
            defaultButtons.get(F1).runAction();
            cursor = 0;
            pageNumber = 1;
            setCurrentView();
        }));
        buttonTray.setButtons(F2, new Button(defaultButtons.get(F2).getName(), () -> {
            if (cursor == (logPageViews.size() - 1)) {
                defaultButtons.get(F2).runAction();
                cursor = 0;
                pageNumber = 1;
            } else {
                cursor++;
                pageNumber = 1;
            }
            setCurrentView();
        }));
        buttonTray.setButtons(F3, new Button(bundle.getString("label.message.scroll.backward"), () -> {
            if (pageNumber > 1) {
                pageNumber--;
                setCurrentView();
            }
        }));
        buttonTray.setButtons(F4, new Button(bundle.getString("label.message.scroll.forward"), () -> {
            if (logPageViews.size() > 0 && pageNumber < logPageViews.get(cursor).getNumberPages(countPageRows)) {
                pageNumber++;
                setCurrentView();
            }
        }));
    }

    /**
     * запросит у базы нужную инфу для страницы
     * и отдаст ее во вью
     */
    private void setCurrentView() {
        if (logPageViews.size() > 0) {
            this.logView.setCurrent(
                    logPageViews.get(cursor).getRows(countPageRows, pageNumber),
                    logPageViews.get(cursor).getPageName(),
                    pageNumber,
                    logPageViews.get(cursor).getNumberPages(countPageRows)
            );
        }
    }

    private class LogView extends AnchorPane {

        private Label pageNameLabel = new Label();
        private Label pageNumberLabel = new Label();

        private List<Label> labels;

        LogView() {
            AnchorPane pane = new AnchorPane();
            pane.setPrefHeight(446);
            pane.setPrefWidth(790);
            pane.setLayoutX(5);
            pane.setLayoutY(5);
            pane.getStyleClass().addAll("log-page", "bordered-pane");

            labels = IntStream.range(0, countPageRows).mapToObj(value -> {
                Label label = new Label();
                label.getStyleClass().add("log");
                return label;
            }).collect(Collectors.toList());

            pageNameLabel.setLayoutX(50);
            pageNameLabel.setLayoutY(5);
            pageNameLabel.getStyleClass().add("info");

            pageNumberLabel.setLayoutX(500);
            pageNumberLabel.setLayoutY(5);
            pageNumberLabel.getStyleClass().add("info");

            VBox logs = new VBox();
            logs.setLayoutX(10);
            logs.setLayoutY(30);
            logs.getChildren().addAll(labels);

            pane.getChildren().addAll(pageNameLabel, pageNumberLabel, logs);

            getChildren().addAll(pane, buttonTray.getView());
            this.visibleProperty().setValue(false);
        }

        /**
         * Задает параметры текущей странице
         * @param logRows
         * @param pageName
         * @param pageNumber
         * @param numberOfPages
         */
        void setCurrent(List<LogRow> logRows, String pageName, int pageNumber, int numberOfPages) {
            pageNameLabel.setText(pageName);
            pageNumberLabel.setText(bundle.getString("label.page") + String.format("%5d/%d", pageNumber, numberOfPages));
            IntStream.range(0, countPageRows).forEach(index -> labels.get(index).setText((logRows.size() > index) ? logRows.get(index).toString() : ""));
        }
    }
}
