package ua.com.ator.trainSE.ui.services;

import ua.com.ator.trainSE.db.dao.ILogPageDao;
import ua.com.ator.trainSE.db.dao.LogPageDao;
import ua.com.ator.trainSE.db.entity.LogPageViewMetaInfo;
import ua.com.ator.trainSE.ui.interfaces.LogPageView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Sokolov on 28.08.2017.
 */
public class LogPageService {

    private ILogPageDao dao;
    private List<LogPageViewMetaInfo> availableViews;

    public LogPageService() {
        this.dao = new LogPageDao();
        createViews();
    }

    /**
     * создаст представления в бд
     */
    private void createViews() {
        availableViews = dao.getAvailableViews();
        dao.createViews(availableViews);
    }

    /**
     * вернет список вьюх
     *
     * @return список вьюх
     */
    public List<LogPageView> getViews() {
        List<LogPageView> views = new ArrayList<>();
        availableViews.forEach(metaInfo -> views.add(new LogPageViewImpl(dao, metaInfo.getPageName(), metaInfo.getViewName())));
        return views;
    }
}
