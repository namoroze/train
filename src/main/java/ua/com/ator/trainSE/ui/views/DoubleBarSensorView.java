package ua.com.ator.trainSE.ui.views;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;

/**
 * реализация SensorView с двойным прогрессбаром
 *
 * @author Dmitry Sokolov on 28.07.2017.
 */
public class DoubleBarSensorView extends SensorView {
    private Node indicatorView;

    /**
     * конструктор
     *
     * @param isError    состояние ошибки сенсора
     * @param value      значение
     * @param title      имя сенсора
     * @param units      единицы измерения
     * @param lowerLimit нижний предел
     * @param upperLimit верхний предел
     */
    public DoubleBarSensorView(BooleanProperty isError,BooleanProperty connectionError, DoubleProperty value, String title, String units, double lowerLimit, double upperLimit) {
        super(isError, connectionError, value, title, units, lowerLimit, upperLimit);
    }

    /**
     * вернет вью индикатора
     * @return вью
     */
    @Override
    public Node getIndicatorView() {
        if (indicatorView == null) {
            indicatorView = new Indicator(connectionError, title, units, isError, value, new DoubleProgressBar(connectionError,value, lowerLimit, upperLimit), isOutOfLimit).getView();
        }
        return indicatorView;
    }
}
