package ua.com.ator.trainSE.ui.pages;

import ua.com.ator.trainSE.keypad.KeypadButtons;
import ua.com.ator.trainSE.ui.interfaces.ActiveButton;
import ua.com.ator.trainSE.ui.interfaces.Controllable;
import ua.com.ator.trainSE.ui.interfaces.Viewable;
import ua.com.ator.trainSE.ui.views.ButtonTray;

import java.util.Map;

/**
 * @author Dmitry Sokolov on 27.07.2017.
 */
public abstract class Page implements Viewable, Controllable {
    ButtonTray buttonTray;
    Map<KeypadButtons, ActiveButton> defaultButtons;

    Page() {
        buttonTray = new ButtonTray();
    }

    /**
     * Установит значения кнопок по умолчанию
     *
     * @param defaultButtons кнопки
     */
    public void setDefaultButtons(Map<KeypadButtons, ActiveButton> defaultButtons) {
        this.defaultButtons = defaultButtons;
        initButtons();
    }

    /**
     * Инициализация кнопок
     */
    protected abstract void initButtons();

    /**
     * дергнет колбэк кнопки по индексу
     *
     * @param key кнопка
     */
    @Override
    public void handle(KeypadButtons key) {
        buttonTray.runAction(key);
    }
}
