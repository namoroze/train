package ua.com.ator.trainSE.ui.views;

import javafx.scene.Node;
import javafx.scene.control.Label;
import ua.com.ator.trainSE.ui.interfaces.ActiveButton;

/**
 * кнопка со своим обработчиком
 * @author Dmitry Sokolov on 27.07.2017.
 */
public class Button implements ActiveButton {
    private String name;
    private Runnable action;

    public Button(String name, Runnable action) {
        this.name = name;
        this.action = action;
    }
    /**
     * дергнет кодбэк кнопки
     */
    @Override
    public void runAction() {
        action.run();
    }
    /**
     * @return строковое представление кнопки
     */
    @Override
    public String getName() {
        return name;
    }
    /**
     * вернет вью
     * @return вью
     */
    @Override
    public Node getView() {
        return new Label(name);
    }
}
