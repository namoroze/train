package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.Node;
import ua.com.ator.trainSE.ui.interfaces.Message;
import ua.com.ator.trainSE.ui.property_adapters.SimpleBooleanPropertyAdapter;

/**
 * Абстрактный класс от которого
 * наследуются все реализации месседжей
 * Использован для инкапсуляции состояния видимости  isVisible
 *
 * @author Baevskiy Ilya
 * @author Dmitry Shokolov
 */
abstract class AbstractMessage implements Message {
    // Состояние видимости сообщения
    private BooleanBinding isVisible;
    // Прервоначальная небезопасная(для FX UI) проперти обвернутая в адаптер
    private ObservableBooleanValue sourceProperty;
    // Инстанс вью
    Node view;

    AbstractMessage( ObservableBooleanValue visibleProperty ) {
        sourceProperty = new SimpleBooleanPropertyAdapter(visibleProperty);
        this.isVisible =
                Bindings
                        .when(sourceProperty)
                        .then(true).otherwise(false);
    }

    AbstractMessage( BooleanBinding bb ) {
        isVisible = bb;
    }

    public BooleanBinding getVisible() {
        return isVisible;
    }
    public BooleanBinding setVisible(ObservableBooleanValue visibility) {
        return this.isVisible =
                Bindings
                        .when(sourceProperty)
                        .then(true).otherwise(false).and(visibility);
    }

    ObservableBooleanValue sourceProperty() {
        return sourceProperty;
    }
}
