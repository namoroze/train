package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import ua.com.ator.trainSE.ui.interfaces.IProgressBar;

import static java.util.Objects.isNull;


/**
 * одинарный прогрессбар
 *
 * @author Dmitry Sokolov on 07.07.2017.
 */
class SingleProgressBar implements IProgressBar {
    private BooleanProperty connectionError;
    private DoubleProperty value;
    private ProgressBarInfo progressBarInfo;
    private double normalizer = 0;
    private double divider = 1;
    private Node progressBarNode = null;

    /**
     * конструктор
     *
     * @param value      значение
     * @param lowerLimit нижний предел
     * @param upperLimit верхний предел
     */
    SingleProgressBar(BooleanProperty connectionError, DoubleProperty value, final double lowerLimit, final double upperLimit) {
        this.connectionError = connectionError;
        this.value = value;
        progressBarInfo = new ProgressBarInfo(lowerLimit, upperLimit);
        normalizer = 0 - lowerLimit;
        divider = normalizer + upperLimit;

    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {

        if (isNull(progressBarNode)) {
            ProgressBar progressBar = new ProgressBar();
            progressBar.setPrefWidth(196);
            progressBar.progressProperty().bind(Bindings.createDoubleBinding(() -> compute(value.get()), value));
            progressBarNode = new ProgressBarFramework(progressBarInfo, progressBar);
            progressBarNode.visibleProperty().bind(connectionError.not());
        }

        return progressBarNode;

    }

    private double compute(double value) {
        double val = (value + normalizer) / divider;
        return ((val <= 0) ? 0.0 : ((val >= 1) ? 1.0 : val));
    }
}
