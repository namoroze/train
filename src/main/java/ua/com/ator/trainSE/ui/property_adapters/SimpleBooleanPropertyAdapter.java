package ua.com.ator.trainSE.ui.property_adapters;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;

/**
 * обвертка проперти для запуска в потоке fx
 *
 * @author Dmitry Sokolov on 13.07.2017.
 */
public class SimpleBooleanPropertyAdapter extends SimpleBooleanProperty {
    /**
     * конструктор
     *
     * @param property проперти
     */
    public SimpleBooleanPropertyAdapter(ObservableBooleanValue property) {
        this.set(property.getValue());
        property.addListener((observable, oldValue, newValue) -> Platform.runLater(() -> this.set(newValue)));
    }

}
