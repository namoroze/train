package ua.com.ator.trainSE.ui.interfaces;

import javafx.scene.Node;

/**
 * @author Dmitry Sokolov on 27.07.2017.
 */
public interface Viewable {
    /**
     * вернет вью
     * @return вью
     */
    Node getView();
}
