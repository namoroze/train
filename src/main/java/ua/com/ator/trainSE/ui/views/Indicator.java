package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import ua.com.ator.trainSE.ui.interfaces.IProgressBar;
import ua.com.ator.trainSE.ui.interfaces.Viewable;

/**
 * индикатор
 *
 * @author Dmitry Sokolov on 28.07.2017.
 */
class Indicator implements Viewable {
    private BooleanProperty connectionError;
    private String name;
    private String units;
    private BooleanProperty error;
    private DoubleProperty value;
    private IProgressBar progressBar;
    private BooleanBinding isOutOfLimit;

    private Node indicatorView = null;

    /**
     * конструктор
     *
     * @param name         имя сенсора
     * @param units        единицы измерения
     * @param error        состояние ошибки сенсора
     * @param value        значение
     * @param progressBar  прогрессбар
     * @param isOutOfLimit выход за предел
     */
    Indicator(BooleanProperty connectionError,
            String name,
              String units,
              BooleanProperty error,
              DoubleProperty value, IProgressBar progressBar, BooleanBinding isOutOfLimit) {
        this.connectionError = connectionError;
        this.name = name;
        this.units = units;
        this.error = error;
        this.value = value;
        this.progressBar = progressBar;
        this.isOutOfLimit = isOutOfLimit;
    }
    /**
     * вернет вью
     * @return вью
     */
    @Override
    public Node getView() {
        if(indicatorView == null){
            indicatorView = new IndicatorFrameworkView(connectionError,name, units, error, value, progressBar, isOutOfLimit);
        }
        return indicatorView;

    }
}




