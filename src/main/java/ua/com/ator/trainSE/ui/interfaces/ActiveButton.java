package ua.com.ator.trainSE.ui.interfaces;

/**
 * кнопка с колбэком
 *
 * @author Dmitry Sokolov on 27.07.2017.
 */
public interface ActiveButton extends Viewable{
    /**
     * дергнет кодбэк кнопки
     */
    void runAction();

    /**
     * @return строковое представление кнопки
     */
    String getName();
}
