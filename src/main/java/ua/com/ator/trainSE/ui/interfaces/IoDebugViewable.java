package ua.com.ator.trainSE.ui.interfaces;

import javafx.scene.Node;

/**
 * @author Dmitry Sokolov on 13.07.2017.
 */
public interface IoDebugViewable {
    /**
     * вернет вью для страницы дэбага
     * @return вью
     */
    Node getDebugView();
}
