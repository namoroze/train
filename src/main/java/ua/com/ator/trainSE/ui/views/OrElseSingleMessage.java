package ua.com.ator.trainSE.ui.views;


import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.ui.interfaces.Message;

/**
 * @author Dmitry Shokolov on 02.11.2018.
 */
public class OrElseSingleMessage extends AbstractMessage {
    // Приоритетное сообщение
    private Message masterMessage;
    // Второстепенное сообщение
    private Message slaveMessage;
    OrElseSingleMessage(Message masterMessage, Message slaveMessage) {
        super(new SimpleBooleanProperty(true));
        this.masterMessage = masterMessage;
        this.slaveMessage = slaveMessage;
    }

    /**
     * вернет вью
     *
     * @return вью
     */
    @Override
    public Node getView() {
        return (view != null) ? view : (view = new OrElseSingleMessage.OrElseSingleMessageView());
    }

    /**
     * вью с приоритетными сообщениями
     */
    private class OrElseSingleMessageView extends AnchorPane {
        AnchorPane masterContainer = new AnchorPane(masterMessage.getView());
        AnchorPane slaveContainer = new AnchorPane(slaveMessage.getView());
        /**
         * конструктор
         */
        private OrElseSingleMessageView() {
            getChildren().addAll(masterContainer, slaveContainer);
            setAnchors(masterContainer);
            setAnchors(slaveContainer);
            setAnchors(this);
            masterContainer.visibleProperty().bind(masterMessage.getVisible());
            slaveContainer.visibleProperty().bind(Bindings.and(slaveMessage.getVisible(), masterMessage.getVisible().not()));
        }
    }
}
