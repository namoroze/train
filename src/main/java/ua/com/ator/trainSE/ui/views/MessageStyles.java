package ua.com.ator.trainSE.ui.views;

/**
 * стили сообщений
 *
 * @author Dmitry Sokolov on 18.10.2017.
 */
public enum MessageStyles {
    SUCCESS("success"),
    DANGER("danger"),
    DANGER_PLUS("danger-plus"),
    NORMAL("normal"),
    OFF("off");

    private String[] styleClasses;

    MessageStyles(String... styleClasses) {
        this.styleClasses = styleClasses;
    }

    public String[] getStyleClasses() {
        return styleClasses;
    }
}
