package ua.com.ator.trainSE.ui.pages;

import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ua.com.ator.trainSE.keypad.KeypadButtons;
import ua.com.ator.trainSE.ui.interfaces.ActiveButton;
import ua.com.ator.trainSE.ui.interfaces.Controllable;
import ua.com.ator.trainSE.ui.interfaces.Viewable;
import ua.com.ator.trainSE.ui.views.Button;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static ua.com.ator.trainSE.keypad.KeypadButtons.F1;
import static ua.com.ator.trainSE.keypad.KeypadButtons.F2;

/**
 * @author Dmitry Sokolov on 27.07.2017.
 */
public class PageSwitcher implements Viewable, Controllable {
    private List<Page> pages;
    private int cursor = 0;
    private Page currentPage;
    private AnchorPane mainLayout;

    /**
     * конструктор
     *
     * @param pages список страниц
     */
    public PageSwitcher(List<Page> pages) {
        this.pages = pages;
        this.mainLayout = new AnchorPane();
        ResourceBundle bundle = ResourceBundle.getBundle("locale");
        Map<KeypadButtons, ActiveButton> defaultButtons = new LinkedHashMap<>();

        this.mainLayout.setPrefSize(800, 480);

        defaultButtons.put(F1, new Button(bundle.getString("label.menu.window.to.home"), this::mainPage));
        defaultButtons.put(F2, new Button(bundle.getString("label.menu.window.next"), this::nextPage));

        this.pages.forEach(page -> page.setDefaultButtons(defaultButtons));
        this.mainLayout.getChildren().addAll(pages.stream().map(Page::getView).collect(Collectors.toList()));
        currentPage = pages.get(0);
//        this.mainLayout.getChildren().add(pages.get(0).getView());
    }

    @Override
    public void handle(KeypadButtons key) {
        pages.get(cursor).handle(key);
    }

    /**
     * Вернет вью в которой будут отображаться страницы
     *
     * @return Node
     */
    @Override
    public Node getView() {
        return mainLayout;
    }

    /**
     * переключит на следующую страницу
     */
    private void nextPage() {
        changePage(pages.get(cursor = ((cursor == pages.size() - 1) ? 0 : (cursor + 1))));
    }

    /**
     * переключит на первую страницу
     */
    private void mainPage() {
        changePage(pages.get(cursor = 0));
    }

    /**
     * переключит на предыдущую страницу
     */
    private void previousPage() {
        changePage(pages.get(cursor = ((cursor == 0) ? (pages.size() - 1) : (cursor - 1))));
    }

    /**
     * покажет текущую страницу
     */
    private void changePage(Page page) {
        currentPage.getView().setVisible(false);
        currentPage = pages.get(cursor);
        currentPage.getView().setVisible(true);

//        this.mainLayout.getChildren().replaceAll(node -> page.getView());
//        this.mainLayout.getChildren().replaceAll(node -> page.getView());
    }
}
