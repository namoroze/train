package ua.com.ator.trainSE.ui.views;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.Node;
import ua.com.ator.trainSE.ui.interfaces.IoDebugViewable;
import ua.com.ator.trainSE.ui.property_adapters.SimpleBooleanPropertyAdapter;

import java.util.ResourceBundle;

/**
 * Created by ActivX on 12.10.2017.
 */
public class DebugViewBooleanProperty implements IoDebugViewable {
    DebugViewElement element;
    private ObservableBooleanValue property;

    //TODO:// Внедрить стрингу локали
    private static final ResourceBundle bundle = ResourceBundle.getBundle("locale");


    /**
     * Конструктор который привязывает входящую ObservableBooleanValue,
     * к внутреннему Label, для булевых значений
     *
     * @param name             - заголовок записи
     * @param valueForSafe     - пропертя
     * @param trueStringValue  - Строка при проперти = true
     * @param falseStringValue - Строка при проперти = false
     */
    public DebugViewBooleanProperty(ObservableBooleanValue connectionError, String name, ObservableBooleanValue valueForSafe, String trueStringValue, String falseStringValue) {
        element = new DebugViewElement(bundle.getString(name));
        this.property = new SimpleBooleanPropertyAdapter(valueForSafe);
        //Привязываем к изменениям входящей проперти
        element.getValueProperty()
                .bind(Bindings.when(property)
                        .then(bundle.getString(trueStringValue))
                        .otherwise(new When(connectionError)
                                .then("--")
                                .otherwise(bundle.getString(falseStringValue)))
                );
    }

    /**
     * Конструктор который привязывает входящую ObservableBooleanValue,
     * к внутреннему Label, для булевых значений,
     * в этом конструкторе строковое представление
     * булевого значения используется по-умолчанию с учетом локали (on/off)
     *
     * @param name
     * @param valueForSafe
     */
    public DebugViewBooleanProperty(ObservableBooleanValue connectionError, String name, ObservableBooleanValue valueForSafe) {
        this(connectionError, name, valueForSafe, "label.on", "label.off");
    }

    /**
     * вернет вью для страницы дэбага
     *
     * @return вью
     */
    @Override
    public Node getDebugView() {
        return element;
    }
}
