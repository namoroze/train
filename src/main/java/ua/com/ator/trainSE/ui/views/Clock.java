package ua.com.ator.trainSE.ui.views;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import ua.com.ator.trainSE.ui.interfaces.Viewable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author Dmitry Sokolov on 27.07.2017.
 */
public class Clock implements Viewable {
    private ClockView clock;
    private DateTimeFormatter dateFormatter;
    private DateTimeFormatter timeFormatter;

    public Clock() {
        ResourceBundle bundle = ResourceBundle.getBundle("locale");
        dateFormatter = DateTimeFormatter.ofPattern(bundle.getString("format.date.label"));
        timeFormatter = DateTimeFormatter.ofPattern(bundle.getString("format.time.label"));
    }

    /**
     * вернет графическое представление часов
     *
     * @return часы
     */
    @Override
    public Node getView() {
        if (isNull(clock)) {
            clock = new ClockView();
            scheduleClock();
        }
        return clock;
    }

    private void scheduleClock() {
        ScheduledService<LocalDateTime> dateTimeScheduledService = new ScheduledService<LocalDateTime>() {
            @Override
            protected Task<LocalDateTime> createTask() {
                return new Task<LocalDateTime>() {
                    @Override
                    protected LocalDateTime call() throws Exception {
                        return LocalDateTime.now();
                    }
                };
            }
        };

        dateTimeScheduledService.setOnScheduled(event -> {
            LocalDateTime currentDate = dateTimeScheduledService.getLastValue();
            if (nonNull(currentDate))
                clock.setDate(currentDate.toLocalDate().format(dateFormatter));
            if (nonNull(currentDate))
                clock.setTime(currentDate.toLocalTime().format(timeFormatter));
        });

        dateTimeScheduledService.setRestartOnFailure(true);
        dateTimeScheduledService.setPeriod(Duration.seconds(15));
        dateTimeScheduledService.start();
    }

    private class ClockView extends AnchorPane {
        private Label dateLabel = new Label();
        private Label timeLabel = new Label();

        private ClockView() {


            this.setPrefSize(196, 62);

            dateLabel.getStyleClass().add("date-label");
            dateLabel.setLayoutY(5);

            timeLabel.getStyleClass().add("time-label");
            timeLabel.setLayoutY(20);

            AnchorPane.setLeftAnchor(dateLabel, 5.0);
            AnchorPane.setRightAnchor(dateLabel, 5.0);

            AnchorPane.setLeftAnchor(timeLabel, 5.0);
            AnchorPane.setRightAnchor(timeLabel, 5.0);

            this.getChildren().addAll(dateLabel, timeLabel);
            this.getStyleClass().add("bordered-pane");
        }

        private void setDate(String date) {
            dateLabel.setText(date);
        }

        private void setTime(String time) {
            timeLabel.setText(time);
        }
    }
}
