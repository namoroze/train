package ua.com.ator.trainSE.ui.views;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * обвертка для прогрессбара
 *
 * @author Dmitry Sokolov on 10.07.2017.
 */
class ProgressBarFramework extends VBox {

    ProgressBarFramework(ProgressBarInfo progressBarInfo, Node progressBar) {
        this.getChildren().add(0, progressBar);
        this.getChildren().add(1, getProgressBarLimitLabels(progressBarInfo));
    }

    private GridPane getProgressBarLimitLabels(ProgressBarInfo progressBarInfo) {
        GridPane gridPane = new GridPane();

        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setPercentWidth(progressBarInfo.getLeft() - 2.5);

        ColumnConstraints columnConstraints2 = new ColumnConstraints();
        columnConstraints2.setPercentWidth(5);

        ColumnConstraints columnConstraints3 = new ColumnConstraints();
        columnConstraints3.setPercentWidth(progressBarInfo.getRight() - 2.5);

        gridPane.getColumnConstraints().addAll(columnConstraints1, columnConstraints2, columnConstraints3);

        Label minValueProgressBarLabel = new Label();
        minValueProgressBarLabel.setAlignment(Pos.CENTER_LEFT);
        minValueProgressBarLabel.setPrefSize(196,10);
        minValueProgressBarLabel.setTextAlignment(TextAlignment.LEFT);
        minValueProgressBarLabel.setText(String.valueOf((int) progressBarInfo.getLowerLimit()));
        minValueProgressBarLabel.setFont(Font.font("System Bold", 14));

        gridPane.add(minValueProgressBarLabel, 0, 1);

        Label zeroValueProgressBarLabel = new Label();
        zeroValueProgressBarLabel.setAlignment(Pos.CENTER);
        zeroValueProgressBarLabel.setPrefSize(10,10);
        zeroValueProgressBarLabel.setTextAlignment(TextAlignment.CENTER);
        zeroValueProgressBarLabel.setText(progressBarInfo.isWithZero() ? String.valueOf(0) : "");
        zeroValueProgressBarLabel.setFont(Font.font("System Bold", 14));

        gridPane.add(zeroValueProgressBarLabel, 1, 1);

        Label maxValueProgressBarLabel = new Label();
        maxValueProgressBarLabel.setAlignment(Pos.CENTER_RIGHT);
        maxValueProgressBarLabel.setPrefSize(196,10);
        maxValueProgressBarLabel.setTextAlignment(TextAlignment.RIGHT);
        maxValueProgressBarLabel.setText(String.valueOf((int) progressBarInfo.getUpperLimit()));
        maxValueProgressBarLabel.setFont(Font.font("System Bold", 14));

        gridPane.add(maxValueProgressBarLabel, 2, 1);

        return gridPane;
    }
}