package ua.com.ator.trainSE.ui.views;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import ua.com.ator.trainSE.keypad.KeypadButtons;
import ua.com.ator.trainSE.ui.interfaces.ActiveButton;
import ua.com.ator.trainSE.ui.interfaces.Viewable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Панель кнопок
 * @author Dmitry Sokolov on 17.07.2017.
 */
public class ButtonTray implements Viewable {

    private Map<KeypadButtons, ActiveButton> buttons = new LinkedHashMap<>();
    private HBox buttonContainer = null;

    /**
     * установит кнопки по индексу
     *
     * @param key индекс
     * @param button кнопка
     */
    public void setButtons(KeypadButtons key, ActiveButton button) {
            buttons.put(key, button);
    }

    /**
     * Дергнет обработчик кнопки
     *
     */
    public void runAction(KeypadButtons key) {
        Optional.ofNullable(buttons.get(key)).ifPresent(ActiveButton::runAction);
    }
    /**
     * Вернет вью кнопок
     * @return Node
     */
    @Override
    public Node getView() {
        if (buttonContainer == null) {
            this.buttonContainer = new HBox();
            this.buttonContainer.getChildren().addAll(buttons.values().stream().map(ActiveButton::getView).toArray(Node[]::new));

            this.buttonContainer.getStyleClass().addAll("button-container");
            this.buttonContainer.setLayoutX(5);
            this.buttonContainer.setLayoutY(450);
        }

        return buttonContainer;
    }
}
