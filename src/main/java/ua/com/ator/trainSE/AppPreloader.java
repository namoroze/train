package ua.com.ator.trainSE;

import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;

public class AppPreloader extends Preloader {


    ProgressIndicator progressIndicator;
    Stage stage;
    ImageView imageView;
    private Scene scene;
    private Scene createPreloaderScene() throws IOException {
        imageView = new ImageView("file:///" + System.getProperty("TRAIN_PATH") + "/start.png");
        imageView.setLayoutX(300);
        imageView.setLayoutY(150);
        progressIndicator = new ProgressIndicator();
        progressIndicator.setLayoutY(250);
        AnchorPane p = new AnchorPane();
        progressIndicator.setLayoutX(800/2-15);
        p.setPrefSize(800, 480);

        p.getChildren().addAll(imageView, progressIndicator);
        p.setStyle("-fx-background-color: black");

        return new Scene(p, 800, 480);
    }

    public void start( Stage stage ) throws Exception {
        this.stage = stage;
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }
    @Override
    public void init() throws Exception {
        scene = createPreloaderScene();
    }
    @Override
    public void handleProgressNotification( ProgressNotification pn ) {
//        progressIndicator.setProgress(pn.getProgress());
    }

    @Override
    public void handleStateChangeNotification( StateChangeNotification evt ) {
        if ( evt.getType() == StateChangeNotification.Type.BEFORE_START ) {
            stage.hide();
        }
    }


}
