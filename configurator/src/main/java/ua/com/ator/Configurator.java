package ua.com.ator;

import com.mchange.v2.log.MLevel;
import org.apache.log4j.Logger;
import ua.com.ator.trainSE.services.impl.CommandService;

import java.io.*;
import java.util.Optional;
import java.util.Scanner;

import static java.lang.System.exit;

/**
 * @author Dmitry Sokolov on 13.04.2017.
 */
public class Configurator {

    private static final Logger logger = Logger.getLogger("SETUP_APP");

    private static final String DEFAULT_PATH = Optional.ofNullable(System.getenv("TRAIN_PATH")).orElse("/etc/train");

    private static final String FILE_INPUT_OUTPUT = "/dev/tty1";
    private static String directory = DEFAULT_PATH;
    private static int numLines = 0;
    private static PrintStream out;
    private static InputStream input;

    /**
     * Главный метод программы
     *
     * @param args аргументы
     */
    public static void main(String[] args) {
        com.mchange.v2.log.MLog.getLogger().setLevel(MLevel.OFF);
        init(args);

        try (Scanner scanner = new Scanner(input, "utf-8")) {
            new CommandService(scanner, out, directory)
                    .init()
                    .printLineSeparator(numLines)
                    .checkPassword()
                    .printHelp()
                    .commandProcess();

        } catch (Throwable throwable) {
            out.println("Невідома помилка");
            logger.error(throwable);
            exit(0);
        }
    }

    /**
     * инициализация путей
     *
     * @param args аргументы
     */
    private static void init(String[] args) {

//        if (args.length > 0) {
//            numLines = Integer.parseInt(args[0]);
//        }

//        if (args.length > 0) {
//            directory = args[0];
//        }

        if (args.length > 0 && args[0].equals("system")) {
            out = System.out;
            input = System.in;
        } else {
            try {
                out = new PrintStream(FILE_INPUT_OUTPUT,"utf-8");

                input = new FileInputStream(FILE_INPUT_OUTPUT);

//                new FileInputStream(FILE_INPUT_OUTPUT);
            } catch (FileNotFoundException e) {
                logger.error(e);
            } catch ( UnsupportedEncodingException e ) {
                e.printStackTrace();
            }
        }
    }
}
