package ua.com.ator.trainSE.db.dao;

import ua.com.ator.trainSE.db.entity.LogRow;
import ua.com.ator.trainSE.db.entity.Page;
import ua.com.ator.trainSE.db.entity.View;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static ua.com.ator.trainSE.db.ConnectionDB.closeConnection;
import static ua.com.ator.trainSE.db.ConnectionDB.createConnection;

/**
 * @author Dmitry Sokolov on 16.11.2017.
 */
public class Dao implements IDao {
    public static void main(String[] args) {


    }

    private String selectPages = "select  v.id, v.page_name,  GROUP_CONCAT(vm.message_id) as ids " +
            "from views v " +
            "LEFT JOIN view_messages vm ON v.id = vm.view_id " +
            "group by v.id";


    @Override
    public List<Page> getPages() throws SQLException {
        Connection connection = createConnection();
        List<Page> logPageViews = new LinkedList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(selectPages);

            while (resultSet.next()) {
                logPageViews.add(new Page(resultSet.getInt("id"),
                        resultSet.getString("page_name"),
                        resultSet.getString("ids") != null ?
                                Arrays.stream(resultSet.getString("ids").split(",")).mapToInt(Integer::parseInt).toArray()
                                : new int[0]));
            }
        } finally {
            closeConnection(connection);
        }
        return logPageViews;
    }

    @Override
    public void createPage(String pageName) throws SQLException {
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement("insert into views (view_name, page_name) values (?, ?)")) {
            statement.setString(1, "view" + String.valueOf(pageName.hashCode()));
            statement.setString(2, pageName);
            statement.executeUpdate();
        } finally {
            closeConnection(connection);
        }

    }

    @Override
    public void deletePage(int pageId) throws SQLException {
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement("delete from views where id = ?")) {

            statement.setInt(1, pageId);

            statement.executeUpdate();
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public void addMessage(int pageId, int... msgIds) throws SQLException {
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement(getSetMessageQuery(pageId, msgIds))) {
            statement.executeUpdate();
        } finally {
            closeConnection(connection);
        }

    }

    @Override
    public void removeMessage(int pageId, int msgId) throws SQLException {
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement("delete from view_messages where view_id = ? and message_id = ?")) {

            statement.setInt(1, pageId);
            statement.setInt(2, msgId);

            statement.executeUpdate();
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public void deleteLogs(Timestamp from, Timestamp to) throws SQLException {
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement("delete from logs where timestamp between ? AND ?")) {

            statement.setTimestamp(1, from);
            statement.setTimestamp(2, to);

            statement.executeUpdate();
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public List<LogRow> load(Timestamp from, Timestamp to) {
        List<LogRow> logRows = new ArrayList<>();
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "select temp.`timestamp`, temp.`value`, temp.`is_format`, temp.`mnemonic` from (select l.`timestamp`, l.`value`, m.`is_format`, m.`mnemonic` from logs l left outer join `messages` m on l.`code` = m.`id` order by l.`timeStamp`) as temp where  temp.`timestamp` between ? and ?")) {
            statement.setTimestamp(1, from);
            statement.setTimestamp(2, to);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                logRows.add(getLogRow(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return logRows;
    }

    @Override
    public List<View> loadViews() {
        List<View> views = new ArrayList<>();
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement("select `id`,`page_name` from `views`")) {

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                views.add(new View(resultSet.getInt("id"), resultSet.getString("page_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return views;
    }

    @Override
    public List<LogRow> loadAllLogs() {
        List<LogRow> logRows = new ArrayList<>();
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "select temp.`timestamp`, temp.`value`, temp.`is_format`, temp.`mnemonic` from (select l.`timestamp`, l.`value`, m.`is_format`, m.`mnemonic` from logs l left outer join `messages` m on l.`code` = m.`id` order by l.`timeStamp`) as temp")) {

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                logRows.add(getLogRow(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return logRows;
    }

    @Override
    public List<LogRow> loadViewLogs(int id) {
        List<LogRow> logRows = new ArrayList<>();
        Connection connection = createConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "select temp.`timestamp`, temp.`value`, temp.`is_format`, temp.`mnemonic`, temp.`id`" +
                        " from " +
                        " (select l.`timestamp`, l.`value`, m.`is_format`, m.`mnemonic`, m.`id`" +
                        " from logs l" +
                        "  left outer join `messages` m on l.`code` = m.`id`" +
                        " order by l.`timeStamp`) as temp" +
                        " where temp.`id` in (" +
                        " select view_messages.message_id" +
                        " from view_messages" +
                        " where view_messages.view_id = ?)")) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                logRows.add(getLogRow(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return logRows;
    }

    private String getSetMessageQuery(int pageId, int... msgIds) {
        return "insert into view_messages (view_id, message_id) values"
                + Arrays.stream(msgIds)
                .mapToObj(msgId -> "(" + pageId + "," + msgId + ")")
                .collect(Collectors.joining(","));
    }

    private LogRow getLogRow(ResultSet resultSet) throws SQLException {
        return new LogRow(
                resultSet.getTimestamp("timestamp"),
                resultSet.getDouble("value"),
                resultSet.getString("mnemonic"),
                resultSet.getBoolean("is_format")
        );
    }
}
