package ua.com.ator.trainSE.db.entity;

/**
 * @author Dmitry Sokolov on 16.02.2018.
 */
public class View implements Comparable {
    private int id;
    private String pageName;

    public View(int id, String pageName) {
        this.id = id;
        this.pageName = pageName;
    }

    public int getId() {
        return id;
    }

    public String getPageName() {
        return pageName;
    }


    @Override
    public int compareTo(Object o) {
        return this.getId() - ((View) o).getId();
    }
}
