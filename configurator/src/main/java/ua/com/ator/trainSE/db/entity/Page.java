package ua.com.ator.trainSE.db.entity;

import java.util.Arrays;

/**
 * @author Dmitry Sokolov on 16.11.2017.
 */
public class Page {

    private int id;
    private String name;
    private int[] msgIds;

    public Page(int id, String name, int[] msgIds) {
        this.id = id;
        this.name = name;
        this.msgIds = msgIds;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int[] getMsgIds() {
        return msgIds;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Page{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", msgIds=").append(Arrays.toString(msgIds));
        sb.append('}');
        return sb.toString();
    }
}
