package ua.com.ator.trainSE.command;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.isNull;

/**
 * @author Dmitry Sokolov on 13.04.2017.
 */
public class Command {
    private String command;
    private String description;
    private LinkedList<SubCommand> subCommands;
    private Consumer<String[]> consumer;

    /**
     * конструктор
     *
     * @param command     имя команды
     * @param description описание команды
     * @param consumer    колбэк
     */
    public Command(String command, String description, Consumer<String[]> consumer) {
        this.command = command;
        this.description = description;
        this.consumer = consumer;
        this.subCommands = new LinkedList<>();
    }

    /**
     * конструктор
     *
     * @param command     имя команды
     * @param description описание команды
     */
    public Command(String command, String description) {
        this(command, description, null);
    }

    /**
     * добавит подкоманду
     *
     * @param subCommand   подкоманда
     * @param description  описание
     * @param propertyName имя проперти
     * @param consumer     колбэк
     * @return this
     */
    public Command setSubCommand(String subCommand, String description, String propertyName, Consumer<String[]> consumer) {
        subCommands.add(new SubCommand(subCommand, description, propertyName, consumer));
        return this;
    }

    /**
     * вернет команду
     *
     * @return команда
     */
    public String getCommand() {
        return command;
    }

    /**
     * вернет описание команды
     *
     * @return описание команнды
     */
    public String getDescription() {
        return description;
    }

    /**
     * вернес список подкоманд
     *
     * @return подкоманды
     */
    public List<SubCommand> getSubCommands() {
        return subCommands;
    }

    /**
     * дергнет колбек
     *
     * @param args команда
     */
    public void accept(String[] args) {
        consumer.accept(args);
    }

    /**
     * @return true если у команды есть consumer
     */
    public boolean isAcceptable() {
        return !isNull(consumer);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Command{");
        sb.append("command='").append(command).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", subCommands=").append(subCommands);
        sb.append('}');
        return sb.toString();
    }
}
