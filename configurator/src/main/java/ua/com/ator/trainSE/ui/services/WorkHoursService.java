package ua.com.ator.trainSE.ui.services;

import java.util.prefs.Preferences;

/**
 * Preferences потокобезопасны и не требуют внешней синхронизации
 */
public class WorkHoursService {

    /**
     * Хранилище моточасов
     */
    private Preferences storage = Preferences.userNodeForPackage(WorkHoursService.class);


    public void setExpendedResources(int minutes) {
        storage.putInt("totalWorkHours", minutes);
    }

    public int getExpendedResources() {
        return storage.getInt("totalWorkHours", 0);
    }
}
