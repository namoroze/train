package ua.com.ator.trainSE.services.impl;

import org.apache.log4j.Logger;
import ua.com.ator.trainSE.command.Command;
import ua.com.ator.trainSE.command.SubCommand;
import ua.com.ator.trainSE.db.dao.Dao;
import ua.com.ator.trainSE.db.dao.IDao;
import ua.com.ator.trainSE.db.entity.LogRow;
import ua.com.ator.trainSE.db.entity.Page;
import ua.com.ator.trainSE.services.ICommandService;
import ua.com.ator.trainSE.ui.services.WorkHoursService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.lang.System.exit;
import static java.lang.System.lineSeparator;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * @author Dmitry Sokolov on 13.04.2017.
 */
public class CommandService implements ICommandService {
    private static final Logger logger = Logger.getLogger("SETUP_APP");
    private static final String DEVICE_PATH = "/dev";
    private static final String DEVICE_PATTERN = "/dev/mmcblk1p1";
    private static final String MOUNT_POINT = "/mnt/usb";
    private static final String CREATE_MOUNT_POINT_DIR_COMMAND = "mkdir /mnt/usb";
    private static final String OUT_FILE_NAME_PATTERN = "%s_%s_%s_%s.log";

    private final IDao DAO;
    private List<Page> pages;

    private static final String FILE_SOURCE_PATH = "/config.properties";
    private static final String FILE_DESTINATION_NAME = "/%s_config.properties";
    private String directory;
    private Properties properties;

    private Scanner scanner;

    private static final String HELP_FORMAT = "  %-8s - %s\n";

    private static final String BACKUP_FORMAT = "  %-8s - %-25s %s\n";
    private static final String SUB_HELP_FORMAT = "  %-8s %-10s - %s\n";
    private static final String PRINT_CHANGES_FORMAT = "  %-10s | %-10s | %-10s\n  ------------------------------------\n";
    private static final String INVALID_COMMAND_FORMAT = "  Невірна команда '%s'%s";
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter DEPLOY_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final Pattern BACKUP_PATTERN = Pattern.compile("^(backup|default)_config\\.properties");
    private PrintStream out;
    private Map<Integer, Path> backupFiles = new TreeMap<>();
    private List<Command> commands;

    /**
     * конструктор
     *
     * @param scanner   сканер
     * @param out       поток вывода
     * @param directory путь к файлу
     */
    public CommandService(Scanner scanner, PrintStream out, String directory) {
        this.scanner = scanner;
        this.out = out;
        this.directory = directory;
        this.commands = new ArrayList<>();
        this.properties = readProperties(directory + FILE_SOURCE_PATH);
        this.DAO = new Dao();
    }

    /**
     * метод инициализирует доступрые команды
     */
    @Override
    public CommandService init() {

        commands.add(new Command("help", "допомога", args -> helpProcess()));

        commands.add(new Command("log", "налаштування відображення сторінок логів")
                .setSubCommand("show", "подивитись доступні сторінки логів та їх ідентефікатори", "", this::showAvailablePagesProcess)
                .setSubCommand("msg", "подивитись відображувані повідомлення за ідентифікатором сторінки логів", "", this::showPageMessagesProcess)
                .setSubCommand("add", "дадати сторінку логів", "", this::addPageProcess)
                .setSubCommand("del", "видалити сторінку логів", "", this::delPageProcess)
                .setSubCommand("set", "додати повідомлення на сторінку логів(log set 1(ідентифікатор_сторінки) 1,2,3...(ідентифікатор(и)_повідомлення))", "", this::setMsgToPageProcess)
                .setSubCommand("rem", "видалити повідомлення зі сторінки логів(log rem 1(ідентифікатор_сторінки) 1(ідентифікатор_повідомлення))", "", this::remMsgToRageProcess)
                .setSubCommand("clear", "видалити повідомлення (log clear [початкова_дата] [кінцева_дата або] log clear [кінцева_дата] (формат рррр-мм-дд год:хв:сек))", "", this::clearLogsProcess)
                .setSubCommand("write", "записати повідомлення на карту пам'яті", "", arg -> writeToFlash())
        );

        commands.add(new Command("exit", "вихід з програми", args -> exitProcess()));

        commands.add(new Command("values", "записати на карту пам'яті команди та встановленні значення", args -> writeCommandsWithDefaultValuesToFlash()));

        commands.add(new Command("save", "зберегти зміни", args -> saveProcess()));

        commands.add(new Command("backup", "повернутися до попереднього стану файлу", this::backupProcess));

        commands.add(new Command("reset", "скидання незбережених налаштувань", args -> resetProcess()));

        commands.add(new Command("res", "установка використаного ресурсу мотогодин у хвилинах", this::resProcess));

        commands.add(new Command("time", "установка дати та часу, формат 'рррр-мм-дд год:хв:сек'", this::systemDateTimeProcess));

        commands.add(new Command("offset", "Корекція дельт для аналогових входів")
                .setSubCommand("tc", "Корекція дельт для температури купе", "OFFSET_TEMP_COUPE", this::doubleProcess)
                .setSubCommand("af", "Корекція дельт для припливної вентиляції", "OFFSET_TEMP_AIR_FLOW", this::doubleProcess)
                .setSubCommand("to", "Корекція дельт для температури зовнішнього повітря", "OFFSET_TEMP_OUT", this::doubleProcess)
                .setSubCommand("tb", "Корекція дельт для температури котла", "OFFSET_TEMP_BOILER", this::doubleProcess)
                .setSubCommand("cb", "Корекція дельт для струму батареї", "OFFSET_CURR_BAT", this::doubleProcess)
                .setSubCommand("cg", "Корекція дельт для струму генератора", "OFFSET_CURR_GEN", this::doubleProcess)
                .setSubCommand("nv", "Корекція дельт для напруги мережі", "OFFSET_NET_VOLT", this::doubleProcess)

                .setSubCommand("cmc", "Коефіцієнт нахилу(матеріалу) для температури купе", "COEFF_MATERIAL_TEMP_COUPE", this::doubleProcess)
                .setSubCommand("cmf", "Коефіцієнт нахилу(матеріалу) для припливної вентиляції", "COEFF_MATERIAL_TEMP_AIR_FLOW", this::doubleProcess)
                .setSubCommand("cmo", "Коефіцієнт нахилу(матеріалу) для температури зовнішнього повітря", "COEFF_MATERIAL_TEMP_OUT", this::doubleProcess)
                .setSubCommand("cmb", "Коефіцієнт нахилу(матеріалу) для температури котла", "COEFF_MATERIAL_TEMP_BOILER", this::doubleProcess)
                .setSubCommand("cmi", "Коефіцієнт нахилу(матеріалу) для струму батареї", "COEFF_MATERIAL_CURR_BAT", this::doubleProcess)
                .setSubCommand("cmg", "Коефіцієнт нахилу(матеріалу) для струму генератора", "COEFF_MATERIAL_CURR_GEN", this::doubleProcess)
                .setSubCommand("cmv", "Коефіцієнт нахилу(матеріалу) для напруги мережі", "COEFF_MATERIAL_NET_VOLT", this::doubleProcess)

                .setSubCommand("oc", "Дільник для значень АЦП", "OFFSET_COEFFICIENT", this::doubleProcess));

        commands.add(new Command("temp", "налаштування для температур")
                .setSubCommand("sev", "Значення при якому з'являеться повідомлення про обрив датчика", "SENSOR_ERROR_VALUE", this::doubleProcess)
                .setSubCommand("bt", "максимальна температура котла, °C", "MAX_BOILER_TEMPERATURE", this::integerProcess)
                .setSubCommand("ibt", "інтервал перевищення максимальної температури котла, мСек", "INTERVAL_MAX_BOILER_TEMPERATURE", this::integerProcess)
                .setSubCommand("tpo", "зовнішня температура необхідна для включення насоса, °C", "MAX_TEMPERATURE_PUMP_ON", this::integerProcess)
                .setSubCommand("onnvo", "зовнішня температура необхідна для включення НВО, °C", "OUT_TEMP_TO_ON_NVO", this::integerProcess)
                .setSubCommand("onlvhd", "гістерезис температури для включення НВО, °C", "TEMP_DIFF_TO_ON_LVH", this::integerProcess)
                .setSubCommand("offlvhd", "гістерезис температури для виключення НВО, °C", "TEMP_DIFF_TO_OFF_LVH", this::integerProcess)
                .setSubCommand("offnvo", "зовнішня температура необхідна для виключення НВО, °C", "OUT_TEMP_TO_OFF_NVO", this::integerProcess)
                .setSubCommand("vvohefon", "гістерезис для температури уставки для включення ВВО", "HVH_HYSTERESIS_ESTABLISHED_FOR_ON", this::integerProcess)
                .setSubCommand("vvohefoff", "гістерезис для температури уставки для відключення ВВО", "HVH_HYSTERESIS_ESTABLISHED_FOR_OFF", this::integerProcess)
                .setSubCommand("vvootfdon", "зовнішня температура необхідна для ввімкненя другої групи ВВО", "HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_ON", this::integerProcess)
                .setSubCommand("vvootfdoff", "зовнішня температура необхідна для вимкненя другої групи ВВО", "HVH_OUTSIDE_TEMPERATURE_FOR_DUAL_OFF", this::integerProcess)
                .setSubCommand("vvohoton", "зовнішня температура необхідна для ввімкненяя ВВО", "HVH_OUTSIDE_TEMPERATURE_ON", this::integerProcess)
                .setSubCommand("vvohotoff", "зовнішня температура необхідна для вимкненя ВВО", "HVH_OUTSIDE_TEMPERATURE_OFF", this::integerProcess)
                .setSubCommand("x0", "T(вкл) = x0 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_BASE_BOILER_TEMP_FOR_ON", this::integerProcess)
                .setSubCommand("x1", "T(викл) = x1 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_BASE_BOILER_TEMP_FOR_OFF", this::integerProcess)
                .setSubCommand("x2", "T(вкл/викл) = x1 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_MULTIPLER_1GROUP", this::integerProcess)
                .setSubCommand("x3", "T(вкл/викл) = x1 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_SUBSTRACT_ESTABLISHED_1GROUP", this::integerProcess)
                .setSubCommand("x4", "T(вкл/викл) = x1 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_MULTIPLER_2GROUP", this::integerProcess)
                .setSubCommand("x5", "T(вкл/викл) = x1 + x2 * (T(уст)-x3) - x4 * Т(нв) + x5 * (T(уст) - T(сал))", "HVH_FORMULA_MULTIPLER_3GROUP", this::integerProcess)
                .setSubCommand("sut", "температура уставки, °C", "SETUP_TEMP", this::integerProcess));

        commands.add(new Command("bat", "налаштування для батареї")
                .setSubCommand("mcb", "максимальний струм батареї, А", "MAX_CURRENT_BAT", this::integerProcess)
                .setSubCommand("imcb", "інтервал перевищення струму батареї, мСек", "INTERVAL_MAX_CURRENT_BAT", this::integerProcess));

        commands.add(new Command("lim", "ліміти які відображаються на головній сторінці")
                .setSubCommand("micb", "мінімум струм батареї", "CURRENT_BAT_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("macb", "максимум струм батареї", "CURRENT_BAT_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("micg", "мінімум струм генератора", "CURRENT_GEN_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("macg", "максимум струм генератора", "CURRENT_GEN_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("mivn", "мінімум напруга бортової мережі", "VOLTAGE_BOARD_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("mavn", "максимум напруга бортової мережі", "VOLTAGE_BOARD_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("mitb", "мінімум температура котла", "BOILER_TEMP_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("matb", "максимум температура котла", "BOILER_TEMP_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("mito", "мінімум температура ззовні", "OUTSIDE_TEMP_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("mato", "максимум температура ззовні", "OUTSIDE_TEMP_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("miti", "мінімум температура салону", "INSIDE_TEMP_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("mati", "максимум температура салону", "INSIDE_TEMP_UPPER_LIMIT", this::integerProcess)
                .setSubCommand("mitaf", "мінімум температура припливної вентиляції", "AIRFLOW_TEMP_LOWER_LIMIT", this::integerProcess)
                .setSubCommand("mataf", "максимум температура припливної вентиляції", "AIRFLOW_TEMP_UPPER_LIMIT", this::integerProcess)
        );

        commands.add(new Command("gen", "налаштування для генератора")
                .setSubCommand("mcg", "максимальний струм генератора, А", "MAX_CURRENT_GENERATOR", this::integerProcess)
                .setSubCommand("imcg", "інтервал перевищення струму генератора, мСек", "INTERVAL_MAX_CURRENT_GENERATOR", this::integerProcess));

        commands.add(new Command("volt", "налаштування для бортовий мережі")
                .setSubCommand("mabn", "максимальна напруга бортової  мережі, V", "MAX_VOLTAGE_BOARD_NETWORK", this::integerProcess)
                .setSubCommand("maibn", "інтервал перевищення напруги бортової мережі, мСек", "INTERVAL_MAX_VOLTAGE_BOARD_NETWORK", this::integerProcess)
                .setSubCommand("mibn", "мінімальна напруга бортової мережі, V", "MIN_VOLTAGE_BOARD_NETWORK", this::integerProcess)
                .setSubCommand("miibn", "інтервал заниження напруги бортової мережі, мСек", "INTERVAL_MIN_VOLTAGE_BOARD_NETWORK", this::integerProcess));

        commands.add(new Command("avg", "затримки накопичення")
                .setSubCommand("coupe", "температура салону", "AVG_DELAY_TEMP_COUPE", this::integerProcess)
                .setSubCommand("airflow", "температура припливної вентиляції", "AVG_DELAY_TEMP_AIR_FLOW", this::integerProcess)
                .setSubCommand("out", "температура ззовні", "AVG_DELAY_TEMP_OUT", this::integerProcess)
                .setSubCommand("boiler", "температура котла", "AVG_DELAY_TEMP_BOILER", this::integerProcess)
                .setSubCommand("bat", "струм батареї", "AVG_DELAY_CURR_BAT", this::integerProcess)
                .setSubCommand("gen", "струм генератора", "AVG_DELAY_CURR_GEN", this::integerProcess)
                .setSubCommand("volt", "напруга мережі", "AVG_DELAY_NET_VOLT", this::integerProcess));

        commands.add(new Command("delay", "налаштування затримок НВО та ВВО")
                .setSubCommand("vvodsd", "час простою перед наступним включенням ВВО, мСек", "HVH_DELAY_STATE_DELAY", this::integerProcess)
                .setSubCommand("vvosod", "затримка включення ВВО, мСек", "HVH_SWITCH_ON_DELAY", this::integerProcess)
                .setSubCommand("vvord", "затримка відповіді включення ВВО від БВВ, мСек", "HVH_RESPONSE_DELAY", this::integerProcess)
                .setSubCommand("dlhon", "затримка включення НВО, мСек", "LOW_HEATING_DELAY_ON", this::integerProcess)
                .setSubCommand("dlhoff", "затримка виключення НВО, мСек", "LOW_HEATING_DELAY_OFF", this::integerProcess)
                .setSubCommand("dlhr", "затримка відповіді включення НВО від БВВ, мСек", "LOW_HEATING_DELAY_RESPONSE", this::integerProcess)
                .setSubCommand("dlh", "затримка повторного включення НВО, мСек", "LOW_HEATING_DELAY_BEFORE_NEXT_TRY", this::integerProcess));

        commands.add(new Command("wagon", "налаштування параметрів вагону")
                .setSubCommand("dd", "дата установки ПЗ, формат 'рррр-мм-дд'", "DEPLOY_DATE", this::dateProcess)
                .setSubCommand("sv", "версія ПЗ", "SOFTWARE_VERSION", this::stringProcess)
                .setSubCommand("num", "номер вагона", "WAGON", this::stringProcess));

        commands.add(new Command("password", "зміна пароля")
                .setSubCommand("new", "зміна пароля", "PASSWORD", this::passwordProcess));

        commands.add(new Command("screen", "налаштування параметрів экрану")
                .setSubCommand("opt", "увімкнення режиму збереження екрану(true або false)", "SCREEN_SAVER_ENABLE", this::trueFalseProcess)
                .setSubCommand("min", "мінімальна яркість(від 0 до 7)", "SCREEN_SAVER_MIN_BRIGHTNESS", this::integerProcess)
                .setSubCommand("max", "максимальна яркість(від 0 до 7)", "SCREEN_SAVER_MAX_BRIGHTNESS", this::integerProcess)
                .setSubCommand("delay", "затримка(мс)", "SCREEN_SAVER_DELAY", this::integerProcess)
        );

        commands.add(new Command("cond", "налаштування кондиціонера")
                .setSubCommand("ccm", "Вкл/Вимк спільної роботи НВО та Ел. Калоріфер (Нагрів), true або false", "CONDITIONER_COMBINE_MODE", this::trueFalseProcess)
        );

        commands.add(new Command("resist", "аварія при порушенні ізоляції")
                .setSubCommand("val", "true або false", "INSULATION_RESISTANCE_IS_ERROR", this::trueFalseProcess)
        );

        return this;
    }

    private CommandService writeToFlash() {
        String date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(new Date(System.currentTimeMillis()));
        String wagonNumber = properties.getProperty("WAGON", "");
        String deployDate = "Дата введення в експлуатацію - " + properties.getProperty("DEPLOY_DATE", "не встановлено") + "\r\n";
        int minutes = new WorkHoursService().getExpendedResources();
        String workingTime = String.format("Час напрацювання - %dгод %dхв\r\n", minutes / 60, minutes % 60);

        try {
            if (Files.notExists(Paths.get("/dev/mmcblk1p1"))) {
                out.println("  Накопичувача не знайдено!");
                logger.error("Накопичувача не знайдено!");
                return this;
            }
            Runtime.getRuntime().exec("mkdir /mnt/usb").waitFor();
            Runtime.getRuntime().exec("mount /dev/mmcblk1p1 /mnt/usb").waitFor();

            String fileName = String.format("%s_%s.txt", wagonNumber, date);


            writeData("Номер вагона - "
                            .concat(wagonNumber)
                            .concat("\r\n")
                            .concat(workingTime)
                            .concat(deployDate)
                            .concat(DAO.loadAllLogs()
                                    .stream()
                                    .map(LogRow::toString)
                                    .collect(joining("\r\n")))
                    , directory + "/" + fileName);

            Runtime.getRuntime().exec("mv " + directory + "/" + fileName + " " + MOUNT_POINT).waitFor();

            final int[] pageNumber = {0};

            DAO.loadViews().stream().sorted().forEach(view -> {
                String fileNameView = String.format("%s_%s_%d.txt", wagonNumber, date, ++pageNumber[0]);

                try {
                    writeData("Номер вагона - "
                                    .concat(wagonNumber)
                                    .concat("\r\n")
                                    .concat(workingTime)
                                    .concat(deployDate)
                                    .concat(view.getPageName())
                                    .concat("\r\n\r\n")
                                    .concat(DAO.loadViewLogs(view.getId())
                                            .stream()
                                            .map(LogRow::toString)
                                            .collect(joining("\r\n")))
                            , directory + "/" + fileNameView);

                    Runtime.getRuntime()
                            .exec("mv " + directory + "/" + fileNameView + " " + MOUNT_POINT).waitFor();

                } catch (IOException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });

            Runtime.getRuntime().exec("umount " + MOUNT_POINT).waitFor();

            out.println("  Виконано!");

        } catch (IOException | InterruptedException | RuntimeException e) {
            out.println("  Невиконано!");
            logger.error(e);
        }


        return this;
    }

    private void writeData(String data, String fileName) throws IOException {
        Files.write(Paths.get(fileName), data.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }

    private void clearLogsProcess(String[] strings) {

        if (strings.length > 3) {
            Timestamp from;
            Timestamp to;
            String time = strings[2].concat(" ").concat(strings[3]);

            try {
                if (strings.length > 4) {
                    from = Timestamp.valueOf(time);
                    time = strings[4].concat(" ").concat(strings[5]);
                    to = Timestamp.valueOf(time);
                } else {
                    from = new Timestamp(0);
                    to = Timestamp.valueOf(time);
                }
                DAO.deleteLogs(from, to);
            } catch (IllegalArgumentException e) {
                out.printf(INVALID_COMMAND_FORMAT, time, ", невірний формат дати, гггг-мм-дд чч:мм:сс" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, Arrays.toString(strings), ", невірний формат дати, гггг-мм-дд чч:мм:сс"));
            } catch (SQLException e) {
                printNoDB();
                logger.error(e);
            }
        } else {
            out.printf(INVALID_COMMAND_FORMAT, Arrays.stream(strings).collect(joining(" ")), ", введіть дату у форматі гггг-мм-дд чч:мм:сс" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, Arrays.toString(strings), ", введіть дату у форматі гггг-мм-дд чч:мм:сс"));
        }

    }

    private void showPageMessagesProcess(String[] strings) {
        if (strings.length > 2) {
            try {
                int pageId = Integer.parseInt(strings[2]);
                pages = DAO.getPages();
                pages.stream().filter(page -> page.getId() == pageId).forEach(page -> out.print("  " + Arrays.stream(page.getMsgIds()).mapToObj(String::valueOf).collect(joining(", "))));
                out.println();
            } catch (NumberFormatException e) {
                out.printf(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом"));
            } catch (SQLException e) {
                printNoDB();
                logger.error(e);
            }
        } else {
            out.printf(INVALID_COMMAND_FORMAT, "", ", введіть ідентифікатор сторінки" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, Arrays.toString(strings), ", введіть ідентифікатор сторінки"));
        }

    }


    private void showAvailablePagesProcess(String[] strings) {
        try {
            pages = DAO.getPages();
            pages.forEach(page -> out.println("  " + page.getId() + " - " + page.getName()));
        } catch (SQLException e) {
            printNoDB();
            logger.error(e);
        }
    }

    private void setMsgToPageProcess(String[] strings) {
        int pageId;
        int[] msgIds;
        if (strings.length > 3) {
            try {
                pageId = Integer.parseInt(strings[2]);
                try {
                    msgIds = Arrays.stream(strings[3].split(",")).mapToInt(Integer::parseInt).toArray();
                    pages = DAO.getPages();

                    DAO.addMessage(pageId, Arrays.stream(msgIds)
                            .filter(value -> pages.stream()
                                    .filter(page -> page.getId() == pageId)
                                    .anyMatch(page -> Arrays.stream(page.getMsgIds()).noneMatch(value1 -> value == value1)))
                            .toArray());

                    pages = DAO.getPages();
                } catch (NumberFormatException e) {
                    out.printf(INVALID_COMMAND_FORMAT, strings[3], ", значення має бути числом" + lineSeparator());
                    logger.info(format(INVALID_COMMAND_FORMAT, strings[3], ", значення має бути числом"));
                } catch (SQLException e) {
                    printNoDB();
                    logger.error(e);
                }
            } catch (NumberFormatException e) {
                out.printf(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом"));
            }
        } else {
            out.printf(INVALID_COMMAND_FORMAT, "", ", log set 1(ідентифікатор сторінки) 1,2,3...(ідентифікатор(и) повідомлення)" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, "", ", log set 1(ідентифікатор сторінки) 1,2,3...(ідентифікатор(и) повідомлення)"));
        }
    }

    private void remMsgToRageProcess(String[] strings) {
        int pageId;
        int msgId;
        if (strings.length > 3) {
            try {
                pageId = Integer.parseInt(strings[2]);
                try {
                    msgId = Integer.parseInt(strings[3]);
                    pages = DAO.getPages();


                    if (pages.stream()
                            .filter(page -> page.getId() == pageId)
                            .anyMatch(page -> Arrays.stream(page.getMsgIds()).anyMatch(value -> msgId == value))) {
                        DAO.removeMessage(pageId, msgId);
                    }

                    pages = DAO.getPages();
                } catch (NumberFormatException e) {
                    out.printf(INVALID_COMMAND_FORMAT, strings[3], ", значення має бути числом" + lineSeparator());
                    logger.info(format(INVALID_COMMAND_FORMAT, strings[3], ", значення має бути числом"));
                } catch (SQLException e) {
                    printNoDB();
                    logger.error(e);
                }
            } catch (NumberFormatException e) {
                out.printf(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом"));
            }
        } else {
            out.printf(INVALID_COMMAND_FORMAT, "", ", log rem 1(ідентифікатор сторінки) 1(ідентифікатор повідомлення)" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, "", ", log rem 1(ідентифікатор сторінки) 1(ідентифікатор повідомлення)"));
        }
    }

    private void addPageProcess(String[] strings) {
        if (strings.length > 2) {

            try {
                DAO.createPage(Arrays.stream(Arrays.copyOfRange(strings, 2, strings.length)).collect(Collectors.joining(" ")));
            } catch (SQLException e) {
                printNoDB();
                logger.error(e);
            }

        } else {
            out.printf(INVALID_COMMAND_FORMAT, "", ", введіть ім'я нової сторінки" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, "", ", введіть ім'я нової сторінки"));
        }
    }

    private void delPageProcess(String[] strings) {
        if (strings.length > 2) {
            int id = Integer.parseInt(strings[2]);
            try {
                DAO.deletePage(id);
            } catch (NumberFormatException e) {
                out.printf(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, strings[2], ", значення має бути числом"));
            } catch (SQLException e) {
                printNoDB();
                logger.error(e);
            }

        } else {
            out.printf(INVALID_COMMAND_FORMAT, "", ", введіть ідентифікатор сторінки" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, "", ", ідентифікатор сторінки"));
        }
    }


    private void trueFalseProcess(String[] args) {
        String propName = getPropertyName(args[1]);

        if (args.length < 3) {
            out.println("  " + properties.get(propName));
        } else if ("true".equals(args[2]) || "false".equals(args[2])) {
            properties.setProperty(propName, args[2]);
            logger.info(format("%s %s %s\n", args[0], args[1], args[2]));
        } else {
            out.printf(INVALID_COMMAND_FORMAT, args[0], ", значення має бути true або false" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, args[0], ", значення має бути true або false"));
        }
    }

    private void systemDateTimeProcess(String[] args) {
        if (args.length < 2) {
            out.println("  " + new Date(System.currentTimeMillis()));
            return;
        }
        try {
            LocalDateTime localDateTime = LocalDateTime.parse(args[1] + " " + args[2], DATE_TIME_FORMATTER);
            Runtime.getRuntime()
                    .exec(String.format("hwclock --set --date=\"%02d/%02d/%02d %02d:%d:%02d\" --localtime",
                            localDateTime.getMonthValue(),
                            localDateTime.getDayOfMonth(),
                            localDateTime.getYear(),
                            localDateTime.getHour(),
                            localDateTime.getMinute(),
                            localDateTime.getSecond()))
                    .destroy();
//            Runtime.getRuntime()
//                    .exec("date " + String.format("%02d%02d%02d%02d%d.%02d",
//                            localDateTime.getMonthValue(),
//                            localDateTime.getDayOfMonth(),
//                            localDateTime.getHour(),
//                            localDateTime.getMinute(),
//                            localDateTime.getYear(),
//                            localDateTime.getSecond()))
//                    .destroy();
//            Runtime.getRuntime()
//                    .exec("hwclock --systohc")
//                    .destroy();

        } catch (DateTimeParseException e) {
            out.printf(INVALID_COMMAND_FORMAT, args[0], ", невірный формат дати" + lineSeparator());
            logger.info(format(INVALID_COMMAND_FORMAT, args[0], ", невірный формат дати"));
        } catch (Throwable throwable) {
            out.printf(INVALID_COMMAND_FORMAT, args[0], ", дата не може бути змінена, зверніться до адміністратора" + lineSeparator());
            logger.error(throwable);
        }
    }

    /**
     * обработчик команд
     *
     * @param args аргументы
     */
    private void handle(String[] args) {
        if (args.length > 0) {
            Optional<Command> command = commands.stream()
                    .filter(c -> c.getCommand().equals(args[0]))
                    .findAny();
            if (command.isPresent()) {
                if (command.get().isAcceptable()) {
                    command.get().accept(args);
                } else {
                    if (args.length > 1) {
                        Optional<SubCommand> subCommand = command.get().getSubCommands().stream().filter(sc -> sc.getSubCommand().equals(args[1])).findAny();
                        if (subCommand.isPresent()) {
                            subCommand.get().accept(args);
                        } else {
                            out.printf(INVALID_COMMAND_FORMAT, args[1], lineSeparator());
                            logger.warn(format(INVALID_COMMAND_FORMAT, args[1], ""));
                        }
                    } else {
                        printHelpSubCommands(args[0]);
                    }
                }
            } else {
                out.printf(INVALID_COMMAND_FORMAT, args[0], ", введіть команду 'help', щоб побачити список доступних команд" + lineSeparator());
                logger.info(format(INVALID_COMMAND_FORMAT, args[0], ", введіть команду 'help', щоб побачити список доступних команд"));
            }
        }
    }

    /**
     * напечатает изменения, если они были
     *
     * @return true если были изменнения
     */
    private boolean printChanges(Properties newProp, Properties oldProp) {

        if (newProp.entrySet().stream().anyMatch((e) -> !oldProp.get(e.getKey()).equals(e.getValue()))) {

            out.printf(PRINT_CHANGES_FORMAT, "старе", "нове", "опис");

            newProp.entrySet().stream()
                    .filter((e) -> !oldProp.get(e.getKey()).equals(e.getValue()))
                    .forEach(entry ->
                            out.printf(PRINT_CHANGES_FORMAT,
                                    oldProp.get(entry.getKey()),
                                    entry.getValue(),
                                    commands.stream()
                                            .filter(c -> c.getSubCommands().size() > 0
                                                    && c.getSubCommands().stream().anyMatch(sub -> sub.getPropertyName().equals(entry.getKey())))
                                            .map(s -> s.getSubCommands().stream()
                                                    .filter(st -> st.getPropertyName().equals(entry.getKey()))
                                                    .map(SubCommand::getDescription)
                                                    .collect(Collectors.joining()))
                                            .findAny()
                                            .orElse("  опису не знайдено")));
            return true;
        }
        return false;
    }

    /**
     * метод вычитывает файл
     *
     * @return property
     */
    private Properties readProperties(String fileName) {
        Properties property = new Properties();

        try (FileInputStream fis = new FileInputStream(fileName)) {
            property.load(fis);
        } catch (FileNotFoundException e) {
            out.println("  Файл " + fileName + " не знайдено");
            logger.error("Файл " + fileName + " не знайдено\n" + e.getMessage());
            exit(0);
        } catch (IOException e) {
            out.println("  Файл " + fileName + " не завантажено");
            logger.error("Файл " + fileName + " не завантажено " + e.getMessage());
            exit(0);
        }
        return property;
    }

    /**
     * выведет на печать
     *
     * @param num строка для печати
     * @return this
     */
    @Override
    public CommandService printLineSeparator(int num) {
        while ((num--) > 0) out.println(lineSeparator());
        return this;
    }

    /**
     * проверка пароля
     *
     * @return this
     */
    @Override
    public CommandService checkPassword() {
        do {
            out.print("\n  Введіть пароль#>");
        } while (!scanner.nextLine().equals(properties.getProperty("PASSWORD")));
        return this;
    }

    /**
     * выведет на печать доступные команды
     *
     * @return this
     */
    @Override
    public CommandService printHelp() {
        helpProcess();
        return this;
    }

    /**
     * вычитает команду  и отправит ее на обработку
     */
    @Override
    public void commandProcess() {
        while (true) {
            out.print("  Введіть команду#>");
            handle(scanner.nextLine().trim().split(" "));
        }
    }

    /**
     * выведет на печать подкоманды
     *
     * @param string команда, подкоманды которой нужно напечатать
     */
    private void printHelpSubCommands(String string) {
        commands.stream().filter(command -> command.getCommand().equals(string)).findAny().ifPresent(command -> command.getSubCommands().forEach(subCommand -> out.printf(SUB_HELP_FORMAT, subCommand.getSubCommand(), properties.getProperty(subCommand.getPropertyName(), ""), subCommand.getDescription())));
    }

    /**
     * вернет имя проперти
     *
     * @param string подкоманда
     * @return имя проперти
     */
    private String getPropertyName(String string) {
        return commands.stream()
                .map(command -> command.getSubCommands().stream()
                        .filter(subCommand -> subCommand.getSubCommand().equals(string))
                        .findAny()
                        .map(SubCommand::getPropertyName)
                        .orElse(""))
                .collect(Collectors.joining());
    }

    /**
     * сохранит изменения и создаст бэкап
     */
    private void saveProcess() {
        String source = directory + FILE_SOURCE_PATH;
        String dest = directory + format(FILE_DESTINATION_NAME, "backup");

        if (printChanges(properties, readProperties(source))) {
            String inLine;
            do {
                out.print("  Зберегти зміни? y/n ");
            }
            while (!(inLine = scanner.nextLine().trim()).equals("y") && !inLine.equals("n") && !inLine.equals("exit"));

            if (inLine.equals("y")) {
                try {
//                    if (new File(dest).exists()) Files.delete(get(dest));

                    Files.deleteIfExists(get(dest));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }


                try {
                    Files.copy(get(source), get(dest));
                } catch (IOException e) {
                    out.println("  Помилка створення резервної копії файлів налаштувань");
                    logger.error("Помилка створення резервної копії файлів налаштувань");
                    throw new RuntimeException(e);
                }

                try (PrintStream os = new PrintStream(source, "utf-8")) {
                    properties.store(os, null);
                } catch (IOException e) {
                    out.println("  Помилка відкриття файлу " + source);
                    logger.error("Помилка відкриття файлу " + source, e);
                }
                logger.info("save");
            }

        } else {
            out.println("  Жодного параметра не було змінено!");
        }
    }

    /**
     * закроет конфигуратор и выбросит событие для старта приложения
     */
    private void exitProcess() {
        logger.info("exit");
        exit(0);
    }

    /**
     * сохранит наработаные моточасы
     *
     * @param args команда
     */
    private void resProcess(String[] args) {
        try {
            WorkHoursService workHoursService = new WorkHoursService();
            if (args.length < 2) {
                out.println("  " + workHoursService.getExpendedResources());
            } else {
                workHoursService.setExpendedResources(Integer.parseInt(args[1]));
                logger.info(format("  %s %s\n", args[0], args[1]));
            }

        } catch (NumberFormatException e) {
            logger.warn(e);
            out.println("  Невірне значення - " + args[1]);
        }
    }

    /**
     * сбросит выполненные изменения
     */
    private void resetProcess() {
        properties = readProperties(directory + FILE_SOURCE_PATH);
    }

    /**
     * откатит конфиг к предыдущему состоянию
     *
     * @param args команда
     */
    private void backupProcess(String[] args) {
        try {

            backupFiles.clear();

            List<Path> paths = Files.list(get(directory))
                    .filter(path -> BACKUP_PATTERN.matcher(path.toFile().getName()).find())
                    .collect(toList());

            for (int i = 0; i < paths.size(); i++) {
                backupFiles.put(i + 1, paths.get(i));
            }

            if (backupFiles.size() == 0) {
                throw new IllegalArgumentException("  Немає збережених файлів для відновлення");
            } else {
                int index = -1;

                if (backupFiles.size() > 1) {
                    out.printf("  %-8s - %-25s %s\n", "0", "вийти", "");
                    backupFiles.forEach((k, v) -> out.printf(BACKUP_FORMAT, k, v.getFileName(), DATE_FORMATTER.format(new Date(v.toFile().lastModified()))));

                    do {
                        out.println("  Введіть індекс від 0 до " + backupFiles.size());
                        String input = "";
                        try {
                            input = scanner.nextLine().trim();
                            index = parseInt(input);
                        } catch (NumberFormatException e) {
                            logger.warn(e);
                            out.println("  Невірне значення - " + input);
                        }
                    } while (index < 0 || index > backupFiles.size());
                } else {
                    index = 1;
                }

                if (index > 0) {
                    Path source = backupFiles.get(index);

                    Properties backupProperties = readProperties(directory + "/" + source.toFile().getName());

                    if (printChanges(backupProperties, readProperties(directory + FILE_SOURCE_PATH))) {
                        String inLine;
                        do {
                            out.print("  Зберегти зміни? y/n ");
                        }
                        while (!(inLine = scanner.nextLine().trim()).equals("y") && !inLine.equals("n") && !inLine.equals("exit"));

                        if (inLine.equals("y")) {

                            properties = backupProperties;

                            try (PrintStream os = new PrintStream(directory + FILE_SOURCE_PATH, "utf-8")) {
                                properties.store(os, null);
                            } catch (IOException e) {
                                out.println("  Помилка відкриття файлу " + FILE_SOURCE_PATH);
                                logger.error("  Помилка відкриття файлу " + FILE_SOURCE_PATH, e);
                            }
                            logger.info(format("  %s %s\n", index, source));
                        }

                    } else {
                        out.println("  Жодного параметра не було змінено!");
                    }
                }
            }

        } catch (IOException e) {
            out.println("  Не вдалося відновити файл конфігурації.");
        } catch (NumberFormatException e) {
            logger.warn(e);
            out.println("  Невірне значення - " + args[1]);
        } catch (IllegalArgumentException i) {
            out.println(i.getMessage());
            logger.warn(i);
        }
    }

    /**
     * непечатает хэлп
     */
    private void helpProcess() {
        commands.forEach(command -> out.printf(HELP_FORMAT, command.getCommand(), command.getDescription()));
    }

    /**
     * установит новое значение проперти
     *
     * @param args команда
     */
    private void doubleProcess(String[] args) {
        String propName = getPropertyName(args[1]);
        try {
            if (args.length < 3) {
                out.println("  " + properties.get(propName));
            } else {
                properties.setProperty(propName, valueOf(parseDouble(args[2])));
                logger.info(format("%s %s %s\n", args[0], args[1], args[2]));
            }
        } catch (NumberFormatException e) {
            logger.warn(e);
            out.println("  Невірне значення - " + args[2]);
        }
    }

    /**
     * установит новое значение проперти
     *
     * @param args команда
     */
    private void integerProcess(String[] args) {
        String propName = getPropertyName(args[1]);
        try {
            if (args.length < 3) {
                out.println(" " + properties.get(propName));
            } else {
                properties.setProperty(propName, valueOf(parseInt(args[2])));
                logger.info(format("%s %s %s\n", args[0], args[1], args[2]));
            }
        } catch (NumberFormatException e) {
            logger.warn(e);
            out.println("  Невірне значення - " + args[2]);
        }
    }

    /**
     * установит новое значение проперти
     *
     * @param args команда
     */
    private void dateProcess(String[] args) {
        String propName = getPropertyName(args[1]);
        try {
            if (args.length < 3) {
                out.println("  " + properties.get(propName));
            } else {
                properties.setProperty(propName, DEPLOY_DATE_FORMATTER.format(DEPLOY_DATE_FORMATTER.parse(args[2])));
                logger.info(format("%s %s %s\n", args[0], args[1], args[2]));
            }
        } catch (DateTimeParseException ex) {
            out.println("  Дата введена невірно");
            logger.warn(ex);
        }
    }

    /**
     * установит новое значение проперти
     *
     * @param args команда
     */
    private void stringProcess(String[] args) {
        String propName = getPropertyName(args[1]);

        if (args.length < 3) {
            out.println("  " + properties.get(propName));
        } else {
            if (args[2].contains("\n") ||
                    args[2].contains("\t") ||
                    args[2].contains(":") ||
                    args[2].contains("\\") ||
                    args[2].contains("/")) {
                out.println("  Неприпустимий символ!");
                return;
            }
            properties.setProperty(propName, args[2]);
            logger.info(format("%s %s %s\n", args[0], args[1], args[2]));
        }
    }

    /**
     * установит новое значение проперти
     *
     * @param args команда
     */
    private void passwordProcess(String[] args) {
        if (args.length > 2) {
            properties.setProperty(getPropertyName(args[1]), args[2]);
            logger.info(format("%s %s\n", args[0], args[2]));
        } else {
            out.println("  введіть - password new [новий пароль]");
        }


    }

    private void printNoDB() {
        out.println("  неможлива операція");
    }


    private CommandService writeCommandsWithDefaultValuesToFlash() {
        String date = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(new Date(System.currentTimeMillis()));
        String wagonNumber = properties.getProperty("WAGON", "");

        try {
            if (Files.notExists(Paths.get("/dev/mmcblk1p1"))) {
                out.println("  Накопичувача не знайдено!");
                logger.error("Накопичувача не знайдено!");
                return this;
            }
            Runtime.getRuntime().exec("mkdir /mnt/usb").waitFor();
            Runtime.getRuntime().exec("mount /dev/mmcblk1p1 /mnt/usb").waitFor();

            String fileName = String.format("commands_with_default_values_%s_%s.txt", wagonNumber, date);
            Path path = Paths.get(directory + "/" + fileName);

            StringBuilder data = new StringBuilder();

            commands.forEach(command -> {
                data.append(String.format("%-8s             - %s\r\n", command.getCommand(), command.getDescription()));
                command.getSubCommands().forEach(subCommand -> {
                    String value = properties.getProperty(subCommand.getPropertyName(), "");
                    data.append(String.format(" %-11s %-10s - %s\r\n", subCommand.getSubCommand(), value, subCommand.getDescription()));
                });
                data.append("\r\n");

            });

            try {
                Files.write(path, data.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);

            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Runtime.getRuntime().exec("mv " + directory + "/" + fileName + " " + MOUNT_POINT).waitFor();
            Runtime.getRuntime().exec("umount " + MOUNT_POINT).waitFor();

            out.println("  Виконано!");
        } catch (IOException | InterruptedException | RuntimeException e) {
            out.println("  Невиконано!");
            logger.error(e);
        }

        return this;
    }
}
