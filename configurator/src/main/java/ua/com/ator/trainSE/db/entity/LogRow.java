package ua.com.ator.trainSE.db.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

/**
 * @author Dmitry Sokolov on 20.11.2017.
 */
public class LogRow {
    private Timestamp timestamp;
    private double value;
    private String mnemonic;
    private boolean isFormat;
    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    public LogRow(Timestamp timestamp, double value, String mnemonic, boolean isFormat) {
        this.timestamp = timestamp;
        this.value = value;
        this.mnemonic = mnemonic;
        this.isFormat = isFormat;
    }

    @Override
    public String toString() {
        return Stream.of(formatter.format(timestamp), isFormat ? String.format(mnemonic, value) : mnemonic).collect(joining(" "));
    }
}
