package ua.com.ator.trainSE.services;

import ua.com.ator.trainSE.services.impl.CommandService;

/**
 * @author Dmitry Sokolov on 24.11.2017.
 */
public interface ICommandService {
    CommandService init();

    CommandService printLineSeparator(int num);

    CommandService checkPassword();

    CommandService printHelp();

    void commandProcess();
}
