package ua.com.ator.trainSE.db.dao;

import ua.com.ator.trainSE.db.entity.LogRow;
import ua.com.ator.trainSE.db.entity.Page;
import ua.com.ator.trainSE.db.entity.View;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Dmitry Sokolov on 24.11.2017.
 */
public interface IDao {
    List<Page> getPages() throws SQLException;

    void createPage(String pageName) throws SQLException;

    void deletePage(int pageId) throws SQLException;

    void addMessage(int pageId, int... msgIds) throws SQLException;

    void removeMessage(int pageId, int msgId) throws SQLException;

    void deleteLogs(Timestamp from, Timestamp to) throws SQLException;

    List<LogRow> load(Timestamp from, Timestamp to);

    List<View> loadViews();

    List<LogRow> loadAllLogs();

    List<LogRow> loadViewLogs(int id);
}
