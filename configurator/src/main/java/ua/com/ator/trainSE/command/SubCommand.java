package ua.com.ator.trainSE.command;

import java.util.function.Consumer;

/**
 * @author Dmitry Sokolov on 13.04.2017.
 */
public class SubCommand {
    private String subCommand;
    private String description;
    private String propertyName;
    private Consumer<String[]> consumer;

    /**
     * конструктор
     *
     * @param subCommand   имя подкоманды
     * @param description  описание подкоманды
     * @param propertyName имя проперти
     * @param consumer     колбэк
     */
    public SubCommand(String subCommand, String description, String propertyName, Consumer<String[]> consumer) {
        this.subCommand = subCommand;
        this.description = description;
        this.propertyName = propertyName;
        this.consumer = consumer;
    }

    /**
     * вернет подкоманду
     *
     * @return подкоманда
     */
    public String getSubCommand() {
        return subCommand;
    }

    /**
     * вернет описание подкоманды
     *
     * @return описание подкоманды
     */
    public String getDescription() {
        return description;
    }

    /**
     * вернет имя проперти
     *
     * @return имя проперти
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * дергнет колбек
     *
     * @param args команда
     */
    public void accept(String[] args) {
        consumer.accept(args);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SubCommand{");
        sb.append("subCommand='").append(subCommand).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", propertyName='").append(propertyName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
