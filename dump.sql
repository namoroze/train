-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.37 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных trainse
DROP DATABASE IF EXISTS `trainse`;
CREATE DATABASE IF NOT EXISTS `trainse` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `trainse`;

-- Дамп структуры для таблица trainse.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_logs_messages` (`code`),
  CONSTRAINT `FK_logs_messages` FOREIGN KEY (`code`) REFERENCES `messages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы trainse.logs: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Дамп структуры для таблица trainse.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mnemonic` varchar(50) NOT NULL,
  `is_format` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы trainse.messages: ~61 rows (приблизительно)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` (`id`, `mnemonic`, `is_format`) VALUES
	(1, 'I max батареї %6.0f А', 1),
	(2, 'Температура води в котлі > %11.0f °C', 1),
	(3, 'Відсутня напруга 3000В', 0),
	(4, 'Umax БВВ=%3.0f В', 1),
	(5, 'Відсутній зв\'язок з БВВ', 0),
	(6, 'Umax ЗРК=%3.0f В', 1),
	(7, 'R ізоляції < 1 кОм', 0),
	(8, 'Увімкнено ВВО 1 гр.', 0),
	(9, 'Увімкнено ВВО 2 гр.', 0),
	(10, 'Вимкнено ВВО 1 гр.', 0),
	(11, 'Вимкнено ВВО 2 гр.', 0),
	(12, 'Увімкнено НВО', 0),
	(13, 'Вимкнено НВО', 0),
	(14, 'Насос опалення вкл.', 0),
	(15, 'Насос опалення вимкнено', 0),
	(16, 'Автоматичний режим увімкнено', 0),
	(17, 'Автоматичний режим вимкнено', 0),
	(18, 'Вимкнення', 0),
	(19, 'Увімкнення', 0),
	(20, 'Аварія', 0),
	(21, 'Обрив датчика зовнішньої температури.', 0),
	(22, 'Обрив датчика температури котла.', 0),
	(23, 'Обрив датчика температури приточного повітря.', 0),
	(24, 'Обрив датчика температури салону.', 0),
	(25, 'I max генератора %6.0f А', 1),
	(26, 'Umin АБ=%3.0f В', 1),
	(27, 'Перегрів ПВ кондиціонера увімкнено', 0),
	(28, 'Перегрів ПВ кондиціонера вимкнено', 0),
	(29, 'Перегрів нагрівачів кондиціонера увімкнено', 0),
	(30, 'Перегрів нагрівачів кондиціонера вимкнено', 0),
	(31, 'ВІДМОВА конт.охол.1 увімкнено', 0),
	(32, 'ВІДМОВА конт.охол.1 вимкнено', 0),
	(33, 'ВІДМОВА конт.охол.2 увімкнено', 0),
	(34, 'ВІДМОВА конт.охол.2 вимкнено', 0),
	(35, 'Охолодження увімкнено', 0),
	(36, 'Охолодження вимкнено', 0),
	(37, 'Нагрів увімкнено', 0),
	(38, 'Нагрів вимкнено', 0),
	(39, 'Відсутній зв\'язок з БУТ', 0),
	(40, 'Відсутній відгук від НВО', 0),
	(41, 'Відсутній відгук від ВВО 1 гр', 0),
	(42, 'Відсутній відгук від ВВО 2 гр', 0),
	(43, 'Температура води в котлі > %11.0f °C', 1),
	(44, 'Увімкнено ВВО 1 гр.', 0),
	(45, 'Увімкнено ВВО 2 гр.', 0),
	(46, 'Вимкнено ВВО 1 гр.', 0),
	(47, 'Вимкнено ВВО 2 гр.', 0),
	(48, 'Увімкнено НВО', 0),
	(49, 'Вимкнено НВО', 0),
	(50, 'Насос опалення вкл.', 0),
	(51, 'Насос опалення вимкнено', 0),
	(52, 'Ручний режим увімкнено', 0),
	(53, 'Ручний режим вимкнено', 0),
	(54, 'Відсутній відгук від НВО', 0),
	(55, 'Відсутній відгук від ВВО 1 гр', 0),
	(56, 'Відсутній відгук від ВВО 2 гр', 0),
	(57, 'Відсутня напруга 3000В', 0),
	(58, 'Вентиляція увімкнено', 0),
	(59, 'Вентиляція вимкнено', 0),
	(60, 'Ручний режим кондиціонера увімкнено', 0),
	(61, 'Ручний режим кондиціонера вимкнено', 0),
	(62, 'Перегрів ВК увімкнено', 0),
	(63, 'Перегрів ВК вимкнено', 0),
	(64, 'Засмітився фільтр увімкнено', 0),
	(65, 'Засмітився фільтр вимкнено', 0),
	(66, 'Аварія реле тиску увімкнено', 0),
	(67, 'Аварія реле тиску вимкнено', 0),
	(68, 'Аварія перетворювача K1(UG2) увімкнено', 0),
	(69, 'Аварія перетворювача K1(UG2) вимкнено', 0),
	(70, 'Аварія перетворювача K2(UG3) увімкнено', 0),
	(71, 'Аварія перетворювача K2(UG3) вимкнено', 0),
	(72, 'Аварія перетворювача ПВ,ВК(UG1) увімкнено', 0),
	(73, 'Аварія перетворювача ПВ,ВК(UG1) вимкнено', 0);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Дамп структуры для таблица trainse.views
CREATE TABLE IF NOT EXISTS `views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_name` varchar(50) NOT NULL,
  `page_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы trainse.views: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `views` DISABLE KEYS */;
INSERT INTO `views` (`id`, `view_name`, `page_name`) VALUES
	(1, 'electric', 'Журнал електрообладнання'),
	(2, 'climate_auto', 'Журнал кліматичної установки режим авто'),
	(3, 'climate_manual', 'Журнал кліматичної установки режим ручний'),
	(4, 'climate_conditioner', 'Журнал кондиціонера'),
	(5, 'worktime', 'Журнал часу роботи');
/*!40000 ALTER TABLE `views` ENABLE KEYS */;

-- Дамп структуры для таблица trainse.view_messages
CREATE TABLE IF NOT EXISTS `view_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_views_mapping_messages` (`message_id`),
  KEY `FK_view_messages_views` (`view_id`),
  CONSTRAINT `FK_view_messages_messages` FOREIGN KEY (`message_id`) REFERENCES `messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_view_messages_views` FOREIGN KEY (`view_id`) REFERENCES `views` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы trainse.view_messages: ~62 rows (приблизительно)
/*!40000 ALTER TABLE `view_messages` DISABLE KEYS */;
INSERT INTO `view_messages` (`id`, `view_id`, `message_id`) VALUES
	(1, 1, 1),
	(2, 1, 7),
	(3, 1, 3),
	(4, 1, 4),
	(5, 1, 5),
	(6, 1, 6),
	(7, 2, 42),
	(8, 1, 20),
	(9, 2, 2),
	(10, 2, 41),
	(11, 2, 40),
	(12, 1, 39),
	(13, 4, 38),
	(14, 4, 37),
	(15, 4, 36),
	(16, 4, 35),
	(17, 4, 34),
	(18, 4, 33),
	(19, 4, 32),
	(24, 2, 12),
	(25, 2, 13),
	(26, 2, 14),
	(27, 2, 15),
	(28, 2, 16),
	(29, 2, 17),
	(30, 4, 31),
	(31, 2, 21),
	(32, 2, 22),
	(33, 2, 23),
	(34, 2, 24),
	(35, 5, 18),
	(36, 5, 19),
	(37, 1, 25),
	(38, 1, 26),
	(39, 4, 27),
	(40, 4, 28),
	(41, 4, 29),
	(42, 4, 30),
	(43, 3, 43),
	(44, 3, 44),
	(45, 3, 45),
	(46, 3, 46),
	(47, 3, 47),
	(48, 3, 48),
	(49, 3, 49),
	(50, 3, 50),
	(51, 3, 51),
	(52, 3, 52),
	(53, 3, 53),
	(54, 3, 54),
	(55, 3, 55),
	(56, 3, 56),
	(57, 3, 57),
	(58, 4, 58),
	(59, 4, 59),
	(60, 4, 60),
	(61, 4, 61),
	(62, 4, 62),
	(63, 4, 63),
	(64, 4, 64),
	(65, 4, 65),
	(66, 4, 66),
	(67, 4, 67),
	(68, 4, 68),
	(69, 4, 69),
	(70, 4, 70),
	(71, 4, 71),
	(72, 4, 72),
	(73, 4, 73);
/*!40000 ALTER TABLE `view_messages` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
